# -*- coding: utf-8 -*-
{
	'name': "Uninstall Group",

	'summary': """Hide Uninstall Button In Apps""",

	'description': """
		 for hiding uninstall button from some app 
	""",

	'author': "Hamza Azeem",
	'application':'True',
	# any module necessary for this one to work correctly
	'depends': ['base'],

	# # always loaded
	'data': [
		'Views/My_view.xml',
	],
	# # only loaded in demonstration mode

}
