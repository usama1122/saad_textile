# -*- coding: utf-8 -*-
{
    'name': "Salary Sheet - Pandas",
    'description': "Salary Sheet - Pandas",
    'author': 'Enterprise Cube (Pvt) Limited',
    'application': True,
    'depends': ['base','web','report','hr','hr_payroll'],
    'data': ['template.xml','views/module_report.xml'],
}