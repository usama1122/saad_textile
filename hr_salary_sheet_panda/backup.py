#-*- coding:utf-8 -*-
from openerp import models, fields, api
from datetime import timedelta,datetime,date
from dateutil.relativedelta import relativedelta
import time
import pandas as pd
import numpy as np
import pandas.io.sql as psql

class PandasSalarySheetReport(models.AbstractModel):
    _name = 'report.hr_salary_sheet_panda.panda_sheet'

    @api.model
    def render_html(self,docids, data=None):
        report_obj = self.env['report']
        docs = self.env['salary.sheet.pandas'].browse(self.env.context.get('active_ids'))

        date_from = docs.date_from
        date_to = docs.date_to
        batches = docs.batches

        pd.set_option('display.max_columns', None)

        pay_slip_line_table = self.env.cr.execute("SELECT p_line.name, p_line.total, p_line.slip_id, p_line.salary_rule_id, s_rules.sequence, emp.department_id, dep.name as dep_name FROM hr_payslip_line p_line INNER JOIN hr_payslip p_slip ON p_line.slip_id = p_slip.id INNER JOIN hr_salary_rule s_rules ON p_line.salary_rule_id = s_rules.id INNER JOIN hr_employee emp ON p_line.employee_id = emp.id INNER JOIN hr_department dep ON emp.department_id = dep.id WHERE p_slip.payslip_run_id = %s ORDER BY s_rules.sequence ASC" % batches.id)

        pay_slip_line_table = self.env.cr.dictfetchall()
        parent_frame = pd.DataFrame(pay_slip_line_table)
        
        salary_rule_frame = parent_frame[['name','salary_rule_id']].groupby(['salary_rule_id','name']).count().reset_index()

        unique_rules = salary_rule_frame.salary_rule_id.unique()
        unique_rules_names = salary_rule_frame.to_dict('records')
        
        department_frame = parent_frame[['dep_name','department_id']].groupby(['dep_name','department_id']).count().reset_index()

        unique_deps = department_frame.department_id.unique()
        unique_dep_names = department_frame.to_dict('records')

        parent_frame["total"] = parent_frame["total"].astype(str)
        parent_frame["salary_rule_id"] = parent_frame["salary_rule_id"].astype(int)
        parent_frame["salary_rule_id"] = parent_frame["salary_rule_id"].astype(str)
        # parent_frame["merger"] = '(' + str(parent_frame["salary_rule_id"]) + ', ' + str( parent_frame["total"]) + ')'
        parent_frame["merger"] = parent_frame["salary_rule_id"] + ', ' + parent_frame["total"]

        print parent_frame[['merger','total','salary_rule_id']]


        # salary_rule_combine = parent_frame.groupby('slip_id')['salary_rule_id'].apply('/'.join).reset_index()
        
        # amounts_combine = parent_frame.groupby('slip_id')['total'].apply('/'.join).reset_index()

        # combine_salary_rule_values = pd.merge(salary_rule_combine, amounts_combine, how='left', left_on='slip_id', right_on ='slip_id')

        # print combine_salary_rule_values

        # final_list = []
        # for index, row in combine_salary_rule_values.iterrows():
        #     if str(row['slip_id']) != 'nan':

        #         current_payslip = self.env['hr.payslip'].search([('id','=',int(row['slip_id']))])
                    
        #         totals = row['total']
        #         totals_list = totals.split("/")
        #         totals_list = [float(i) for i in totals_list]

        #         totals_list_float = [float(i) for i in totals_list]
                
        #         all_rules = row['salary_rule_id']
        #         rules_list = all_rules.split("/")
        #         rules_list = [float(i) for i in rules_list]

        #         entity = ({
                    
        #             'employee_id': current_payslip.employee_id.emp_code,
        #             'employee_name': current_payslip.employee_id.name,
        #             'designation': current_payslip.employee_id.job_id.name,
        #             'employee_cnic': current_payslip.employee_id.identification_id,
        #             'employee_slip': current_payslip.number,
        #             'totals': [],
        #         })

        #         for rule in unique_rules:
        #             if rule in rules_list:
        #                 entity['totals'].append(totals_list[rules_list.index(rule)])
        #             else:
        #                 entity['totals'].append(0)

        #         final_list.append(entity)
        
        # records = {
        #     'heads': unique_rules_names,
        #     'data': final_list,
        # }
        
        records = {
            'heads': [],
            'data': [],
        }

        docargs = {
            'doc_ids': docids,
            'doc_model': 'hr.payslip',
            'records': records
        }
        return self.env['report'].render('hr_salary_sheet_panda.panda_sheet', docargs)

                # parent_frame["total"] = parent_frame["total"].astype(str)
        # parent_frame["salary_rule_id"] = parent_frame["salary_rule_id"].astype(int)
        # parent_frame["salary_rule_id"] = parent_frame["salary_rule_id"].astype(str)
        # parent_frame["merger"] = pd.Series([{parent_frame["salary_rule_id"]:parent_frame["total"]}])
        # print parent_frame["merger"]

        # salary_rule_combine = parent_frame.groupby('slip_id')['merger'].apply('-'.join).reset_index()

        # salary_rule_combine["merger"] = salary_rule_combine["merger"].str.split("-")
        # # parent_frame["merger"] = parent_frame["merger"].astype(str)
        # salary_rule_combine["merger1"] = pd.Series([{str(sub).split(":")[0]: str(sub).split(":")[1]} for sub in str(parent_frame["merger"])[1:-1].split(", ")])
        # print salary_rule_combine["merger1"]
        # salary_rule_combine["merger_1"] = {sub.split(":")[0]: sub.split(":")[1] for sub in salary_rule_combine["merger"]}

        # print salary_rule_combine

        # pay_slip_employee = self.env.cr.execute("SELECT p_slip.id as slip_id, p_slip.employee_id as emp, dep.id as dep_id, dep.name as dep_name FROM hr_payslip p_slip INNER JOIN hr_employee emp ON p_slip.employee_id = emp.id INNER JOIN hr_department dep ON emp.department_id = dep.id WHERE p_slip.payslip_run_id = %s " % batches.id)

        # pay_slip_emp_table = self.env.cr.dictfetchall()
        # emp_frame = pd.DataFrame(pay_slip_emp_table)

        # combined_frames = pd.merge(salary_rule_combine, emp_frame, how='left', left_on='slip_id', right_on ='slip_id')

        # entries = combined_frames.to_dict('records')

        # print entries[0]
        # print "------------------------"

        # res = {sub.split(":")[0]: sub.split(":")[1] for sub in test_string[1:-1].split(", ")}

        # res = {dict(sub['merger']) for sub in entries}
        # print res[0]



        # final_list = []
        # for index, row in combine_salary_rule_values.iterrows():
        #     if str(row['slip_id']) != 'nan':

        #         current_payslip = self.env['hr.payslip'].search([('id','=',int(row['slip_id']))])
                    
        #         totals = row['total']
        #         totals_list = totals.split("/")
        #         totals_list = [float(i) for i in totals_list]

        #         totals_list_float = [float(i) for i in totals_list]
                
        #         all_rules = row['salary_rule_id']
        #         rules_list = all_rules.split("/")
        #         rules_list = [float(i) for i in rules_list]

        #         entity = ({
                    
        #             'employee_id': current_payslip.employee_id.emp_code,
        #             'employee_name': current_payslip.employee_id.name,
        #             'designation': current_payslip.employee_id.job_id.name,
        #             'employee_cnic': current_payslip.employee_id.identification_id,
        #             'employee_slip': current_payslip.number,
        #             'totals': [],
        #         })

        #         for rule in unique_rules:
        #             if rule in rules_list:
        #                 entity['totals'].append(totals_list[rules_list.index(rule)])
        #             else:
        #                 entity['totals'].append(0)

        #         final_list.append(entity)
        
        # records = {
        #     'heads': unique_rules_names,
        #     'data': final_list,
        # }


# ---------------------------------------------------------------------------------

# new_dict = []
#         for line in pay_slip_line_table:
#             new_dict.append({'slip_id': line['slip_id'], 'rule': {'rule_id': line['salary_rule_id'], 'amount': line['total']}})
        
        
#         parent_frame['slip_id'] = parent_frame['slip_id'].apply(str)
#         salary_rule_combine = parent_frame.groupby('slip_id')['rule'].apply(list).reset_index()
#         salary_rule_combine['slip_id'] = salary_rule_combine['slip_id'].apply(int)

#         print salary_rule_combine

        # pay_slip_emp = self.env.cr.execute("SELECT emp.emp_code AS code, emp.name_related AS emp_name, emp.identification_id AS cnic, emp.department_id AS dep_id, dep.name AS dep_name, emp.id AS emp_id, p_slip.id AS slip_id, job.name AS job FROM hr_payslip p_slip INNER JOIN hr_employee emp ON p_slip.employee_id = emp.id INNER JOIN hr_department dep ON emp.department_id = dep.id INNER JOIN hr_job job ON emp.job_id = job.id WHERE p_slip.payslip_run_id = %s" % batches.id)

        # pay_slip_emp = self.env.cr.dictfetchall()
        # emp_frame = pd.DataFrame(pay_slip_emp)

        # combined_frames = pd.merge(salary_rule_combine, emp_frame, how='left', left_on='slip_id', right_on ='slip_id')

        # rule_and_sequance = self.env.cr.execute("SELECT p_line.name, s_rules.sequence, p_line.salary_rule_id FROM hr_payslip_line p_line INNER JOIN hr_payslip p_slip ON p_line.slip_id = p_slip.id INNER JOIN hr_salary_rule s_rules ON p_line.salary_rule_id = s_rules.id WHERE p_slip.payslip_run_id = %s ORDER BY s_rules.sequence ASC" % batches.id)

        # rule_and_sequance = self.env.cr.dictfetchall()
        # salary_rule_frame = pd.DataFrame(rule_and_sequance)

        # salary_rule_frame = salary_rule_frame[['name','sequence','salary_rule_id']].groupby(['name','sequence','salary_rule_id']).count().reset_index()

        # unique_rules = salary_rule_frame.salary_rule_id.unique()
        # unique_rules_names = salary_rule_frame.to_dict('records')
        # unique_rules_names.sort(key=itemgetter('sequence','salary_rule_id'), reverse=False)

        # entries = combined_frames.to_dict('records')
        # for x in entries:
        #     x['rule'].sort(key=itemgetter('rule_id'), reverse=False)
        
        # print entries[0]['rule']
        # print "----------------------------"