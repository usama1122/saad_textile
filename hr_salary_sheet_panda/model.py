#-*- coding:utf-8 -*-
from openerp import models, fields, api
from datetime import timedelta,datetime,date
from dateutil.relativedelta import relativedelta
import time
import pandas as pd
import numpy as np
import pandas.io.sql as psql
from operator import itemgetter

class PandasSalarySheetReport(models.AbstractModel):
    _name = 'report.hr_salary_sheet_panda.panda_sheet'

    @api.model
    def render_html(self,docids, data=None):
        report_obj = self.env['report']
        docs = self.env['salary.sheet.pandas'].browse(self.env.context.get('active_ids'))

        report_type = docs.report_type
        department_id = docs.department_id
        batches = docs.batches

        pd.set_option('display.max_columns', None)

        pay_slip_line_table = self.env.cr.execute("SELECT p_line.name, p_line.total, p_line.slip_id, p_line.salary_rule_id, s_rules.sequence, s_rules.category_id, r_categ.name AS categ_name, r_categ.sequance AS r_seq FROM hr_payslip_line p_line INNER JOIN hr_payslip p_slip ON p_line.slip_id = p_slip.id INNER JOIN hr_salary_rule s_rules ON p_line.salary_rule_id = s_rules.id INNER JOIN hr_salary_rule_category r_categ ON s_rules.category_id = r_categ.id WHERE p_slip.payslip_run_id = %s ORDER BY s_rules.sequence ASC" % batches.id)

        pay_slip_line_table = self.env.cr.dictfetchall()
        parent_frame = pd.DataFrame(pay_slip_line_table)
        parent_frame['r_seq'] = parent_frame['r_seq'].apply(str)
        salary_rule_frame = parent_frame[['name','salary_rule_id','sequence','category_id']].groupby(['name','salary_rule_id','sequence','category_id']).count().reset_index()

        unique_rules = list(salary_rule_frame.salary_rule_id.unique())
        rule_name = salary_rule_frame.to_dict('records')
        rule_name.sort(key=itemgetter('category_id','sequence','salary_rule_id'), reverse=False)

        salary_categ_frame = parent_frame[['salary_rule_id','category_id','categ_name','r_seq']].groupby(['salary_rule_id','category_id','categ_name','r_seq']).count().reset_index()

        categ_frame = salary_categ_frame[['category_id','categ_name','r_seq']]
        categ_frame['len'] = 0

        categ_frame = categ_frame.groupby(['category_id','categ_name','r_seq']).count().reset_index()
        salary_categs = categ_frame.to_dict('records')
        # REQUIRES MORE SEARCHING
        for item in unique_rules:
            parent_frame[item] = np.where((parent_frame['salary_rule_id'] == item), parent_frame['total'], 0)

        parent_frame.drop(['salary_rule_id', 'total'], axis = 1)
        parent_frame = parent_frame.groupby(['slip_id']).sum().reset_index()

        pay_slip_emp = self.env.cr.execute("SELECT emp.emp_code AS code, emp.name_related AS emp_name, emp.identification_id AS cnic, emp.department_id AS dep_id, dep.name AS dep_name, emp.id AS emp_id, p_slip.id AS slip_id, p_slip.number AS slip_name, job.name AS job FROM hr_payslip p_slip INNER JOIN hr_employee emp ON p_slip.employee_id = emp.id INNER JOIN hr_department dep ON emp.department_id = dep.id INNER JOIN hr_job job ON emp.job_id = job.id WHERE p_slip.payslip_run_id = %s" % batches.id)

        pay_slip_emp = self.env.cr.dictfetchall()
        emp_frame_root = pd.DataFrame(pay_slip_emp)

        emp_attendance = self.env.cr.execute("SELECT attend.employee_id, attend.total_time, attend.todaystatus FROM hr_payslip p_slip INNER JOIN actual_attendence attend ON p_slip.employee_id = attend.employee_id WHERE p_slip.payslip_run_id = %s AND attend.date >= '%s' AND attend.date <= '%s'" % (batches.id,batches.date_start,batches.date_end))

        emp_attendance = self.env.cr.dictfetchall()
        attend_frame = pd.DataFrame(emp_attendance)

        attend_stages = ['present','absent']
        attend_frame['days'] = np.where(((attend_frame['total_time'] >= 7) & (attend_frame['todaystatus'] == 'present')) | (~attend_frame['todaystatus'].isin(attend_stages)), 1, 0)
        attend_frame = attend_frame[['employee_id','days']].groupby(['employee_id']).sum().reset_index()
        emp_frame = pd.merge(attend_frame, emp_frame_root, how='left', left_on='employee_id', right_on ='emp_id')

        combined_frames = pd.merge(parent_frame, emp_frame, how='left', left_on='slip_id', right_on ='slip_id')

        if report_type in ('dep_wise','specific_dep'):

            dep_frame = emp_frame[['dep_id','dep_name']]
            # dep_frame['len'] = 0
            dep_frame = dep_frame.groupby(['dep_id','dep_name']).count().reset_index()

            if report_type == 'dep_wise':
                unique_deps = list(dep_frame.dep_id.unique())
                entries = dep_frame.to_dict('records')

            elif report_type == 'specific_dep':
                unique_deps = department_id.ids
                the_list = dep_frame.to_dict('records')

                entries = []
                for dep in the_list:
                    if dep['dep_id'] in unique_deps:
                        entries.append(dep)

            # entries.sort(key=itemgetter('len'), reverse=True)

            for item in entries:
                df_filtered = combined_frames[combined_frames['dep_id'] == item['dep_id']].reset_index()
                item['records'] = df_filtered.to_dict('records')
                item['totals'] = (df_filtered.groupby(['dep_id']).sum().reset_index()[unique_rules]).to_dict('records')

        elif report_type == 'full':
            entries = combined_frames.to_dict('records')
            entries.sort(key=itemgetter('dep_id','emp_id'), reverse=False)

        combined_frames['addition'] = 1
        grand_totals = (combined_frames.groupby(['addition']).sum().reset_index()[unique_rules]).to_dict('records')

        print rule_name
        records = {
            'salary_categs': salary_categs,
            'heads': rule_name,
            'data': entries,
            'grand_totals': grand_totals
        }

        docargs = {
            'doc_ids': docids,
            'doc_model': 'hr.payslip',
            'records': records,
            'dynamic_cols': len(unique_rules),
            'report_type': report_type,
            'batches': batches
        }

        print "finallllllll"
        return self.env['report'].render('hr_salary_sheet_panda.panda_sheet', docargs)