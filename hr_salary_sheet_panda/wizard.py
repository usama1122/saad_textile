#-*- coding:utf-8 -*-
from odoo import models, fields, api
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

class SalarySheetPandas(models.TransientModel):
    _name = "salary.sheet.pandas"

    batches = fields.Many2one('hr.payslip.run',string="Batches")
    report_type = fields.Selection([
		('dep_wise','Department Wise'),
		('full','All Employees'),
		('specific_dep','Specific Department'),
		], string='Report Type', default='dep_wise')
    department_id = fields.Many2many('hr.department',string="Department")

    @api.multi
    def print_report(self):
        data = {}
        data['form'] = self.read(['batches','report_type','department_id'])[0]
        return self.env['report'].get_action(self, 'hr_salary_sheet_panda.panda_sheet', data=data)