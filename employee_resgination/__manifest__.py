# -*- coding: utf-8 -*-
{
    'name': "Employee Resignation...",

    'summary': """
        Ecube""",
    'author': "Muhammad Hamza Azeem Qureshi,Enterprise Cube (Pvt) Limited",
    'website': "http://www.ecube.pk",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mail','hr',],

    # always loaded
    'data': [
        'views/mainview.xml',
	    'views/separation.xml',
    ],

}
