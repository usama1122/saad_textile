# -*- coding: utf-8 -*-
from openerp import models, fields, api
from datetime import timedelta,datetime,date
from dateutil.relativedelta import relativedelta
from openerp.exceptions import ValidationError

class EmployeeSeparation(models.Model):
	_name = 'employee.separation'
	_description = 'Employee Separation'
	_inherit = ['mail.thread']
	_rec_name = 'employee_code' or ''+'-'or''+'employee_id'or ''
	
	employee_code =  fields.Char(
		string='Employee ID',required=True, copy=False,track_visibility='onchange'
	)
	employee_id = fields.Many2one(
		'hr.employee',
		string='Employee', copy=False,track_visibility='onchange'
	)
	cnic = fields.Char(
		string='CNIC', copy=False
	)
	address = fields.Char(
		string='Address',copy=False
	)
	reason = fields.Char(
		string='Reason',copy=False
	)
	department_id = fields.Many2one(
		'hr.department',
		string='Department',copy=False
	)
	designation_id = fields.Many2one('hr.job',"Designation" ,copy=False  )
	date_of_join = fields.Date(
		string='Date of Joining', copy=False
	)
	employee_status = fields.Char("Employee Status",copy=False ,track_visibility='onchange')
	separation_type = fields.Selection([
		('resignation', 'Resignation'),
		('terminal', 'Termination'),
		('layoff', 'Lay off'),
		('deceased', 'Deceased'),
		('retirement', 'Retirement'),
		],string = "Separation Type" ,track_visibility='onchange', copy=False )
	notice_period = fields.Selection([
		('hours', '24 Hours'),
		('month', '1 Month'),
		],string = "Notice Period Status" ,track_visibility='onchange', copy=False )
	states = fields.Selection([
		('draft', 'Draft'),
		('inprogress', 'In Progress'),
		('approved', 'Final Approval'),
		],string = "States" ,track_visibility='onchange',default='draft' , copy=False)
	SELECT =[
	('approve', 'Approved'),
	('waiting', 'waiting'),
	]
	admin_dep_approval = fields.Selection(SELECT,string = "Admin Department Approval" ,track_visibility='onchange',default='waiting' , copy=False)
	it_dep_approval = fields.Selection(SELECT,string = "IT Department Approval" ,track_visibility='onchange',default='waiting' ,copy=False)
	prod_dep_approval = fields.Selection(SELECT,string = "Production Department Approval" ,track_visibility='onchange',default='waiting',copy=False)
	account_dep_approval = fields.Selection(SELECT,string = "Accounts Department Approval" ,track_visibility='onchange',default='waiting',copy=False)
	
	start_date = fields.Date(
		string='Start Date',required=True,copy=False,track_visibility='onchange'
	)
	end_date = fields.Date(
		string='End Date',required=True,copy=False,track_visibility='onchange'
	)
	comment = fields.Text(
		string='Comment',
	)
	attachment_ids = fields.Many2many(comodel_name="ir.attachment", 
								relation="m2m_ir_identity_card_rel", 
								column1="m2m_id",
								column2="attachment_id",
								string="Attachments",copy=False,track_visibility='onchange')

	lock_deductions = fields.Boolean(
		string='Lock',
	)
	unlock_deductions  = fields.Boolean(
		string='Unlock',default=True,
	)
	lock_payable = fields.Boolean(
		string='Lock',
	)
	unlock_payable  = fields.Boolean(
		string='Unlock',default=True,
	)

	deduction_detail = fields.Char(
		string='deduction detail', 
	)

	admin_deduction_detail = fields.Char(
		string='Deduction Detail',track_visibility='onchange'
	)

	admin_payable_detail = fields.Char(
		string='Payable Detail',track_visibility='onchange'
	)

	admin_payable_amount = fields.Float(
		string='Payable Amount',track_visibility='onchange'
	)

	admin_deducttion_amount = fields.Float(
		string='Deduction Amount',track_visibility='onchange' 
	)
	admin_remarks = fields.Text(
		string='Remarks',track_visibility='onchange'
	)

	it_deduction_detail = fields.Char(
		string='Deduction Detail',track_visibility='onchange' 
	)

	it_payable_detail = fields.Char(
		string='Payable Petail',track_visibility='onchange' 
	)

	it_payable_amount = fields.Float(
		string='Paybel Amount',track_visibility='onchange' 
	)

	it_deducttion_amount = fields.Float(
		string='Deduction Amount',track_visibility='onchange' 
	)
	other_payable = fields.Float(
		string='Other Payable',track_visibility='onchange' 
	)
	it_remarks = fields.Text(
		string='Remarks',track_visibility='onchange'
	)

	prod_deduction_detail = fields.Char(
		string='Deduction Detail',track_visibility='onchange' 
	)

	prod_payable_detail = fields.Char(
		string='Payable Petail',track_visibility='onchange' 
	)

	prod_payable_amount = fields.Float(
		string='Payable Amount',track_visibility='onchange' 
	)

	prod_deducttion_amount = fields.Float(
		string='Deduction Amount',track_visibility='onchange' 
	)
	prod_remarks = fields.Text(
		string='Remarks',track_visibility='onchange'
	)

	account_deduction_detail = fields.Char(
		string='Deduction Detail',track_visibility='onchange' 
	)

	account_payable_detail = fields.Char(
		string='Payable Petail',track_visibility='onchange' 
	)

	account_payable_amount = fields.Float(
		string='Payable Amount',track_visibility='onchange' 
	)

	account_deducttion_amount = fields.Float(
		string='Deduction Amount',track_visibility='onchange' 
	)
	account_remarks = fields.Text(
		string='Remarks',track_visibility='onchange'
	)
	payable_detail	 = fields.Text(
	string='payable detail',track_visibility='onchange'
	)
	remarks = fields.Text(
		string='Remarks',track_visibility='onchange'
	)

	basic_salary = fields.Float(
		string='Basic Salary',track_visibility='onchange')

	# Deduction field only for notice period = 24 Hours
	b_salary_deduction_notice = fields.Float(string='Basic Salary Deduction for Notice', readonly=True)

	gratuity = fields.Float(
		string='Gratuity',track_visibility='onchange' )

	gratuity_accounts = fields.Float(
		string='Gratuity',track_visibility='onchange' 
	)

	days_payable = fields.Float(
		string='Days Payable',track_visibility='onchange' 
	)

	days_payable_amount = fields.Float(
		string='Days Payable Amount',track_visibility='onchange' 
	)

	deduction_days = fields.Float(
		string='Deducted Days',track_visibility='onchange' 
	)

	deduction_amount = fields.Float(
		string='Days Deduction Amount',track_visibility='onchange' 
	)

	deduction_amount_department = fields.Float(
		string='Deduction Amount',track_visibility='onchange' 
	)

	payable_amount_department = fields.Float(
		string='Payable Amount',track_visibility='onchange' 
	)

	tax = fields.Float(
		string='Tax Amount',track_visibility='onchange' 
	)

	advance_salary = fields.Float(
		string='Advance salary',track_visibility='onchange' 
	)


	net_salary = fields.Float(string="Net Payable")
	
	@api.onchange('basic_salary')
	def calculate_gratuity(self):
		if self.date_of_join:
			date_format_str = '%Y-%m-%d'
			current_date = datetime.strptime(str(date.today()), date_format_str)
			joining_date = datetime.strptime(str(self.date_of_join), date_format_str)
			# joining_date = datetime.strptime('2020-07-01', date_format_str)

			diff = relativedelta(current_date,joining_date)

			# diff = current_date - self.date_of_join

			# days =  diff.days
			# months = round(((diff.days)/26),0)
			# years_float = round((((diff.days)/26)/12),2)
			years_float = diff.years
			# years = round((((diff.days)/26)/12),0)


			print "####################"
			print "						"
			# print days
			print "						"
			# print months
			print "						"
			# print years
			print current_date
			print joining_date
			print years_float

			print "####################"
			if self.basic_salary > 0 and years_float >= 1:

				print self.basic_salary

				grat = (self.basic_salary/26*31)*years

				self.gratuity = grat


	@api.onchange('admin_deducttion_amount','it_deducttion_amount','prod_deducttion_amount','account_deducttion_amount')
	def _onchange_deduction(self):
		self.deduction_amount_department = self.admin_deducttion_amount + self.it_deducttion_amount + self.prod_deducttion_amount + self.account_deducttion_amount
	
	@api.onchange('admin_payable_amount','it_payable_amount','prod_payable_amount','account_payable_amount')
	def _onchange_payable(self):
		self.payable_amount_department = self.admin_payable_amount + self.it_payable_amount + self.prod_payable_amount + self.account_payable_amount
	
	@api.onchange('deduction_days')
	def on_change_deduction_days(self):
		if self.basic_salary:
			pass
		else:
			self.getting_basic_info_from_contract()

		self.deduction_amount = (self.basic_salary / 26) * self.deduction_days
	
	@api.onchange('days_payable')
	def on_change_payable_days(self):
		if self.basic_salary:
			pass
		else:
			self.getting_basic_info_from_contract()
		self.days_payable_amount = (self.basic_salary / 26) * self.days_payable
	
	@api.onchange('employee_id')
	def getting_basic_info_from_contract(self):
		
		contract_id = self.env['hr.contract'].search([('employee_id','=',self.employee_id.id),('state','!=','close')])
		gratuity_payslips_lines = self.env['hr.payslip.line'].search([('slip_id.employee_id','=',self.employee_id.id),('slip_id.state','=','done'),('code','=','GRAT')])
		# self.gratuity= sum(gratuity_payslips_lines.mapped('amount'))+contract_id.gratuity
	
		for x in reversed(contract_id):
			# self.basic_salary = x.total_salary
			self.basic_salary = x.wage
			break
		self.b_salary_deduction_notice = self.basic_salary

		# print gratuity_payslips.mapped(line_id.)
		# self.gratuity = self.basic_salary


	# Function for Basic Salary Deduction upon Notice Period Change

	@api.onchange('notice_period')
	def basic_salary_deduction_for_notice(self):
		if self.notice_period == 'hours':
			self.b_salary_deduction_notice = self.basic_salary
		else:
			self.b_salary_deduction_notice = 0

	# Function for Basic Salary Deduction upon Notice Period Change Ends Here


	@api.onchange('deduction_days')
	def on_change_deduction_days(self):
		if self.basic_salary:
			pass
		else:
			self.getting_basic_info_from_contract()

		self.deduction_amount = (self.basic_salary / 26) * self.deduction_days


	
	@api.onchange('deduction_amount_department','payable_amount_department','deduction_amount','days_payable_amount','gratuity','tax','advance_salary','notice_period','other_payable')
	def get_net_salary(self):
		self.net_salary = (self.payable_amount_department + self.days_payable_amount + self.gratuity + self.other_payable) - (self.deduction_amount_department + self.deduction_amount+ self.tax + self.advance_salary + self.b_salary_deduction_notice)
	# self.basic_salary + 
	
	@api.onchange('employee_id')
	def _onchange_employee(self):
		if self.employee_id:
			self.employee_code = self.employee_id.emp_code
			self.cnic = self.employee_id.identification_id
			self.address = self.employee_id.par_address
			self.department_id = self.employee_id.department_id.id
			self.designation_id = self.employee_id.job_id.id
			self.date_of_join = self.employee_id.joining_date

	@api.onchange('employee_code')
	def _onchange_employee_code(self):
		if self.employee_code:
			employee_id = self.env['hr.employee'].search([('emp_code','=',self.employee_code)],limit=1)
			if employee_id:
				self.employee_id =employee_id.id
				self.cnic = str(employee_id.identification_id)
				self.address = str(employee_id.par_address)
				self.department_id =employee_id.department_id.id
				self.designation_id =employee_id.job_id.id
				self.date_of_join =employee_id.joining_date





	def set_to_draft(self):
		if self.employee_id:
			self.employee_id.active = True
			self.states = 'draft'
	
	def set_to_inprogress(self):

		if self.employee_id:
			self.employee_id.active = False
			self.states = 'inprogress'

	def set_to_approve(self):
		if self.admin_dep_approval != 'approve':
			raise ValidationError("Employee Separation is not approved by admin Department.")
		if self.it_dep_approval != 'approve':
			raise ValidationError("Employee Separation is not approved by IT Department.")
		if self.prod_dep_approval != 'approve':
			raise ValidationError("Employee Separation is not approved by Production Department.")
		if self.account_dep_approval != 'approve':
			raise ValidationError("Employee Separation is not approved by Accounts Department.")
		contract_id = self.env['hr.contract'].search([('employee_id','=',self.employee_id.id),('state','!=','close')])
		for x in reversed(contract_id):
			x.state = 'close'
			break
		self.states = 'approved'
	
	def locked_deductions(self):
		self.lock_deductions= True
		self.unlock_deductions= False

	def unlocked_deductions(self):
		self.unlock_deductions= True
		self.lock_deductions = False
	
	def locked_payable(self):
		self.lock_payable= True
		self.unlock_payable= False

	def unlocked_payable(self):
		self.unlock_payable= True
		self.lock_payable= False


	@api.multi
	def unlink(self):
		for x in self:
			if x.states != "draft":
				raise  ValidationError('Cannot Delete Record')
		return super(EmployeeSeparation,self).unlink()