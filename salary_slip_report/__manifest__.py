# -*- coding: utf-8 -*-
{
    'name': "Salary Slip Report",
    'description': "Salary Slip Report",
    'author': 'Rana Rizwan',
    'application': True,
    'depends': ['base'],
    'data': ['template.xml','views/module_report.xml'],
}