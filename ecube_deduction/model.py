# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from openerp.exceptions import Warning, ValidationError, UserError
import datetime
import datetime as dt


class EcubeDeductionClass(models.Model):
	_name = 'ecube.deduction'
	_rec_name = 'employee_id'

	description = fields.Text(string="Description")
	amount = fields.Float(string="Amount", required=True)
	overtime_hours = fields.Float(string="Overtime Hours")

	employee_id = fields.Many2one('hr.employee',string="Employee", required=True)
	type_id = fields.Many2one('ecube.deduction.type',string="Type", required=True)
	branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

	date = fields.Date(string="Date", required=True)
	state = fields.Selection([
		('draft', 'Draft'),
		('validate', 'Validate'),
		], default = "draft")

	@api.multi
	def unlink(self):
		for x in self:
			if x.state == 'validate':
				raise ValidationError('Cannot delete record in this stage.')
		res = super(EcubeDeductionClass, self).unlink()

		return res

	@api.multi
	def draft_pressed(self):
		self.state = "draft"

	@api.multi
	def validate_pressed(self):
		self.state = "validate"

	@api.onchange('employee_id')
	def get_branch_id(self):
		if self.employee_id:
			if not self.branch:
				self.branch = self.employee_id.branch.id

	@api.multi
	def write(self, vals):
		before=self.write_date
		super(EcubeDeductionClass, self).write(vals)
		after = self.write_date
		if before != after:
			self.get_branch_id()

		return True

class EcubePayrollTypeClass(models.Model):
	_name = 'ecube.deduction.type'
	_rec_name = 'name'

	name = fields.Char(string="Name")

