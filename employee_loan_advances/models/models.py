# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import Warning, ValidationError
from datetime import datetime, timedelta , date
import time
from dateutil.relativedelta import relativedelta
import re

class EmployeeLoanAdvances(models.Model):
    _name = 'contractor.advances'
    _inherit = ['mail.thread']

    name = fields.Many2one('res.partner', required = "1" ,track_visibility='onchange')
    e_name = fields.Char("Contractor Name")
    relation = fields.Char("S/O,D/O,W/O")
    cnic = fields.Char("CNIC")
    remark_des = fields.Char(string="Remarks",track_visibility='onchange')
    employee = fields.Many2one('hr.employee',string="Employee",required=True ,track_visibility='onchange')
    date = fields.Date(default=datetime.today() ,track_visibility='onchange')
    d_date = fields.Date(string="Deduction Date" ,track_visibility='onchange')
    loan_start_date = fields.Date(string = "Loan Start Date" ,track_visibility='onchange')
    j_entry= fields.Many2one('account.move',string="Journal Entry")
    loan_end_date = fields.Date(string = "Loan End Date" ,track_visibility='onchange')
    interval_number = fields.Integer(string = "Interval Number" ,track_visibility='onchange')
    no_of_installments = fields.Integer(string = "No of Installments" ,track_visibility='onchange' , readonly = True)
    installment_amount = fields.Float(string = "Installment Amount" ,track_visibility='onchange')
    amount = fields.Float(track_visibility='onchange' , copy=False)
    aprr_amount = fields.Float(string="Approved Amount" ,track_visibility='onchange')
    received = fields.Float(track_visibility='onchange' , copy=False)
    remaining = fields.Float(track_visibility='onchange' , copy=False)
    set_data = fields.Boolean("set data" , copy=False)
    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')


    valiated_bol = fields.Boolean("set data" , copy=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('approved', 'Approved By HR'),
        ('verify_acounts', 'Verified by Accounts'),
        ('validate', 'Validated'),
        ('cancel', 'cancelled'),
        ],string = "Stages", default = 'draft' ,track_visibility='onchange')
    unit = fields.Selection([
        ('week', 'Weeks'),
        ('months', 'Months'),
        ],string = "Unit" ,track_visibility='onchange' , default="months")
    type = fields.Many2one('payment.type.class',string = "Type", required = "1" ,track_visibility='onchange')
    debit_account = fields.Many2one('account.account' ,track_visibility='onchange')
    payment = fields.One2many('contractor.advances.lines','payment_id')
    receipts = fields.One2many('contractor.advances.lines','receipt_id')
    loan_lines = fields.One2many('loan.lines','contract_id')
    loan_deduct = fields.One2many('loan.deduct','contract_id')

    @api.onchange('employee')
    def get_partner(self):
        if self.employee:
            self.name = self.employee.partner_id.id
            # self.relation = self.employee.relation
            self.cnic = self.sudo().employee.identification_id
            if not self.branch:
                self.branch = self.employee.branch.id

    @api.onchange('cnic')
    def get_employee(self):
        if self.cnic:
            employee = self.env['hr.employee'].search([('identification_id','=',self.cnic)])
            self.employee = employee.id
            # self.relation = employee.relation

    def cnic_match(self,rec):
        pass
        # if rec.cnic:
        #     temp=str(rec.cnic)
        #     check=re.match('^[0-9]{5}-[0-9]{7}-[0-9]{1}$',temp)
        #     if not check:
        #         raise ValidationError("Wrong CNIC pattern")
        # else:
        #     print "___________________________________"

    @api.onchange('employee','type')
    def get_account(self):
        self.debit_account = self.type.employee_account.id
    
    @api.multi
    def get_loan(self):
        if self.state == "draft":
            self.loan_lines.unlink()
            self.loan_end_date = False
            start_date = datetime.strptime(str(self.loan_start_date),'%Y-%m-%d')

            installments = self.amount/self.installment_amount
            self.no_of_installments = installments

            if self.unit == 'months':
                for x in range(self.no_of_installments):
                    number = x * self.interval_number
                    start_date = datetime.strptime(str(self.loan_start_date),'%Y-%m-%d')
                    date = start_date + relativedelta(months=number)

                    self.env['loan.lines'].create({
                        'date':date,
                        'amount':self.installment_amount,
                        'contract_id':self.id,
                        })
                    self.loan_end_date = date

            elif self.unit == 'week':

                for x in range(self.no_of_installments):
                    number = x * self.interval_number
                    start_date = datetime.strptime(str(self.loan_start_date),'%Y-%m-%d')
                    date = start_date + relativedelta(weeks=number)

                    self.env['loan.lines'].create({
                        'date':date,
                        'amount':self.installment_amount,
                        'contract_id':self.id,
                        })
                    self.loan_end_date = date

    @api.multi
    def validate(self):
        self.state = "validate"
        self.valiated_bol = True
    
    @api.multi
    def is_cancelled(self): 
        self.state = "cancel"

    @api.multi
    def accounts_approve(self):
        self.state = "verify_acounts"

    @api.multi
    def apporve_by_hr(self):
        # self.state = "approved"
        self.state = "validate"
        self.valiated_bol = True

    @api.multi
    def reset_apporve_by_hr(self):
        # self.state = "approved"
        self.state = "draft"


    def check_today_entry(self,rec,date,emp,a_type):
        medical = self.env['contractor.advances'].search([('id','!=',rec),('date','=',date),('employee','=',emp.id),('type','=',a_type.id),('state','!=','cancel')])
        if medical:
            raise ValidationError("Advance Already Exist for Same Date")



    @api.model
    def create(self, vals):
        new_record = super(EmployeeLoanAdvances, self).create(vals)
        new_record.check_today_entry(new_record.id,new_record.date,new_record.employee,new_record.type)
        new_record.check_employee_type()
        self.cnic_match(new_record)

        return new_record

    @api.multi
    def write(self, vals):
        before=self.write_date
        result = super(EmployeeLoanAdvances, self).write(vals)
        after = self.write_date
        if before != after:
            # self.check_employee_type()
            self.cnic_match(self)

        return result

    def check_employee_type(self):
        pass
        # record= self.env['contractor.advances'].search([('employee','=',self.employee.id),('type','=',self.type.id),('id','!=',self.id)])
        # if record:
        #     raise ValidationError("Already Create Same Employee and Same Type")


    @api.multi
    def set_data_button(self):
        record= self.env['contractor.advances'].search([])
        for x in record:
            x.e_name = x.employee.name
            # if x.payment:
            #   for p in x.payment:
            #       if p.entry_id:
            #           x.set_data = True
            # if x.receipts:
            #   for r in x.receipts:
            #       if r.entry_id:
            #           x.set_data = True


    @api.multi
    def draft(self):
        self.state = "draft"

    @api.multi
    def fetch_deduct(self):

        self.loan_deduct.unlink()

        payslip = self.env['hr.payslip'].search([('date_from','>=',self.loan_start_date),('date_from','<',self.loan_end_date),('date_to','>',self.loan_start_date),('date_to','<=',self.loan_end_date),('employee_id.id','=',self.employee.id),('state','!=','cancel')])
        print payslip
        print "check 1"
        for x in payslip:
            for y in x.line_ids:
                if self.type.name == 'Employees Temp Advances':
                    if y.name == 'Advance':
                        deducts = self.env['loan.deduct'].search([('payslip_id.id','=',y.id)])
                        if not deducts:
                            if y.amount > 0:
                                deduction = self.env['loan.deduct'].create({
                                    'date': x.date_from,
                                    'amount': y.amount,
                                    'payslip_id': y.id,
                                    'contract_id': self.id
                                    })
                else:
                    if y.name == 'Loan Deduction':
                        deducts = self.env['loan.deduct'].search([('payslip_id.id','=',y.id)])
                        if not deducts:
                            if y.amount > 0:
                                deduction = self.env['loan.deduct'].create({
                                    'date': x.date_from,
                                    'amount': y.amount,
                                    'payslip_id': y.id,
                                    'contract_id': self.id
                                    })
        self.getamount()

    @api.onchange('payment','aprr_amount')
    def getpayment(self):
        total_amount = 0
        for amount in self.payment:
            total_amount = total_amount + amount.amount
        self.amount = total_amount
        self.remaining = self.amount - self.received

    @api.onchange('receipts','loan_deduct','aprr_amount')
    def getamount(self):
        total_amount = 0
        deduct = 0
        
        if self.receipts:
            for amount in self.receipts:
                total_amount = total_amount + amount.amount
        
        if self.loan_deduct:
            for salry_deduct in self.loan_deduct:
                if salry_deduct.payslip_id:
                    deduct = deduct + salry_deduct.amount
                
        self.received = total_amount + deduct
        self.remaining = self.amount - self.received 

    @api.multi
    def unlink(self):

        if self.state != "draft":
            raise ValidationError('Cannot Delete Entry')

        if self.valiated_bol == True:
            raise ValidationError('Cannot Delete Entry')
            
        super(EmployeeLoanAdvances, self).unlink()

class LoanLines(models.Model):
    _name = 'loan.lines'

    date = fields.Date(required = "1")
    amount = fields.Float()
    contract_id = fields.Many2one('contractor.advances',string="Contractor")

class LoanDeductLines(models.Model):
    _name = 'loan.deduct'

    date = fields.Date(required = "1",string="Date")
    amount = fields.Float(string="Amount")
    contract_id = fields.Many2one('contractor.advances',string="Contractor")
    payslip_id = fields.Many2one('hr.payslip.line',string="Payslip Line")

    @api.multi
    def write(self, vals):

        super(LoanDeductLines, self).write(vals)
        if not self.payslip_id:
            self.unlink()
        return True

class ContractorAdvancesLines(models.Model):
    _name = 'contractor.advances.lines'

    date = fields.Date(default=datetime.today(),required = "1")
    amount = fields.Float()
    account_id = fields.Many2one('account.account', string = "Account")
    journal_id = fields.Many2one('account.journal', string = "Journal")
    entry_id = fields.Many2one('account.move', string = "Entry")
    payment_id = fields.Many2one('contractor.advances')
    receipt_id = fields.Many2one('contractor.advances')
    remark_des = fields.Char(string="Remarks")


    @api.multi
    def button_print(self):
        return self.env['report'].get_action(self,'advances_payment_voucher.general_journal')



    def validate_payments(self):
        journal_entries = self.env['account.move']
        journal_entries_lines = self.env['account.move.line']
        if not self.entry_id:
            self.payment_id.set_data = True
            create_journal_entry = journal_entries.create({
                    'journal_id': self.journal_id.id,
                    'date':self.date,
                    'module': 'Advances'
                    })
            self.entry_id = create_journal_entry.id
            self.creat_debit_credit()
            create_journal_entry.post()
        else:
            self.entry_id.button_cancel()
            for lines in self.entry_id.line_ids:
                lines.unlink()
            self.creat_debit_credit()
            self.entry_id.post()

    def validate_receipts(self):
        journal_entries = self.env['account.move']
        journal_entries_lines = self.env['account.move.line']
        if not self.entry_id:
            self.receipt_id.set_data = True
            create_journal_entry = journal_entries.create({
                    'journal_id': self.journal_id.id,
                    'date':self.date,
                    'module': 'Advances received'                   
                    })
            self.entry_id = create_journal_entry.id
            self.creat_credit_debit()
            create_journal_entry.post()
        else:
            self.entry_id.button_cancel()
            for lines in self.entry_id.line_ids:
                lines.unlink()
            self.creat_credit_debit()
            self.entry_id.post()

    @api.multi
    def unlink(self):
        if self.entry_id:

            self.entry_id.unlink()
            
        super(ContractorAdvancesLines, self).unlink()

            

    def creat_credit_debit(self):
        debit = self.create_entry_lines(self.account_id.id,self.amount,0,self.entry_id.id)
        credit = self.create_entry_lines(self.receipt_id.debit_account.id,0,self.amount,self.entry_id.id)


    def creat_debit_credit(self):
        debit = self.create_entry_lines(self.payment_id.debit_account.id,self.amount,0,self.entry_id.id)
        credit = self.create_entry_lines(self.account_id.id,0,self.amount,self.entry_id.id)
        
    def create_entry_lines(self,account,debit,credit,entry_id):
        if self.payment_id:
            name = self.payment_id.name.name
            partner = self.payment_id.name.id
        if self.receipt_id:
            name = self.receipt_id.name.name
            partner = self.receipt_id.name.id
        self.env['account.move.line'].create({
                'account_id':account,
                'partner_id':partner,
                'name':name,
                'debit':debit,
                'credit':credit,
                'move_id':entry_id,
                })

class PaymentTypes(models.Model):
    _name = "payment.type.class"

    name = fields.Char(string="Name")
    employee_account = fields.Many2one('account.account',string="Employee Account")

class MedicalBulkApproval(models.TransientModel):
    _name = "advance.bulk.approval"

    journal_id = fields.Many2one('account.journal',string="Journal")
    account_id = fields.Many2one('account.account',string="Debit Account")
    credit_account = fields.Many2one('account.account',string="Credit Account")

    @api.multi
    def apporve_by_hr(self):
        docs = self.env['contractor.advances'].browse(self.env.context.get('active_ids'))
        for rec in docs:
            if rec.state == 'draft':
                rec.state = 'approved'

    @api.multi
    def accounts_approve(self):
        docs = self.env['contractor.advances'].browse(self.env.context.get('active_ids'))
        for rec in docs:
            # if rec.state == "approved":
            rec.state = 'verify_acounts'

    @api.multi
    def verify_records(self):

        if not self.account_id:
            raise ValidationError("Debit Account Field Can't be empty.")
        if not self.credit_account:
            raise ValidationError("Credit Account Can't be empty.")
        if not self.journal_id:
            raise ValidationError("Journal Field Can't be empty.")

        docs = self.env['contractor.advances'].browse(self.env.context.get('active_ids'))
        clubing = [line.id for line in docs if line.state == 'verify_acounts']
        verified_by_accounts_records = self.env['contractor.advances'].search([('id','in',clubing)])
        contains_je = self.env['contractor.advances'].search([('id','in',clubing),('j_entry','!=',False)])

        if len(verified_by_accounts_records) != len(docs):
            raise ValidationError("Some of the records have not been Verified by the Accounts Department.")

        if contains_je:
            raise ValidationError("Some of the records already contain a Journal Entry. This transaction can't be completed.")

        total_amount = sum(medical.aprr_amount for medical in verified_by_accounts_records)

        entry = self.env['account.move'].create({
            'journal_id': self.journal_id.id,
            'date': datetime.now(),
            'ref': "Advance/Loan to employees against salary for the month of " + str(time.strftime('%B',time.strptime(str(date.today()),'%Y-%m-%d')))
        })

        debit_entry = self.env['account.move.line'].create({
            'account_id': self.account_id.id, 
            'credit': 0,
            'debit': total_amount,
            'company_id': self.env['res.company'].search([]).id,
            'date_maturity': datetime.now(),
            'name': "Advance reimbursement",
            'move_id': entry.id
        })

        credit_entry = self.env['account.move.line'].create({
            'account_id': self.credit_account.id, 
            'credit': total_amount,
            'debit': 0,
            'company_id': self.env['res.company'].search([]).id,
            'date_maturity': datetime.now(),
            'name': "Advance reimbursement",
            'move_id': entry.id
        })
        
        for medical in verified_by_accounts_records:
            medical.j_entry = entry.id
            medical.state = 'validate'
