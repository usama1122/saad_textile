# -*- coding: utf-8 -*-
{
    'name': "Employee Loan Advances",

    'summary': """
        Module for Employee Loan Advances""",

    'description': """
        Module for Employee Loan Advances
    """,

    'author': "Enterprise Cube (Pvt) Limited",
    'website': "http://ecube.pk",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','account','hr_payroll'],

    # always loaded
    'data': [
        'views/templates.xml',
    ],
}
