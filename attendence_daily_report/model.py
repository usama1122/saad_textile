#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################
from openerp import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime

class SampleDevelopmentReport(models.AbstractModel):
	_name = 'report.attendence_daily_report.customer_report'

	@api.model
	def render_html(self,docids, data=None):
		report_obj = self.env['report']
		report = report_obj._get_report_from_name('attendence_daily_report.customer_report')
		active_wizard = self.env['dailyreport.attend'].search([])
		emp_list = []
		for x in active_wizard:
			emp_list.append(x.id)
		emp_list = emp_list
		emp_list_max = max(emp_list) 

		record_wizard = self.env['dailyreport.attend'].search([('id','=',emp_list_max)])

		record_wizard_del = self.env['dailyreport.attend'].search([('id','!=',emp_list_max)])
		record_wizard_del.unlink()


		typed = record_wizard.typed
		branch = record_wizard.branch
		employee = record_wizard.employee
		department = record_wizard.department
		date = record_wizard.date

		print branch
		print typed
		print "--------------"
		




		if typed == 'all_dept':
			records = []
			rec = self.env['hr.department'].search([('branch','=',branch.id)])
			for x in rec:
				records.append(x)


		if typed == 'department':
			records = []
			for x in record_wizard.department:
				records.append(x)


		if typed == 'department' or typed == 'all_dept':
			enteries = []
			def get_record(attr):
				del enteries [:]
				rec = self.env['actual.attendence'].search([('employee_id.department_id.id','=',attr),('date','=',record_wizard.date),('branch','=',branch.id)])
				for x in rec:
					enteries.append(x)



		if typed == 'all_emp':
			records = []
			rec = self.env['hr.employee'].search([('branch','=',branch.id)])
			for x in rec:
				records.append(x)

		print "check 1"
		print branch
		# print rec
		if typed == 'specific':
			records = []
			for x in record_wizard.employee:
				records.append(x)


		if typed == 'all_emp' or typed == 'specific':
			enteries = []
			def get_record(attr):
				del enteries [:]
				rec = self.env['actual.attendence'].search([('employee_id.id','=',attr),('date','=',record_wizard.date),('branch','=',branch.id)])
				for x in rec:
					enteries.append(x)




		def get_name():
			num = 0
			if typed == 'all_emp' or typed == 'specific':
				num = 1
			if typed == 'department' or typed == 'all_dept':
				num = 2


			return num


		def get_check(idz,attr):
			
			attendence = self.env['actual.attendence'].search([('id','=',idz)])
			if attr == 'entry':
				if attendence.checkin:
					return '{0:02.0f}:{1:02.0f}'.format(*divmod(attendence.checkin * 60, 60))
				else: 
					return "-"
			
			if attr == 'exit':
				if attendence.checkout:
					return '{0:02.0f}:{1:02.0f}'.format(*divmod(attendence.checkout * 60, 60))
				else: 
					return "-"

			if attr == 'time':
				if attendence.total_time:
					return '{0:02.0f}:{1:02.0f}'.format(*divmod(attendence.total_time * 60, 60))
				else: 
					return "-"



		

		docargs = {
		
			'doc_ids': docids,
			'doc_model': 'actual.attendence',
			'date': date,
			'records': records,
			'enteries': enteries,
			'get_record': get_record,
			'get_check': get_check,
			'get_name': get_name,
			
	
			}

		return report_obj.render('attendence_daily_report.customer_report', docargs)