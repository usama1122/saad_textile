# #-*- coding:utf-8 -*-
from odoo import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime
from openerp.exceptions import Warning, ValidationError, UserError

class EmployeeAbsentReport(models.TransientModel):
	_name = "employee.absent.report"

	employee = fields.Many2many('hr.employee',string="Employee")
	department = fields.Many2one('hr.department',string="Department")
	shift = fields.Many2one('shifts.attendence',string="Shift")
	form = fields.Date(string="From")
	to = fields.Date(string="To")
	typed = fields.Selection([
		('shift_wise','Shift Wise'),
		('department','Department Wise'),
		('specific','Specific'),
		],string='Employee(s)',)

	@api.multi
	def print_report(self):
		data = {}
		data['form'] = self.read(['employee','department','form','to','typed'])[0]
		return self.env['report'].get_action(self, 'employee_absent_report.absent_report', data=data)