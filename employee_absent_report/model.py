#-*- coding:utf-8 -*-
from openerp import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime
import re

class EmployeeWiseAttendence(models.AbstractModel):
	_name = 'report.employee_absent_report.absent_report'

	@api.model
	def render_html(self,docids, data=None):
		report_obj = self.env['report']
		record_wizard = self.env['employee.absent.report'].browse(self.env.context.get('active_ids'))
		company = self.env['res.company'].search([])

		form = record_wizard.form
		to = record_wizard.to
		typed = record_wizard.typed
		department = record_wizard.department
		shift = record_wizard.shift

		d1 = datetime.datetime.strptime(record_wizard.form, "%Y-%m-%d")
		d2 = datetime.datetime.strptime(record_wizard.to, "%Y-%m-%d")

		delta = d2 - d1
		dates = []
		for i in range(delta.days + 1):
			dates.append((d1 + timedelta(days=i)).strftime('%Y-%m-%d'))


		if typed == 'specific':
			employees = []
			for x in record_wizard.employee:
				employees.append(x)

		if typed == 'department':
			employees = self.env['hr.employee'].search([('department_id.id','=',department.id)])

		if typed == 'shift_wise':
			employees = self.env['hr.employee'].search([('schedule.id','=',shift.id)])

		main_lisst = []
		for d in dates:
			if typed == 'department':
				# print "Dept"
				actual_att = self.env['actual.attendence'].search([('employee_id.department_id','=',department.id),('date','=',d),('todaystatus','=','absent')])
			elif typed == 'shift_wise':
				# print "Spec"
				actual_att = []
				for e in employees:
					record = self.env['actual.attendence'].search([('employee_id','=',e.id),('date','=',d),('todaystatus','=','absent')])
					actual_att.append(record)
			elif typed == 'specific':
				# print "Spec"
				actual_att = []
				for e in employees:
					record = self.env['actual.attendence'].search([('employee_id','=',e.id),('date','=',d),('todaystatus','=','absent')])
					actual_att.append(record)
			else:
				# print "Else"
				actual_att = self.env['actual.attendence'].search([('date','=',d),('todaystatus','=','absent')])

			# for emp in actual_att:
			# 	if emp.todaystatus == 'absent':
			# 		emp_lisst.append({
			# 			'name':emp.employee_id.name,
			# 			'department':emp.employee_id.department_id.name,
			# 			'job':emp.employee_id.job_id.name,
			# 			'status':emp.todaystatus,
			# 			})
			emp_lisst=[{
				'name':emp.employee_id.name,
				'department':emp.employee_id.department_id.name,
				'code':emp.employee_id.emp_code,
				'job':emp.employee_id.job_id.name,
				'status':emp.todaystatus,
				}for emp in actual_att if emp.todaystatus == 'absent'] 
			emp_lisst = sorted(emp_lisst)
			# print emp_lisst
			main_lisst.append({
				'date':d,
				'employees':emp_lisst,
				})

		

		docargs = {
			'doc_ids': docids,
			'doc_model': 'hr.employee',
			'data': data,
			'form': form,
			'to': to,
			'main_data':main_lisst,
			'company': company,
			'shift':shift,
			}

		return self.env['report'].render('employee_absent_report.absent_report', docargs)