# -*- coding: utf-8 -*-
{
    'name': "Employee Absent Report",

    'summary': "Employee Absent Report",

    'description': "Employee Absent Report",

    'author': "Siddiq Chauhdry",
    # 'website': "http://www.bcube.pk",

    # any module necessary for this one to work correctly
    'depends': ['base','web','report','hr','shift_schedule'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}
