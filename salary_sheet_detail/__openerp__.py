# -*- coding: utf-8 -*-

{
    'name': "Salary Sheet Detail....",

    'summary': "Salary Sheet Detail",

    'description': "Salary Sheet Detail",

    'author': "Muhammmad Kamran",
    'website': "http://www.ecube.pk",

    # any module necessary for this one to work correctly
    'depends': ['base', 'report','account','hr','hr_payroll'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}