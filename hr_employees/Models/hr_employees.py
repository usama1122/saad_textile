from odoo import models, fields, api
import datetime
from datetime import date, datetime, timedelta
from dateutil import relativedelta as rdelta
import time
import calendar
from openerp.exceptions import ValidationError

class PartnerExtend_Ext(models.Model):
    _inherit = 'res.partner'

    taxpayer_business_name = fields.Char()
    employee = fields.Boolean(string="Employee")

class hr_custom_job_id(models.Model):
    _inherit = 'hr.job'

    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

class hr_custom_department(models.Model):
    _inherit = 'hr.department'

    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

class hr_custom_employee(models.Model):
    _inherit = 'hr.employee'
    
    stg_activ_code= fields.Char('STG Active Code')
    eobi_no = fields.Char('EOBI No', track_visibility='onchange')
    blood_group = fields.Selection([('o_positive', 'O+'), ('o_negative', 'O-'), ('a_positive', 'A+'), ('a_negative', 'A-'),('b_positive', 'B+'), ('b_negative', 'B-'), ('ab_positive', 'AB+'), ('ab_negative', 'AB-')], 'Blood Group',select=True)
    joining_date = fields.Date('Date of Joining', track_visibility='onchange')
    confirm_date = fields.Date('Date of Confirmation')
    end_date = fields.Date('End Date')
    emp_id = fields.Char('Employee Code', track_visibility='onchange')
    iban_no = fields.Char('IBAN Number')
    home_town = fields.Char('Home Town')
    first_email = fields.Char('Email', track_visibility='onchange')
    second_email = fields.Char('Secondary Email', track_visibility='onchange')
    email_password = fields.Char('Password', track_visibility='onchange')
    total_experience = fields.Integer('Total Experience  :  Years:')
    cricket_allowance = fields.Float(string="Cricket Allowance")
    service_period = fields.Char('Service Period')
    last_working_day = fields.Char('Last Working Day')
    accounts_and_finance = fields.Char('Accounts & Finance', track_visibility='onchange')
    administration_and_hrm = fields.Char('Administration & HRM')
    it_dept = fields.Char('IT Department')
    site_settlement = fields.Char('Site Settlement')
    hse_dept = fields.Char('HSE Department')
    security_settlement = fields.Char('Security Settlement')
    others_if_any = fields.Char('Others (if any)')
    validation_date = fields.Date('CNIC Validity')
    emp_code = fields.Char(string="Employee Code")
    health_insurance = fields.Selection([
            ('a', 'A'),
            ('b', 'B'),
            ('c', 'C'),
            ('d', 'D'),
            ],)
    
    official_email = fields.Char("Official Email", track_visibility='onchange')
    insider_outsider = fields.Selection([
            ('insider','Insider'),
            ('outsider', 'Outsider'),
            ],string="Insider/Outsider")
    religion = fields.Selection([
            ('muslim','Muslim'),
            ('non_muslim','Non Muslim'),
            ],string="Religion")
    emergency_name = fields.Char("Name")
    emergency_cell_number = fields.Char("Contact Number")
    emergency_relation = fields.Char("Relation")
    emergency_address = fields.Char("Current Address")
    par_address = fields.Char("Permanent Address")
    life_insurance = fields.Boolean("Life Insurance")
    daily_wager = fields.Boolean("Daily Wager")
    phone = fields.Char("Phone")
    mobile_num = fields.Char("Cell No.")
    father_name = fields.Char("Name")
    father_cnic = fields.Char("CNIC")
    father_dob = fields.Date('DOB')
    title=fields.Many2one('res.partner.title',string="Name Title")
    card_no=fields.Many2one('emp.card.num',string="Card No")
    live_status = fields.Selection([
            ('alive', 'Alive'),
            ('dead', 'Deceased'),
            ], string="Status")
    father_opp = fields.Char("Occupation")
    curr_emp_father = fields.Boolean('Currently Employed')
    mother_name = fields.Char("Name")
    mother_cnic = fields.Char("CNIC")
    mother_dob = fields.Date('DOB')
    mother_status = fields.Selection([
            ('alive', 'Alive'),
            ('dead', 'Deceased'),
            ], string="Status")

    mother_opp = fields.Char("Occupation")
    curr_emp = fields.Boolean('Currently Employed')
    provident_fund =fields.Boolean("Provident Fund")
    medical_facilities =fields.Boolean("Medical Facilities")
    xx_mobile_phone = fields.Boolean('Mobile Phone')
    xx_laptop = fields.Boolean('Laptop')
    fuels = fields.Boolean('Fuel')
    xx_fuel = fields.Boolean('Fuel')
    xx_car = fields.Boolean('Company Car')

    hafiz_allo = fields.Boolean('Hafiz Allowance', track_visibility='onchange')
    tardenenss_check = fields.Boolean('Tardenenss', track_visibility='onchange', default=True)
    leave_incentive = fields.Boolean('Leave Incentive', track_visibility='onchange',default=True)
    eobi = fields.Boolean('EOBI', track_visibility='onchange',default=True)
    marriage_allo = fields.Boolean('Marriage Allowance', track_visibility='onchange')
    night_allo = fields.Boolean('Night Allowance', track_visibility='onchange')
    lunch_allo = fields.Boolean('Lunch Allowance', track_visibility='onchange')
    eobi_bool = fields.Boolean('EOBI', track_visibility='onchange')
    social_securtiy = fields.Boolean('Social Securtiy', track_visibility='onchange')
    group_life_insr = fields.Boolean('Group Life Insurance', track_visibility='onchange')
    medical = fields.Boolean('Medical', track_visibility='onchange')
    
    xx_salary = fields.Float(string="Salary")
    cost_centre = fields.Many2one('cost_centre.cost_centre')
    working_adress = fields.Char("Working Address")
    employee_qualify_id = fields.One2many('employee.qualification','employee_qualification_id',string='Details')
    employee_certify_id = fields.One2many('employee.certification','employee_certification_id',string='Details')
    family_tree_id = fields.One2many('family.tree','employee_family_id',string='Details')
    employee_expert_id = fields.One2many('employee.experience','employee_experience_id',string='Details')
    company_provided_assets = fields.One2many('company.assets','company_assets',string='Company Provided Assets')
    spouse_tree_id = fields.One2many('spouse.tree','spouse_tree')
    contract_type = fields.Many2one('hr.contract.type')
    struct_id = fields.Many2one('hr.payroll.structure')
    schedule_pay = fields.Selection([
        ('monthly','Monthly'),
        ('quarterly','Quarterly'),
        ('semi-annually','Semi-annually'),
        ('annually','Annually'),
        ('weekly','Weekly'),
        ('bi-weekly','Bi-weekly'),
        ('bi-monthly','Bi-monthly'),
        ],string="Scheduled Pay",default='monthly')

    lunch_section = fields.Selection([
        ('one_time','One Time'),
        ('two_time','Two Time'),
        ],string="Lunch Time", track_visibility='onchange')
    
    marriage_date_rec = fields.Many2many('marriage.date.ext', string="Marriage Date", track_visibility='onchange')
    gross_opening = fields.Float(string="Gross Opening", track_visibility='onchange')
    gratuity = fields.Float(string="Gratuity Opening", track_visibility='onchange')
    tax_opening = fields.Float(string="Tax Opening", track_visibility='onchange')
    opening_amount=fields.Float("Opening Medical Amount",track_visibility='onchange')
    remaing_amount=fields.Float("Remaing Medical Amount")
    overtime_hour=fields.Datetime("Overtime Hours")
    overtime_amount=fields.Float("Overtime Amount")
    gros_salary=fields.Float("Gross Salary")
    advance=fields.Float("Advance")
    total_loan=fields.Float("Total Loan")
    deduct_loan=fields.Float("Deduct Loan")
    remaining_loan=fields.Float("Remaining Loan")
    canteen=fields.Float("Canteen")
    mess=fields.Float("Mess")
    pf_emp=fields.Float("P.F EMP")
    pf_co=fields.Float("P.F CO.")
    total_pf=fields.Float("Total P.F")
    net_salary=fields.Float("Net Salary")
    absent =fields.Float("Absent")
    leave=fields.Float("Leave")
    present=fields.Float("Present")
    lunch=fields.Float("Lunch")
    # remaing_amount=fields.Float("Remaing Medical Amount",compute='_get_remaining_medical_amount')
    can_approve_leave = fields.Boolean('Approve own leave', track_visibility='onchange')


    partner_id = fields.Many2one('res.partner',string="Related Partner" ,track_visibility='onchange')
    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

    @api.multi
    def creating_users(self):
        if self.name and self.emp_code and self.joining_date:   
            self.CreateDateWiseRecords_employee(self.joining_date)
        else:
            if not self.emp_code:
                raise ValidationError('Please enter employee code.')
            if not self.joining_date:
                raise ValidationError('Please enter employee date of joining.')



                









    def CreateDateWiseRecords_employee(self,lastdate):

        print "function start"
        print lastdate
        print lastdate

        today_date = datetime.today().strftime('%Y-%m-%d')
        today = datetime.strptime(today_date, "%Y-%m-%d")
        lastdate = datetime.strptime(lastdate, "%Y-%m-%d")

        if lastdate <= today:
            while (lastdate <= today):
                
                # print "lastdate lastdate lastdate lastdate "
                # create_date = self.env['dates.attendence'].create({
                #   'date': lastdate,
                # })

                employees = self.env['hr.employee'].search([('active','=',True),('id','=',self.id)])
                date = lastdate

                for employee in employees:
                    actual = self.env['actual.attendence'].search([('date','=',date),('employee_id','=',employee.id)])
                    print date
                    print actual
                    if not actual:
                        create_date = actual.sudo().create({
                            'employee_id': employee.id,
                            'card_no': employee.emp_code,
                            'date': date,
                            'shift': employee.schedule.name,
                            'shift_link': employee.schedule.id,
                            'shift_work_hour': employee.schedule.shift_work_hour,
                        })

                        create_date.set_shift()

                lastdate = lastdate + timedelta(days=1)

    @api.multi
    @api.depends('name', 'emp_code')
    def name_get(self):
        result = []
        for rec in self:
            self.env.uid
            if self.env.uid == 1:
                name = str(rec.emp_code)+' - '+str(rec.name)
                result.append((rec.id, name))
            else:
                name = str(rec.emp_code)+' - '+str(rec.name)
                result.append((rec.id, name))
            
        return result




    @api.model
    def _name_search(self, name='', args=None, operator='ilike', limit=100, name_get_uid=None):
        if args is None:
            args = []

        domain = args + ['|',('emp_code',operator,name),('name',operator,name)]

        return super(hr_custom_employee, self).search(domain,limit=limit).name_get()
    

    @api.multi
    def create_update_employee_contract(self,):
        contract = self.env['hr.contract'].search([('employee_id','=',self.id)])
        if contract:
            for x in reversed(contract):
                x.name = self.name
                x.wage = self.xx_salary
                x.struct_id = self.struct_id.id
                x.job_id = self.job_id.id
                x.department_id = self.department_id.id
                x.type_id = self.contract_type.id
                x.schedule_pay = self.schedule_pay
                break
        else:
            create_contract = self.env['hr.contract'].create({
                'name': self.name,
                'employee_id': self.id,
                'job_id': self.job_id.id,
                'department_id': self.department_id.id,
                'type_id': self.contract_type.id,
                'wage': self.xx_salary,
                'schedule_pay': self.schedule_pay,
                'struct_id': self.struct_id.id
            })

    def tax_calculation(self,payslip):
        amt=self.env['hr.payslip'].get_pre_ded_batch(payslip.date_to,payslip.employee_id,payslip)
        if amt > 0:
            result=amt
            return result
        else:
            salary = 0
            prv_gross=self.env['hr.payslip'].get_gross_amount(payslip.date_to,payslip.employee_id)
            r_month=self.env['hr.payslip'].get_remaining_months(payslip.date_to)
            salary =prv_gross+0+(0)*r_month
            if salary < 600001:
                result  = 0
                
            if salary > 600000 and salary < 1200001:
                exced_amt = salary - 600000 
                result  = (exced_amt * .05)

            if salary > 1200000 and salary < 1800001:
                exced_amt = salary - 1200000
                result = ((exced_amt * .1) + 30000)

            if salary > 1800000 and salary < 2500001:
                exced_amt = salary - 1200000
                result = ((exced_amt * .15) + 90000)

            if salary > 2500000 and salary < 3500001:
                exced_amt = salary - 2500000
                result = ((exced_amt * .175) + 195000)

            if salary > 3500000 and salary < 5000001:
                exced_amt = salary - 3500000
                result = ((exced_amt * .2) + 370000)

            if salary > 5000000 and salary < 8000001:
                exced_amt = salary - 5000000
                result = ((exced_amt * .225) + 670000)

            if salary > 8000000 and salary < 12000001:
                exced_amt = salary - 8000000
                result = ((exced_amt * .25) + 1345000)

            if salary > 12000000 and salary < 30000001:
                exced_amt = salary - 12000000
                result = ((exced_amt * .275) + 2345000)

            if salary > 30000000 and salary < 50000001:
                exced_amt = salary - 30000000
                result = ((exced_amt * .3) + 7295000)

            if salary > 50000000 and salary < 75000001:
                exced_amt = salary - 50000000
                result = ((exced_amt * .325) + 13295000)

            if salary > 75000000:
                exced_amt = salary - 75000000
                result = ((exced_amt * .35) + 21420000)

            final_tax = 0
            prv_ded = payslip.env['hr.payslip'].get_prv_ded_tax(payslip.date_to,payslip.employee_id)
            final_tax = (result - prv_ded)/(r_month+1)
            result = final_tax
            return result

    def loan_payments(self, payslip):
        advances = self.env['contractor.advances'].sudo().search([('employee', '=', payslip.employee_id),('state','=','validate')])
        qty = 0
        for y in advances:
            if y.type.name == "Loan Deduction":
                for x in y.loan_lines:
                    if x.date >= payslip.date_from and x.date <= payslip.date_to:
                        qty = qty + x.amount
        return qty


    def bouns_amount(self, payslip):
        

        bonus_rec = self.env['hr.bonus'].search([('state','=','validate'),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])
        
        result = 0
        employee_joining_date = self.env['hr.employee'].search([('id','=',payslip.employee_id)])
        
        for rec in bonus_rec:
            if rec:
                

                # if rec.date >= payslip.date_from and rec.date <= payslip.date_to:
                joining_date_of_employee = employee_joining_date.joining_date
                basic=0
                if joining_date_of_employee:
                    d1 = datetime.strptime(joining_date_of_employee, "%Y-%m-%d")
                    d2 = datetime.strptime(rec.month_date, "%Y-%m-%d") 
                    no_of_months = abs((d1.year - d2.year) * 12 + d1.month - d2.month)
                    basic = payslip.contract_id.wage+payslip.contract_id.group_amount
                    if no_of_months < 12:

                        basic = (payslip.contract_id.wage+payslip.contract_id.group_amount)/12
                        result = basic * no_of_months
                        description=str(no_of_months)+" Months."
                    else:
                        result =basic
                        description = "More then 1 year."
                for line in rec.tree_link:
                    if payslip.employee_id == line.employee_id.id:
                        line.unlink()
            rec.write({'tree_link': [(0,0, { 
                    'employee_id':payslip.employee_id,
                    'joining_date': joining_date_of_employee,
                    'bonus_amount':result,
                    'description':description,
                                           })]
                })
        return result



    def Salary_Slip_Deduction_canteen(self, payslip):
        duration = 0.0 
        timesheets = self.env['ecube.deduction'].search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('employee_id','=',self.id),('state','=','validate')])
        for x in timesheets:
            if x.type_id.name == "Asif Canteen":
                duration = duration + x.amount
        return duration



    def Salary_Slip_Deduction_mess(self, payslip):
        duration = 0.0 
        timesheets = self.env['ecube.deduction'].search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('employee_id','=',self.id),('state','=','validate')])
        for x in timesheets:
            if x.type_id.name == "Zaman Canteen":
                duration = duration + x.amount
        return duration



    def Salary_Slip_Deduction_overtime(self, payslip):
        print "1111111111"
        # duration = 0.0 
        # timesheets = self.env['ecube.deduction'].search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('employee_id','=',self.id),('state','=','validate')])
        # print "22222222222"
        # print "22222222222"
        # print timesheets
        # for x in timesheets:
        #     if x.type_id.name == "Overtime":
        #         duration = duration + x.amount
        # print duration
        # return duration



    def Salary_Slip_Deduction_hour(self, payslip):
        duration = 0.0 
        timesheets = self.env['ecube.deduction'].search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('employee_id','=',self.id),('state','=','validate')])
        for x in timesheets:
            if x.type_id.name == "Overtime":
                duration = duration + x.overtime_hours
        print duration
        return duration



    def Salary_Slip_Deduction_lunch(self, payslip):
        duration = 0.0 
        timesheets = self.env['ecube.deduction'].search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('employee_id','=',self.id),('state','=','validate')])
        for x in timesheets:
            if x.type_id.name == "Lunch":
                duration = duration + x.amount
        return duration


    def salary_advance_payments(self, payslip):
        advances = self.env['contractor.advances'].sudo().search([('employee', '=', payslip.employee_id),('state','=','validate')])
        print advances
        # if advances.remaining == 0:
        print "advance salary start"
        # if advances.
        # advances.getpayment()
        print "---------"
        amount = 0
        print "check a"
        for y in advances:
            y.getpayment()
            if y.type.name == "Advance Salary":
                print "-----"
                if y.date >= payslip.date_from and y.date <= payslip.date_to:
                    amount = amount + y.remaining
        print "0000000" 
        print amount       
        return amount


    @api.onchange('date')
    def update_month_days(self):
        if self.date:
            year  = int(self.date[:4])
            month = int(self.date[5:7])
            day = int(self.date[8:10])
            month_days =  calendar.monthrange(year,month)[1]
            diff_month_days = month_days - day
            self.month = str(calendar.month_name[month])+' '+str(year)

        else:
            self.month = False

    def absent_deduction(self, payslip):
        print "absent duction start"
        contract = self.env['hr.contract'].search([('employee_id','=',self.id)])
        print contract
        year  = int(payslip.date_from[:4])
        month = int(payslip.date_from[5:7])
        month_days =  calendar.monthrange(year,month)[1]
        par_day_salary = (contract.wage)/month_days
       
        print par_day_salary
        print par_day_salary
       
        total_days = contract.absent_days + (contract.half_days)/2 
        print total_days
        total_deduction = 0
        total_deduction = total_days * par_day_salary
        print total_deduction
        return total_deduction


    def Tardiness_deduction(self, payslip):
        result = 0

        employee = self.env['hr.employee'].sudo().search([('id','=',payslip.employee_id),('tardenenss_check','=',True)])
        if employee:
            late_arrival_tardiness_deduction = self.env['actual.attendence'].search([('intime','=','lin'),('todaystatus','=','present'),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])
            
            leaves = self.env['hr.holidays'].search_count([('state','=','validate'),('employee_id','=',payslip.employee_id),('holiday_status_id.name','in',['Short leave']),('date_from_ecube','>=',payslip.date_from),('date_from_ecube','<=',payslip.date_to)])

            total_late_minutes = 0
            if late_arrival_tardiness_deduction:
                total_late_minutes = sum([x.total_late_time for x in late_arrival_tardiness_deduction])
                total_late_minutes = ((total_late_minutes * 60))
                short_leave_adjusted_time = 0
                if leaves > 2:
                    short_leave_adjusted_time = 120 * 2
                else:
                    short_leave_adjusted_time = 120 * leaves

                basic_tardiness_time = 330 
                total_tardiness_time = basic_tardiness_time + short_leave_adjusted_time

                if total_late_minutes > total_tardiness_time:
                    Tardiness_deduction_minutes = total_late_minutes - total_tardiness_time
                    
                    per_day_salary = payslip.contract_id.wage / 26
                    per_minutes_salary = per_day_salary / 9 / 60
                    result = per_minutes_salary * Tardiness_deduction_minutes
        return round(result)


    def get_basic(self, payslip):
        result = 0

        joining_date_check = self.env['hr.employee'].search([('joining_date', '>=', payslip.date_from),('joining_date', '<=', payslip.date_to),('id','=',self.id)])
        if joining_date_check:

            date_join = datetime.strptime(joining_date_check.joining_date,"%Y-%m-%d")
            month_start_date = datetime.strptime(payslip.date_from,"%Y-%m-%d")
            month_end_date = datetime.strptime(payslip.date_to,"%Y-%m-%d")
            timedelta = date_join - month_start_date
            month_days = month_end_date - month_start_date
            days = timedelta.days
            month_days = month_days.days + 1
            working_days = month_days - days
            basic = ((payslip.contract_id.wage)/month_days) * working_days
            return round(basic)

        basic = payslip.contract_id.wage
        return round(basic)

    def employee_present(self, payslip):


        employee_present = self.env['actual.attendence'].search_count([('todaystatus','=','present'),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])

        employee_present_holiday = self.env['actual.attendence'].search_count([('todaystatus','in',['holiday','gazetted_holiday']),('checkin','>',0),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])

        total_present = (employee_present + employee_present_holiday)
        
        employee = self.env['hr.employee'].search([('id','=',self.id)])
        if employee.daily_wager:
            return (total_present)
        
        if total_present:


            if total_present >= 6 and total_present <= 11:
                total_present = total_present + 1

            elif total_present >= 12 and total_present <= 17:
                total_present = total_present + 2

            elif total_present >= 18 and total_present <= 23:
                total_present = total_present + 3

            elif total_present >= 24 and total_present <= 30:
                total_present = total_present + 4

            elif total_present == 31:
                total_present = total_present + 5

        return (total_present)



    def employee_present_amount_rule(self, payslip):
        per_day = 0
        total_ampunt = 0
        employee_present = self.env['actual.attendence'].search_count([('todaystatus','=','present'),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])

        employee_present_holiday = self.env['actual.attendence'].search_count([('todaystatus','in',['holiday','gazetted_holiday']),('checkin','>',0),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])

        total_present = (employee_present + employee_present_holiday)

        
        employee = self.env['hr.employee'].search([('id','=',self.id)])
        if employee.daily_wager:
            total_ampunt = (float(total_present) * float(employee.xx_salary))
            return total_ampunt

        if total_present:
            
            if total_present >= 6 and total_present <= 11:
                total_present = total_present + 1

            elif total_present >= 12 and total_present <= 17:
                total_present = total_present + 2

            elif total_present >= 18 and total_present <= 23:
                total_present = total_present + 3

            elif total_present >= 24 and total_present <= 30:
                total_present = total_present + 4

            elif total_present == 31:
                total_present = total_present + 5

        
        year  = int(payslip.date_from[:4])
        month = int(payslip.date_from[5:7])
        month_days =  calendar.monthrange(year,month)[1]

        per_day = payslip.contract_id.wage/month_days
        total_ampunt = (float(total_present) * float(per_day))
        
        return total_ampunt

    def employee_present_saad2(self, payslip):


        total_present = 0
        employee_present = self.env['actual.attendence'].search_count([('todaystatus','in',['present','annual_leaves','sick_leaves','casual_leaves']),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])

        employee = self.env['hr.employee'].search([('id','=',self.id)])

        employee_present_holiday = self.env['actual.attendence'].search_count([('todaystatus','in',['holiday','gazetted_holiday']),('employee_id','=',payslip.employee_id),('date','>=',employee.joining_date),('date','<=',payslip.date_to)])

        total_present = (employee_present + employee_present_holiday)
        return (total_present)



    def employee_present_amount_rule_saad2(self, payslip):
        per_day = 0
        total_amount = 0


        total_present = 0
        employee_present = self.env['actual.attendence'].search_count([('todaystatus','in',['present','annual_leaves','sick_leaves','casual_leaves']),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])

        employee = self.env['hr.employee'].search([('id','=',self.id)])

        employee_present_holiday = self.env['actual.attendence'].search_count([('todaystatus','in',['holiday','gazetted_holiday']),('employee_id','=',payslip.employee_id),('date','>=',employee.joining_date),('date','<=',payslip.date_to)])

        total_present = (employee_present + employee_present_holiday)
        per_day = payslip.contract_id.wage/26
        total_amount = (float(total_present) * float(per_day))
        return total_amount


    def employee_absent(self, payslip):

        employee_absent = self.env['actual.attendence'].search_count([('todaystatus','=','absent'),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])
        return employee_absent


    def hafiz_allowance(self, payslip,hafiz_allowance):
        result = 0
        
        hafiz_emplpyee = self.env['hr.employee'].sudo().search([('id','=',payslip.employee_id),('hafiz_allo','=',True)])
        if hafiz_emplpyee:
            result = hafiz_allowance
        return result

    def eobi_deduction(self, payslip,eobi_amount):
        result = 0
        
        emplpyee = self.env['hr.employee'].sudo().search([('id','=',payslip.employee_id),('eobi','=',True)])
        if emplpyee:
            result = eobi_amount
        return result


    def Assistant_QA(self, payslip,Assistant_QA):
        result = 0
        Assistant_QA_rec = self.env['hr.employee'].search([('id','=',payslip.employee_id)])
        if Assistant_QA_rec:
            if Assistant_QA_rec.job_id.name == 'Assistant QA':
                result = Assistant_QA

        return round(result)

    def total_overtime_amount(self, payslip):
        total_pay = 0.0
        duration = 0.0
        print "check 1"
        tsheet_obj = self.env['overtime.approve'].sudo().search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('stages','=','validated')])
        year  = int(payslip.date_from[:4])
        month = int(payslip.date_from[5:7])
        month_days =  calendar.monthrange(year,month)[1]
        shift_hours = 0
        for y in tsheet_obj:
            for x in y.tree_link_id:
                if x.employee_id.id == self.id:
                    shift_hours = x.employee_id.schedule.shift_work_hour
                    duration = duration + x.actual_overtime_hours
        if duration and shift_hours:
            total_pay = (payslip.contract_id.wage/month_days/shift_hours)*duration
            return total_pay
        else:
            return 0

    def total_overtime_hours(self, payslip):
        duration = 0.0
        tsheet_obj = self.env['overtime.approve'].sudo().search([('date', '>=', payslip.date_from),('date', '<=', payslip.date_to),('stages','=','validated')])
        for y in tsheet_obj:
            for x in y.tree_link_id:
                if x.employee_id.id == self.id:
                    duration = duration + x.actual_overtime_hours
        return duration

    def night_shift(self, payslip,night_allowance_amount):
        result = 0
        employee_watchman = self.env['hr.employee'].sudo().search([('id','=',payslip.employee_id),('job_id.name','=','Watchman')])
        employee = self.env['hr.employee'].sudo().search([('id','=',payslip.employee_id),('night_allo','=',True)])
        if employee:
            result = night_allowance_amount
            if employee_watchman:
                result = employee_watchman.job_id.description

        return result

    def marriage_date(self, payslip,marriage_amount):
        result = 0
        employee = self.env['hr.employee'].sudo().search([('id','=',payslip.employee_id),('marriage_allo','=',True)])
        if employee:
            result = marriage_amount

        return round(result)

    def lunch_section_rule(self, payslip,one_time , two_time):
        result = 0
        employee = self.env['hr.employee'].search([('id','=',payslip.employee_id)])
        if employee:
            if employee.lunch_section_rule:
                if employee.lunch_section == 'one_time':
                    result = one_time
                if employee.lunch_section == 'two_time':
                    result = two_time

        return result

    def un_paid_leaves(self, payslip):
        employee_single_presnet = self.env['actual.attendence'].search_count([('todaystatus','!=','absent'),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])
        if employee_single_presnet:
            print "check 1"
            employee_joining_date = self.env['hr.employee'].search([('id','=',payslip.employee_id),('joining_date','>=',payslip.date_from),('joining_date','<=',payslip.date_to)])
            days = 0
            print "check 2"
            print employee_joining_date
            if employee_joining_date:
                print "check 3"
                joining_date_of_employee = employee_joining_date.joining_date
                days=employee_joining_date.joining_date.split('-')[-1]

                days = int(days) - 1
                print "check 4"
                actual_attendace = self.env['actual.attendence'].search_count([('todaystatus','=','absent'),('employee_id','=',payslip.employee_id),('date','>=',str(joining_date_of_employee)),('date','<=',payslip.date_to)])
                print actual_attendace
                print actual_attendace
                print actual_attendace
                print actual_attendace
                print actual_attendace
                print "test1"
            else:
                actual_attendace = self.env['actual.attendence'].search_count([('todaystatus','=','absent'),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])


            # actual_attendace_preset = self.env['actual.attendence'].search_count([('todaystatus','=','present'),('total_time','<',7.0),('employee_id','=',payslip.employee_id),('date','>=',payslip.date_from),('date','<=',payslip.date_to)])
            
            deduction_amount = 0
            total_working_days = 26
            print "check 8888"
            if actual_attendace or employee_joining_date :
                print "check 99"
                total_absent = 0
                total_absent = actual_attendace + days
                per_day_salary = payslip.contract_id.wage / 26
                deduction_amount = total_absent * per_day_salary
            return round(deduction_amount)
        else:
            print "else start -------------"
            result =  payslip.contract_id.wage
            print result
            return round(result)

    @api.multi
    def write(self, vals):
        before=self.write_date
        rec = super(hr_custom_employee, self).write(vals)
        after = self.write_date
        if before != after:
            print vals
            if 'joining_date' in vals or 'contract_type' in vals or 'struct_id' in vals or 'schedule_pay' in vals or 'xx_salary' in vals or 'name' in vals:
                self.create_update_employee_contract()
                self.employee_to_partner_create()
            # self.employee_unique_code()
        return rec

    @api.model
    def create(self, vals):
        new_record = super(hr_custom_employee, self).create(vals)
        new_record.employee_unique_code()
        new_record.create_update_employee_contract()
        new_record.employee_to_partner_create()
        return new_record

    def employee_unique_code(self):
        employee = self.env['hr.employee'].sudo().search_count([('emp_code','=',self.emp_code),('id','!=',self.id)])
        if employee:
            raise ValidationError('Employee code is already exist.')
    

    def employee_to_partner_create(self):
        if self.partner_id:
            self.partner_id.name = self.name
        else:
            Receivable_list=[]
            Payable_list=[]
            account_receivable=self.env['account.account'].search([('user_type_id.name','=','Receivable')])
            for x in account_receivable:
                Receivable_list.append(x)
            
            account_payable=self.env['account.account'].search([('user_type_id.name','=','Payable')])
            for y in account_payable:
                Payable_list.append(y)
            
            member_entries = self.env['res.partner']
            customer_id = member_entries.create({
                    'name': self.name,
                    'property_account_receivable_id': Receivable_list[0],
                    'property_account_payable_id':  Payable_list[0],
                    'employee': True,
                    'customer': False,
                    'supplier': False,
                })
            self.partner_id =customer_id.id


class CompanyEmployeeQualification(models.Model):
    _name = 'company.assets'

    name = fields.Char('Name')
    detailed = fields.Char('Detailed')
    asset_typee=fields.Many2one('company.provided.ext',string="Type")
    asset_amount=fields.Integer(string="Amount")
    company_assets = fields.Many2one('hr.employee')

class EmployeeQualification(models.Model):
    _name = 'employee.qualification'

    qualification = fields.Many2one('qualification.list','Qualification')
    passing_year = fields.Many2one('year.list','Passing Year')
    institue = fields.Many2one('institute.list','Institute')
    attachment = fields.Many2many('ir.attachment', string="Attachment")

    employee_qualification_id = fields.Many2one('hr.employee','Employee Qualification')

class EmployeeCertification(models.Model):
    _name = 'employee.certification'
    certification = fields.Char('Certification')
    year = fields.Many2one('year.list','Year')
    conducting_institute = fields.Many2one('institute.list','Conducting Institute')
    duration = fields.Char('Duration')
    attachment = fields.Many2many('ir.attachment', string="Attachment")
    employee_certification_id = fields.Many2one('hr.employee','Employee Certification')

class FamilyTree(models.Model):
    _name = 'family.tree'
    _rec_name = 'child_name'

    child_name = fields.Char('Child Name')
    mother_name = fields.Char('Mother Name')
    status = fields.Selection([
            ('son', 'Son'),
            ('daughter', 'Daughter'),
            ], string="Son/Daughter")
    dob = fields.Date('DOB')
    child_id = fields.Char('B.Form/CNIC No.')
    child_age = fields.Float('Age', digits=(12,1),compute='childage_calc')
    employee_family_id = fields.Many2one('hr.employee','Link')

    @api.onchange('dob')
    def childage_calc(self):
        s_experience_from = fields.date.today()
        s_experience_from = str(s_experience_from)
        for rec in self:
            s_experience_to = rec.dob
            if rec.dob:
                dt_s_obj = datetime.strptime(s_experience_from,"%Y-%m-%d")
                dt_e_obj = datetime.strptime(s_experience_to,"%Y-%m-%d")
                timedelta = dt_s_obj - dt_e_obj
                days = timedelta.days
                years = days/365.0

                rec.child_age = years

class SpouseTree(models.Model):
    _name = 'spouse.tree'
    _rec_name='spouse_name'
    spouse_name = fields.Char('Spouse Name')
    s_dob = fields.Date('Date of Birth')
    s_contact = fields.Char('Spouse Contact')
    s_cnic = fields.Char('CNIC #')
    spouse_age = fields.Float('Age',compute='spouseage_calc')
    spouse_tree = fields.Many2one('hr.employee','Link')

    @api.onchange('s_dob')
    def spouseage_calc(self):
        s_experience_from = fields.date.today()
        s_experience_from = str(s_experience_from)
        for rec in self:

            s_experience_to = rec.s_dob
            if rec.s_dob:
                dt_s_obj = datetime.strptime(s_experience_from,"%Y-%m-%d")
                dt_e_obj = datetime.strptime(s_experience_to,"%Y-%m-%d")
                timedelta = dt_s_obj - dt_e_obj
                days = timedelta.days
                years = days/365.0
                rec.spouse_age = years

class EmployeeExperience(models.Model):
    _name = 'employee.experience'

    company = fields.Char('Company')
    designation = fields.Char('Designation')
    experience_from = fields.Date('Experience From')
    experience_to = fields.Date('Experience To')
    attachment = fields.Many2many('ir.attachment', string="Attachment")
    total_experience_diff = fields.Float('Total', digits=(12,1))

    employee_experience_id = fields.Many2one('hr.employee','Employee Experience')

    @api.onchange('experience_from','experience_to')
    def experince_diff(self,):
        s_experience_from = self.experience_from
        s_experience_to = self.experience_to
        if s_experience_from and s_experience_to:
            dt_s_obj = datetime.strptime(s_experience_from,"%Y-%m-%d")
            dt_e_obj = datetime.strptime(s_experience_to,"%Y-%m-%d")
            timedelta = dt_e_obj - dt_s_obj
            days = timedelta.days
            months = days/30.43
            years = days/365.0
            self.total_experience_diff = years

class InstituteList(models.Model):
    _name = 'institute.list'

    name = fields.Char(string='Name of Institute')

class YearList(models.Model):
    _name = 'year.list'

    name = fields.Char(string='Create Year')

class InstituteList(models.Model):
    
    _name = 'institute.list'

    name = fields.Char(string='Name of Institute')

class QualificationList(models.Model):
    _name = 'qualification.list'

    name = fields.Char(string='Name of Degree')

class EmpCardNum(models.Model):
    _name = 'emp.card.num'

    name = fields.Char(string='Name')

class MarriageDate(models.Model):
    _name = 'marriage.date.ext'
    _rec_name = 'date'

    name = fields.Char(string='Name')
    date = fields.Date('Marriage Date')

class UserFormExtension(models.Model):
    _inherit = 'res.users'

    description = fields.Text(string='Description')
    department_id = fields.Many2many('hr.department',string='Department')
    employee_id = fields.Many2one('hr.employee',string='Employee')

    def set_employee_id(self,):
        if self.partner_id:
            if self.partner_id.active == True:
                self.partner_id.active == False
                self.partner_id.toggle_active()
        if self.employee_id:
            self.employee_id.user_id = self.id



    

class CompanyProvidedAssetsExtension(models.Model):
    _name = 'company.provided.ext'
    _rec_name = 'name'

    name = fields.Char(string='Name')