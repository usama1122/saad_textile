# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import json
from openerp import models, fields, api
import datetime as dt
from openerp.exceptions import Warning
import base64

# import magic
# from mimetypes import guess_extension, guess_types



class hr_custom_employee(http.Controller):

	@http.route('/getUserProfile/',type='json', auth='user')
	def getEmployees(self, **kw):
		employee_id = kw.get('employee_id')
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		contrat_employee = request.env['hr.contract'].sudo().search([('employee_id', '=' ,hr_employee.id)])
		annual_leaves = request.env['annual.leaves'].sudo().search([('employee_id', '=' ,hr_employee.id)])
		holiday_status = request.env['hr.holidays.status'].sudo().search([])
		shifts = request.env['shifts.attendence'].sudo().search([])
		loan_type = request.env['payment.type.class'].sudo().search([])


		# liste = []
		# for x in holiday_status:
		# 	liste.append({
		# 		'id':x.id,
		# 		'name':x.name,
		# 		})
		liste = [{'id': idx['id'], 'name': idx['name']}for idx in holiday_status] 

		# shift_list = []
		# for x in shifts:
		# 	shift_list.append({
		# 		'id':x.id,
		# 		'name':x.rec_name,
		# 		})
		# list_id=shifts.mapped('id')
		# shifts=shifts.mapped('rec_name')
		# key_list=['id','name']
		# n = len(list_id) 
		# shift_list = [{key_list[0]: list_id[idx], key_list[1]: shifts[idx]}for idx in range(0, n, 2)] 
		

		shift_list = [{'id': idx['id'], 'name': idx['rec_name']}for idx in shifts] 


		# loan_type_list = []
		# for x in loan_type:
		# 	loan_type_list.append({
		# 		'id':x.id,
		# 		'name':x.name,
		# 		})

		loan_type_list = [{'id': idx['id'], 'name': idx['name']}for idx in loan_type] 
	  
		# experience = []
		# for x in hr_employee.employee_expert_id:
		# 	experience.append({
		# 		'company':x.company,
		# 		'designation':x.designation,
		# 		'experience_from':x.experience_from,
		# 		'experience_to':x.experience_to,
		# 		'total':x.total_experience_diff,
				# })
		experience = [{'company':idx['company'],'designation':idx['designation'],'experience_from':idx['experience_from'],'experience_to':idx['experience_to'],'total':idx['total_experience_diff'],}for idx in hr_employee.employee_expert_id] 


		# qualification = []
		# for x in hr_employee.employee_qualify_id:
		# 	qualification.append({
		# 		'qualification':x.qualification.name,
		# 		'passing_year':x.passing_year.name,
		# 		'institue':x.institue.name,
		# 		})
		qualification = [{
				'qualification':idx.qualification.name,
				'passing_year':idx.passing_year.name,
				'institue':idx.institue.name,
				}for idx in hr_employee.employee_qualify_id] 




		spouse_info = []
		for x in hr_employee.spouse_tree_id:
			spouse_info.append({
				'spouse_name':x.spouse_name,
				'id':x.id,
				})

		child_info = []
		for x in hr_employee.family_tree_id:
			child_info.append({
				'child_name':x.child_name,
				'id':x.id,
				})
	   
		print "emememememememememememememememememememememememememememememememememem"

		em = {
			'name':hr_employee.name,
			'cnic':hr_employee.identification_id,
			'job_title':hr_employee.job_id.name,
			'address':hr_employee.emergency_address,
			'shift':{"id":hr_employee.schedule.id,"name":hr_employee.schedule.rec_name},
			'job_status':hr_employee.active,
			'father_name':hr_employee.father_name,
			'father_live':hr_employee.live_status,
			'mother_name':hr_employee.mother_name,          
			'mother_live':hr_employee.mother_status,
			'spouse_info':spouse_info,          
			'child_info':child_info,          
			'wage':contrat_employee.wage,
			'remaining_medical_claim':hr_employee.remaing_amount,
			'total_leaves':annual_leaves.total_leaves,
			'leave_availed':annual_leaves.leave_availed,
			'leave_remaining':annual_leaves.leave_remaining,
			# 'year':annual_leaves.year,
			'image':hr_employee.image,
			'employee_id':hr_employee.id,
			'holiday_status':liste,
			'shifts':shift_list,
			'loan_type_list':loan_type_list,
			'experience':experience,
			'qualification':qualification,
		}
		data = {'status':200,'response':em, 'message':"Success"}
		return data

	@http.route('/getUserleave/',type='json', auth='user')
	def getEmployeesleaves(self, **kw):
		employee_id = kw.get('employee_id')
		print "Get employee leaves"
		print employee_id
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		print "--------"
		print hr_employee
		print hr_employee.id
		print hr_employee.name
		annual_leaves = request.env['annual.leaves'].sudo().search([('employee_id', '=' ,hr_employee.id)])
		print annual_leaves

		all_leaves_type = []
		for x in annual_leaves.tree_link_id:
			all_leaves_type.append({
				'leaves_type':x.leaves_type.name,
				'total_leaves':x.total_leaves,
				'leave_availed':x.leave_availed,
				'leave_remaining':x.leave_remaining,
				})
		print "Get employee leaves"
		leaves_availed = []
		for x in annual_leaves.tree_link_holidays:
			print "                        "
			print x.holiday_status_id.name
			if x.holiday_status_id.name in ['Annual Leaves','Casual Leaves','Sick Leaves']:
				print "append"
				leaves_availed.append({
					'description':x.name,
					'leaves_type':x.holiday_status_id.name,
					'date_from_ecube':x.date_from_ecube,
					'date_to_ecube':x.date_to_ecube,
					'days':x.number_of_days_temp_ecube,
					})


		leave_data = {
		'leaves':all_leaves_type,
		'leaves_availed':leaves_availed,

		}
		data = {'status':200,'response':leave_data, 'message':"Success"}

		print "Get employee end"
		return data

	
 
	
	@http.route('/getcreateleave/',type='json', auth='user')
	def getCreateleavesform(self, **kw):
		print "getCreateleavesform function start"
		employee_id = kw.get('employee_id')
		name = kw.get('name')
		holiday_status_id = kw.get('holiday_status_id')
		date_from_ecube = kw.get('date_from_ecube')
		date_to_ecube = kw.get('date_to_ecube')
		type_is = kw.get('type')
		reason = kw.get('reason')

	
		holiday_status = request.env['hr.holidays.status'].sudo().search([('id', '=' ,holiday_status_id)])
		# hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		# annual_leaves = request.env['annual.leaves'].sudo().search([('employee_id', '=' ,employee_id)])
		leave_type=None
		if type_is=='2':
			leave_type='half'
		if type_is=='1':
			leave_type='fulday'



		hr_holidays = request.env['hr.holidays']
		# print holiday_status
		# print hr_employee
		# print annual_leaves
		print "check 1"
		print hr_holidays
		print name
		print employee_id
		print holiday_status.id
		print date_from_ecube
		print date_to_ecube
		print "create leaves start"
		request_data = {
			'name': reason,
			'employee_id': int(employee_id),
			'holiday_status_id':int(holiday_status.id),
			'date_from_ecube':date_from_ecube,
			'date_from':date_from_ecube,
			'date_to_ecube':date_to_ecube,
			'date_to':date_to_ecube,
			'type':'remove',
			'day_type':leave_type,
			'holiday_type':'employee',
			}

		create_request = hr_holidays.create(request_data)
		

		print "check 2"
		print "check 3"

		data = {'status':200,'response':request_data, 'message':"Success"}
		print "check 4"

		print "getCreateleavesform function end"
		return data

	@http.route('/getholidays/',type='json', auth='user')
	def getEmployeesholidays(self, **kw):
		year = kw.get('year')
		holidays = request.env['holidays.tree'].sudo().search([])
		# ('tree_link.year','=',year)
		liste = []
		for x in holidays:
			liste.append({
					'date':x.date,
					'day':x.day,
					'remarks':x.remarks,
					# 'year':x.tree_link.year,
					})
		data = {'status':200,'response':liste, 'message':"Success"}

		return data

	@http.route('/getattendancerep/',type='json', auth='user')
	def getAttendance(self, **kw):
		employee_id = kw.get('employee_id')
		date = kw.get('date')
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		actual_attendence = request.env['actual.attendence'].sudo().search([('employee_id','=',hr_employee.id)])
		# ,('date','=',date_to)
		liste = []
		for x in actual_attendence:
			acc_month = dt.datetime.strptime(x.date, '%Y-%m-%d')
			user_month = dt.datetime.strptime(date, '%Y-%m-%d')
			if acc_month.month == user_month.month:
				liste.append({
						'employee_id':x.employee_id.name,
						'card_no':x.card_no,
						'date':x.date,
						'shift':x.shift,
						# 'intime':dict(x._fields['intime'].selection).get(x.intime),
						'intime':x.checkin,
						'department':x.department.name,
						# 'outtime':dict(x._fields['outtime'].selection).get(x.outtime),
						'outtime':x.checkout,
						'todaystatus':dict(x._fields['todaystatus'].selection).get(x.todaystatus),
						'defaultstatus':dict(x._fields['defaultstatus'].selection).get(x.defaultstatus),
						})
		data = {'status':200,'response':liste, 'message':"Success"}

		return data


	@http.route('/getloandetailed/',type='json', auth='user')
	def getDetailedLoan(self, **kw):

		print "getDetailedLoan start"
		employee_id = kw.get('employee_id')
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		advances = request.env['contractor.advances'].sudo().search([('employee','=',hr_employee.id)])
		
		liste = []
		for x in advances: 
			liste.append({
					'employee_id':x.employee.name,
					'cnic':x.cnic,
					'date':x.date,
					'amount':x.amount,
					'received':x.received,
					'remaining':x.remaining,
					'type':x.type.name,
					'state':dict(x._fields['state'].selection).get(x.state),
					})
		print liste
		data = {'status':200,'response':liste, 'message':"Success"}
		print "end..........."
		return data

	@http.route('/getcreateloan/',type='json', auth='user')
	def getCreateLoanForm(self, **kw):
		print "getCreateLoanForm start"
		employee_id = kw.get('employee_id')
		print employee_id
		cnic = kw.get('cnic')
		date = kw.get('date')
		reason = kw.get('reason')

		amount = kw.get('amount')
		loan_type = kw.get('loan_type')
		hr_employee = request.env['hr.employee'].sudo().search([('id', '=' ,int(employee_id))])
		print hr_employee
		print "check 1"
		print amount
		print int(employee_id)
		print cnic
		print date
		print loan_type
		print hr_employee.partner_id.name
		print int(hr_employee.partner_id.id)
		request_data = {
				'employee':int(employee_id),
				'cnic':str(hr_employee.identification_id),
				'date':date,
				'name': int(hr_employee.partner_id.id),
				'type':loan_type,
				'remark_des':reason,
				}
		print "check 2"
		print request_data
		create_request = request.env['contractor.advances'].create(request_data)
		print "form create "
		print create_request
		print create_request.id
		request_data_line = {
				'date':date,
				'amount': amount,
				'payment_id': create_request.id,
				}
		print "============="
		create_request_tree =  request.env['contractor.advances.lines'].create(request_data_line)
		# create_request.payment = request_data_line
		create_request.getpayment()
		print "check 3"
		data = {'status':200,'response':request_data, 'message':"Success"}
		print "function end"
		return data
	

	@http.route('/getMedicalClaim/',type='json', auth='user')
	def getMedicalForm(self, **kw):
		print "getMedicalForm  funtion start"
		employee_id = kw.get('employee_id')
		
		print employee_id
		print "check 1"
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		print hr_employee
		print hr_employee.name
		medical_claim = request.env['medical.claim'].sudo().search([('employee_id', '=' ,int(hr_employee.id))])
		print "check 2"
		print medical_claim
		print medical_claim
		print medical_claim
		print medical_claim


		# tree_medical = request.env['medical.detail']
		request_data =[]
		for medical_claim_form in medical_claim:
			liste = []
			for data in medical_claim_form.tree_link:
				liste.append({
					'description':data.description,
					'date_claim':data.date,
					'claim_amount':data.claim_amount,
					# 'document_id':data.document_id,holiday_status_id
					})
			print liste
			name = ""
			record_claim_amount = 0
			apporve_amount = 0

			record_claim_amount = medical_claim_form.total_amount
			apporve_amount = medical_claim_form.final_approved_amount

			if medical_claim_form.claim_type == "self":
				name = medical_claim_form.employee_id.name
			if medical_claim_form.claim_type == "children":
				name = medical_claim_form.children.child_name
			if medical_claim_form.claim_type == "spouse":
				name = medical_claim_form.spouse.spouse_name
			if medical_claim_form.claim_type in ["father","mother"]:
				name = medical_claim_form.parent_name
			print "ddddddddddddddddddddddddddddd"
			request_data.append({
					'id':medical_claim_form.id,
					'date':medical_claim_form.date,
					'state':medical_claim_form.state,
					'name':name,
					'record_claim_amount':record_claim_amount,
					'apporve_amount':apporve_amount,
					'claim_type':medical_claim_form.claim_type,
					'tree':liste,})
			print request_data
			print "request_data"
		print request_data
		print "request_data"
		requested_data = {
				'data':request_data,
				}
		# create_request = medical_claim_form.create(request_data)
		# print  "Challllaaaaaaaaa"
		# tree_data = {
		#             'description':description,
		#             'date':date_claim,
		#             'claim_amount':claim_amount,
		#             'return_link':create_request.id,           
		#           }
		# create_tree = tree_medical.create(tree_data)
		data = {'status':200,'response':requested_data, 'message':"Success"}
		print "getMedicalForm  funtion END"
		return data
	
	@http.route('/getCreateMedicalClaim/',type='json', auth='user')
	def getCreateMedicalForm(self, **kw):
		print "getCreateMedicalForm function start"
		employee_id = kw.get('employee_id')
		relation_with = kw.get('relation_with')
		date = kw.get('date')
		image = kw.get('images')
		# print image

		print employee_id
		print "check 1"
		if relation_with :        
			description = kw.get('description')
			claim_amount = kw.get('claim_amount')
			date_claim = kw.get('date_claim')
			id_of_selected = kw.get('id_of_selected')
			# document_id= kw.get('document_id')holiday_status_id
		
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		print "check 2"
		print hr_employee
		parent_name=None
		if relation_with == 'father':
			parent_name = hr_employee.father_name
		if relation_with == 'mother':
			parent_name = hr_employee.mother_name
		child_id=None
		# if relation_with == 'children':
		#     child_id = kw.get('child_id')
		if relation_with == 'self':
			child_id = None
			spouse_id = None
		if relation_with == 'spouse':
			print "================="
			# hr_employee = request.env['spouse.tree'].sudo().search([('spouse_tree.id', '=' ,int(employee_id)),('id', '=' ,id_of_selected)])
			# spouse_id = kw.get('spouse_id')
		medical_claim_form = request.env['medical.claim']
		tree_medical = request.env['medical.detail']
		attachment = request.env['ir.attachment']
		# liste = [description,date_claim,date_claim]
		# for data in medical_claim_form.tree_link:
		#     liste.append({
		#         'description':data.leaves_type.name,
		#         'date':data.date,
		#         'claim_amount':data.claim_amount,
		#         # 'document_id':data.document_id,
		#         })
		# print liste
		# user_obj = user_ids[0]
		# withopen("max.jpg", "rb") as image:
		# 	BinaryImage = client.Binary(image.read())
		# 	BytesImage = BinaryImage.data
		# 	ImageBase64 = base64.b64encode(BytesImage)
		request_data={}
		print "ddddddddddddddddddddddddddddd"
		if relation_with in ['father','mother']:
			request_data = {
					'employee_id':hr_employee.id,
					'claim_type': relation_with,
					'parent_name' : parent_name,
					'date' : date,
					'emp_code' : hr_employee.emp_code,
					'job_id' :hr_employee.job_id.id,
					'shift_id' :hr_employee.schedule.id,
					'department_id' : hr_employee.department_id.id,
					'balance' :hr_employee.remaing_amount,
					}
		if relation_with == 'children':
			request_data = {
					'employee_id':hr_employee.id,
					'claim_type': relation_with,
					'children' : id_of_selected,
					'date' : date,
					
					'job_id' :hr_employee.job_id.id,
					'shift_id' :hr_employee.schedule.id,
					'department_id' : hr_employee.department_id.id,
					'emp_code' : hr_employee.emp_code,
					'balance' :hr_employee.remaing_amount,
					}
		if relation_with == 'spouse':
			request_data = {
					'employee_id':hr_employee.id,
					'claim_type': relation_with,
					'spouse' : id_of_selected,
					'date' : date,
					'job_id' :hr_employee.job_id.id,
					'shift_id' :hr_employee.schedule.id,
					'department_id' : hr_employee.department_id.id,
					'emp_code' : hr_employee.emp_code,
					'balance' :hr_employee.remaing_amount,
					}
		if relation_with == 'self':
			request_data = {
					'employee_id':hr_employee.id,
					'claim_type': relation_with,
					'date' : date,
					'job_id' :hr_employee.job_id.id,
					'shift_id' :hr_employee.schedule.id,
					'department_id' : hr_employee.department_id.id,
					'balance' :hr_employee.remaing_amount,
					'emp_code' : hr_employee.emp_code,

					}
		# else:
		#     request_data = {
		#             'employee_id':hr_employee.id,
		#             'date' : date,
		#             'emp_code' : hr_employee.emp_code,
		#             'job_id' :hr_employee.job_id.id,
		#             'shift_id' :hr_employee.schedule.id,
		#             'department_id' : hr_employee.department_id.id,
		#             'balance' :hr_employee.remaing_amount,
		#             }
				# <======================================Hamza Code Start=================================>
		# request_data = {
		#         'employee_id':hr_employee.id,
		#         'emp_code':hr_employee.emp_code,
		#         'job_id' :hr_employee.job_id.id,
		#         'shft_id' :hr_employee.schedule.id,
		#         'claim_type':claim_for,
		#    i     'parent_name' : parent_name,
		#         'department_id' : hr_employee.department_id.id,
		#         'spouse_ids' :hr_employee.spouse_tree_id.ids or spouse_id,
		#         'child_ids' :hr_employee.family_tree_id.ids or child_id,
		#         'balance' :hr_employee.remaing_amount,
		#         'date':date,
		#         'children':child_id,
		#         'spouse':spouse_id,
		#         }
				# <======================================Hamza Code End=================================>

		print "request_data"
		print request_data
		create_request = medical_claim_form.create(request_data)
		list_of_ids=[]
		for img in image:
			for im in img:
					

				# print img[im]
				ImageBase=img[im]
				# ImageBase64=base64.b64encode(ImageBase.encode('utf-8'))
				ImageBase64=ImageBase.encode('utf-8','ignore')
				decoded = base64.b64decode(ImageBase64[23:])
				# print decoded
				# mime_type = magic.from_buffer(ImageBase64[23:], mime=True)
				# print mime_type
				# file_ext = '.' + mime_type.split('/')[1]
				# file_ext = mimetypes.guess_extension(mime_type)
				# print file_ext
				# print  ImageBase64[23:]

			
				

				print  "Challllaaaaaaaaa"
				image_id=attachment.create({
				'name': 'document',
				'type': 'binary',
				'datas': ImageBase64[23:],
				'datas_fname': 'document',
				'res_id': create_request.id,
				})
				list_of_ids.append(image_id.id)

		tree_data = {
					'description':description,
					'claim_amount':claim_amount,
					'date':date_claim,
					'document_id':[(6,0,[list_of_ids])],
					# 'document_id':[(4, id_of_images.id)],
					'return_link':create_request.id,       
				  }
		# print tree_data
		create_tree = tree_medical.create(tree_data)
		create_request._get_total_amount()
		data = {'status':200,'response':request_data, 'message':"Success"}

		return data
	

	
	@http.route('/getShiftChangeDetails/',type='json', auth='user')
	def getShiftChange(self, **kw):

		print "gethifchangedetails start"
		employee_id = kw.get('employee_id')
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		shift_id = request.env['shift.change'].search([('employee_id', '=' ,hr_employee.id)])
		liste = []
		for x in shift_id: 
			liste.append({
					'employee_id':x.employee_id.id,
					'from_date':x.from_date,
					'to_date':x.to_date,
					'reason':x.reason,
					'requested_shift':x.requested_shift_id.id,
					'state':dict(x._fields['state'].selection).get(x.state),
					})
		data = {'status':200,'response':liste, 'message':"Success"}

		return data



	@http.route('/CreateShiftChange/',type='json', auth='user')
	def getCreateShiftChange(self, **kw):

		
		print "check 1"
		employee_id = kw.get('employee_id')
		print employee_id
		from_date = kw.get('from_date')
		print from_date

		to_date = kw.get('to_date')
		print to_date
		requested_shift = kw.get('requested_shift')
		reason = kw.get('reason')
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])
		shift_form = request.env['shift.change']
		print hr_employee.name
		print hr_employee
		print int(hr_employee.id)
		request_data = {
				
				'employee_id':hr_employee.id,

				'current_shift_id' :hr_employee.schedule.id,

				'emp_code' : hr_employee.emp_code,


				'from_date':from_date,
				'to_date':to_date,
				'reason':reason,
				'requested_shift_id':requested_shift,


				}
		print "check 2"
		print request_data
		create_request = shift_form.sudo().create(request_data)
		print create_request
		print "check 3"
		data = {'status':200,'response':request_data, 'message':"Success"}
		print "end..........."
		return data

	@http.route('/updatePassword/',type='json', auth='user')
	def getupdatePassword(self, **kw):

		
		print "check 1"
		employee_id = kw.get('employee_id')
		password = kw.get('password')
		print password
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])

		user_form = request.env['res.users'].sudo().search([('id', '=' ,employee_id)])

		request_data = {
				'password':password,
				
				}

		create_request = user_form.sudo().write(request_data)
		data = {'status':200,'response':request_data, 'message':"Success"}
		return data


	@http.route('/GetSalarySlipReport/',type='json', auth='user')
	def GetSalarySlipReport(self, **kw):

		
		employee_id = kw.get('employee_id')
		date = kw.get('date')
		hr_employee = request.env['hr.employee'].sudo().search([('user_id', '=' ,int(employee_id))])

		hr_contract = request.env['hr.contract'].sudo().search([('employee_id', '=' ,int(hr_employee.id))],limit=1)
		



		salary_slip = request.env['hr.payslip'].sudo().search([('contract_id', '=' ,int(hr_contract.id)),('state', '=' ,'done'),('date_from', '<=' ,date),('date_to', '>=' ,date)])


		if salary_slip:
			pdf = request.env['report'].sudo().get_pdf([salary_slip.id], 'salary_slip.module_report')
			b64_pdf = base64.encodestring(pdf)
			# pdfhttpheaders = [('Content-Type', 'application/pdf'), ('Content-Length', len(pdf))]
			request_data = {
					'b64_pdf':b64_pdf,
					
					}

		# return request.make_response(pdf, headers=pdfhttpheaders)

		# create_request = user_form.sudo().write(request_data)
			data = {'status':200,'response':request_data, 'message':"Success"}
		else:
			data = {'status':400,'response':"Not avaliable at this moment", 'message':"Success"}
		# return request.make_responseC
		return data
	# @http.route('/todo_app/todo_app/objects/', auth='public')
	# def list(self, **kw):
	#     return http.request.render('todo_app.listing', {
	#         'root': '/todo_app/todo_app',
	#         'objects': http.request.env['todo_app.todo_app'].search([]),
	#     })

	# @http.route('/todo_app/todo_app/objects/<model("todo_app.todo_app"):obj>/', auth='public')
	# def object(self, obj, **kw):
	#     return http.request.render('todo_app.object', {
	#         'object': obj
	#     })
	# pip install python-magic