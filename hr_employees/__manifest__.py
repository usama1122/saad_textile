# -*- coding: utf-8 -*-
{
	'name': "Hr Managment",

	'summary': """HR Employees Managment by Ecube""",

	'description': """
		 For Managing HR Issues 
	""",

	'author': "Abd ur Rehman & Hamza Azeem",
	'application':'True',
	# any module necessary for this one to work correctly
	'depends': ['base','mail','account','hr','hr_payroll','branch'],

	# # always loaded
	'data': [
		'Views/My_view.xml',
		# 'Views/view_menu.xml',
	],
	# # only loaded in demonstration mode

}
