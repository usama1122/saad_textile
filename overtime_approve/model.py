# -*- coding: utf-8 -*- 
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.exceptions import ValidationError
from openerp.exceptions import Warning, ValidationError, UserError
import datetime
from datetime import date, datetime, timedelta
import time
import calendar
from dateutil.relativedelta import relativedelta
import datetime as dt
import pytz

class OvertimeForm(models.Model):
	_name = 'overtime.approve'
	_rec_name = 'department'
	_inherit = ['mail.thread', 'ir.needaction_mixin']

	date = fields.Date(string="Date" ,required=True, track_visibility='onchange')
	department = fields.Many2one('hr.department',string="Department" ,required=True, track_visibility='onchange')
	total_hours = fields.Float(string="Actual Total Hours", track_visibility='onchange',copy=False)
	tree_link_id = fields.One2many('overtime.approve.tree','tree_link', track_visibility='onchange')
	stages = fields.Selection([
		('draft',' Un Approve'),
		('validated','Approve'),
		],default="draft",copy=False, track_visibility='onchange')
	branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')


	@api.onchange('date')
	def get_branch_id(self):
		users = self.env['res.users'].search([('id','=',self._uid)])
		if self.date:
			if not self.branch:
				self.branch = users.branch.id

	@api.multi
	def in_draft(self):
		self.stages = "draft"
		for x in self.tree_link_id:
			if x.approve == True:
				actual = self.env['actual.attendence'].search([('date','=',self.date),('employee_id','=',x.employee_id.id)])
				for acl in actual:
					acl.star_hours = 0
					acl.end_hours = 0
					acl.total_hours = 0
						
	@api.multi
	def in_validate(self):
		self.stages = "validated"
		# for x in self.tree_link_id:
		# 	if x.actual_overtime_hours:
		# 		pass
		# 	else:
		# 		x.unlink()
			# if x.approve == True:
			# 	# x.actual_overtime_hours = x.total_hours
			# 	actual = self.env['actual.attendence'].search([('date','=',self.date),('employee_id','=',x.employee_id.id)])
			# 	for acl in actual:
			# 		acl.set_shift()
			# 		# acl.star_hours = x.star_hours
			# 		# acl.end_hours = x.end_hours
			# 		# acl.total_hours = x.total_hours
			# 		acl.overtime = x.approve
			# 		DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
			# 		actual_date = datetime.strptime(str(acl.end_date), DATETIME_FORMAT)
			# 		date_field_data = actual_date + timedelta(hours=12,minutes=00)
			# 		acl.end_date_overtime = date_field_data


	@api.onchange('department')
	def get_department_employee(self):
		pass
		# dept_emp = self.env['hr.employee'].search([('department_id','=',self.department.id)])
		# create_record = []
		# for x in dept_emp:
		# 	actual = self.env['actual.attendence'].search([('date','=',self.date),('employee_id','=',x.id)])
		# 	create_record.append({
		# 		'employee_id':x.id,
		# 		'card_no':x.card_no.name,
		# 		'star_hours':x.schedule.outtime,
		# 		'end_hours':actual.end_shift,
		# 		'tree_link':self.id,
		# 		})
		# self.tree_link_id = create_record


	@api.onchange('tree_link_id')
	def get_total_hours(self):
		if self.tree_link_id:
			total_hours = 0
			for x in self.tree_link_id:
				total_hours = total_hours + x.actual_overtime_hours

			self.total_hours = total_hours


	@api.multi 
	def unlink(self): 
		for x in self: 
			if x.stages == "validated": 
				raise ValidationError('Cannot Delete Record') 
		return super(OvertimeForm,self).unlink()



	def duplicate_record(self):
		overtime_duplicate = self.env['overtime.approve'].search_count([('date','=',self.date),('department','=',self.department.id),('id','!=',self.id)])
		if overtime_duplicate:
			raise ValidationError("Second overtime record isn't allowed for same department in one day. Please use already created one") 

		self.SameProductCheck()

	@api.multi
	def write(self, vals):
		before=self.write_date
		super(OvertimeForm, self).write(vals)
		after = self.write_date
		if before != after:
			self.duplicate_record()
		return True

	@api.model
	def create(self, vals):
		new_record = super(OvertimeForm, self).create(vals)
		new_record.duplicate_record()
		return new_record

	def SameProductCheck(self):
		products = []
		for lines in self.tree_link_id:
			products.append(lines.employee_id.id)

		duplicates = set([x for x in products if products.count(x) > 1])
		if duplicates:
			duplicate_list = list(duplicates)
			product_name = self.env['hr.employee'].search([('id','=',duplicate_list[0])])
			raise ValidationError(str(product_name.name) +' Employee has been added multiple times')


class OvertimeapproveTree(models.Model):
	_name = 'overtime.approve.tree'

	employee_id = fields.Many2one('hr.employee',string="Employee" ,required=True)
	card_no = fields.Char(string="Card No")
	star_hours = fields.Float(string="Start Hours")
	target_end_hours = fields.Float(string="Target End Hours")
	end_hours = fields.Float(string="Actual End Hours")
	total_target_hours = fields.Float(string="Target Hours")
	total_hours = fields.Float(string="Total Target Hours")
	actual_overtime_hours = fields.Float(string="Actual Overtime Hours")
	overtime_amount = fields.Float(string="Overtime Amount")
	remarks = fields.Char(string="Remarks")
	approve = fields.Boolean(string="Approve")
	tree_link = fields.Many2one('overtime.approve')


	@api.onchange('star_hours','end_hours','target_end_hours')
	def get_total_hours(self):
		if self.end_hours:
			self.total_hours = self.end_hours - self.star_hours
		if self.star_hours:
			if self.star_hours > self.target_end_hours:
				self.total_target_hours = self.star_hours - self.target_end_hours
			if self.star_hours < self.target_end_hours:
				self.total_target_hours = self.target_end_hours - self.star_hours
	
	@api.multi
	def action_actual_hours(self):
		if self.approve == True:
			self.actual_overtime_hours = self.end_hours

	@api.multi
	def action_target_hours(self):
		if self.approve == True:
			self.actual_overtime_hours = self.total_hours


