{ 
    'name': 'Overtime Approve',

    'summary': 'Overtime Approve', 

    'description': 'Overtime Approve', 

    'author': 'Rana Rizwan',

    'website': "http://www.bcube.com",

    'depends': ['base','hr'], 

    'application': True, 

    'data': [
    	'views/view.xml',
    	'menu.xml',
    ], 
}