# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import Warning
from odoo.exceptions import UserError
from openerp.exceptions import UserError
import datetime
from datetime import date, datetime, timedelta
import time
import calendar
import datetime
from datetime import time
from dateutil.relativedelta import relativedelta
import datetime as dt
from datetime import datetime 


class EcubeRawAttendance(models.Model):
	_name = 'ecube.raw.attendance'
	_rec = 'EcubeRawAttendance'
	_rec_name = 'employee_id'
	

	name = fields.Char('ERP Name')
	# machine_name = fields.Char('Machine Name')
	machine_id = fields.Char(string='Card No')
	date = fields.Date(string='Date',store=True,)

	time = fields.Char(string='Attendance Time',store=True,)
	shift_id= fields.Many2one('shifts.attendence',string="Shift")
	employee_id = fields.Many2one('hr.employee',string="Employee Name")
	tree_link = fields.Many2one('actual.attendence',string="Tree Link")
	tree_link_mm = fields.Many2many('actual.attendence',string="Tree Link M2M")
	department = fields.Many2one('hr.department',string="Department")
	attendance_date = fields.Char('Attendance Date')
	attendence_done = fields.Boolean(string="Attendence Generated")
	ecube_date = fields.Datetime(string="DateTime")
	emp_name = fields.Char(string='Employee Name')
	check_in = fields.Char(string='Check In')
	check_out = fields.Char(string='Check Out')
	branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

	@api.multi
	def get_attendance(self):
		self.env['hr.employee'].updateAttendanceAll_not_web()

	@api.multi
	def edit_attendance(self):
		self.tree_link = False
		attend = self.env['ecube.raw.attendance'].search([('tree_link','=',False),('employee_id','!=',False),('id','=',self.id)])
		if attend:
			count = 0
			for x in attend:
				if len(x.tree_link_mm) == 3:
					pass
				else:
					count = count + 1
					x.get_miss_attendance()
			print "countcountcountcountcount"
			print count
			print "countcountcountcountcount"

	@api.multi
	def get_all_adjust_attebndnace(self):
		attend = self.env['ecube.raw.attendance'].search([('tree_link','=',False),('employee_id','!=',False),('date','=',self.date)])
		if attend:
			count = 0
			for x in attend:
				if len(x.tree_link_mm) == 3:
					pass
				else:
					count = count + 1
					x.get_miss_attendance()
			print "countcountcountcountcount"
			print count
			print "countcountcountcountcount"
	
	@api.model
	def create(self,vals):
		""" Create a new record for a model EcubeRawAttendance
		:param values: provides a data for new record
		:type: dictionary
		:return: returns recordset of new records
		:rtype: object
		"""
		rec= super(EcubeRawAttendance, self).create(vals)
		
		# if rec.name== False:
			# print "SAAAAAAAAd	"
		rec.get_ecube_date()
		rec.LinkingManualAttendence()
		return rec


	def get_ecube_date(self):
		if self.attendance_date:
			if not self.ecube_date:
				self.ecube_date = self.attendance_date
				self.ecube_date = datetime.strptime(self.ecube_date, '%Y-%m-%d %H:%M:%S') - relativedelta(hours=5)
		

	def LinkingManualAttendence(self):
		dbName = self._cr.dbname
		if dbName == "saad_hr_db":
			self.branch = self.employee_id.branch.id
			current_date = fields.date.today()
			date = (current_date - relativedelta(days=3))
			# all_unmark = self.env['ecube.raw.attendance'].search([('attendence_done','=',False),('employee_id','!=',False),('date','>',date)])
			all_unmark = self.env['ecube.raw.attendance'].search([('attendence_done','=',False),('employee_id','!=',False),('id','=',self.id)])
			count = 0
			print len(all_unmark)
			count = 0
			for x in all_unmark:
				if x.ecube_date:
					count = count + 1
					print count
					# DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
					# ecube_raw_date = datetime.strptime(str(x.attendance_date), DATETIME_FORMAT)
					# raw_date = ecube_raw_date - relativedelta(hours=5)
					# x.ecube_date = raw_date
					print x
					recorded = self.env['actual.attendence'].search([('employee_id.id','=',x.employee_id.id),('effective_start_date','<=',x.ecube_date),('effective_end_date','>=',x.ecube_date)])
					print recorded
					if recorded:
						x.attendence_done = True
						rec = recorded[-1]
						x.tree_link = rec.id
						rec.set_shift()
						print "attendance link successfully"
	
	@api.onchange('employee_id')
	def _onchange_employee(self):
		if self.employee_id:
			self.machine_id =self.employee_id.emp_code
			self.department = self.employee_id.department_id
			self.shift_id = self.employee_id.schedule
		

	@api.onchange('machine_id')
	def _onchange_machine_id(self):
		if self.machine_id:
			employee_id = self.env['hr.employee'].search([('emp_code','=',self.machine_id)],limit=1)
			if employee_id:
				self.employee_id =employee_id.id
				self.department =employee_id.department_id
				self.shift_id =employee_id.schedule
		

	@api.onchange('ecube_date')
	def _onchange_ecube_date(self):
		if self.ecube_date:
			start = datetime.strptime(self.ecube_date, "%Y-%m-%d %H:%M:%S")+ timedelta(hours=5)
			self.time = start.strftime("%H:%M:%S")
			self.date = start.strftime("%Y-%m-%d")
			self.attendance_date = datetime.strptime(self.ecube_date, "%Y-%m-%d %H:%M:%S")+ timedelta(hours=5)
		# elif self.ecube_date==False:
		# 	self.time = ""
		# 	self.date=False
		# 	self.attendance_date = False

	def get_miss_attendance(self):
		date_time_start = dt.datetime.strptime(self.date, '%Y-%m-%d')
		yesterday_date = date_time_start - relativedelta(days=1)
		tomorrowyes_date = date_time_start + relativedelta(days=1)
		rec = self.env['actual.attendence'].search([('date','>=',yesterday_date),('date','<=',tomorrowyes_date),('employee_id','=',self.employee_id.id)])
		miss_employee = []
		for x in rec:
			miss_employee.append(x.id)
		if miss_employee:
			self.tree_link_mm = [(6,0,miss_employee)]

		

	@api.multi
	def set_attendance(self):
		pass
		# rec = self.env['ecube.raw.attendance'].search([('date','>','2019-02-28')])
		# for x in rec:
		#     x.attendence_done = False
		#     x.tree_link = False



	@api.multi
	def write(self, vals):
		before=self.write_date
		actual_attendence_record = False
		if self.tree_link:
			actual_attendence_record = self.tree_link

		rec = super(EcubeRawAttendance, self).write(vals)
		after = self.write_date
		if before != after:
			if not self.tree_link:
				if actual_attendence_record:
					actual_attendence_record.set_shift()
			if self.tree_link:
				self.tree_link.set_shift()
			else:
				self.LinkingManualAttendence()
		return rec



	# @api.multi
	# def set_data(self):

	#     attendance = self.env['ecube.raw.attendance'].search([])
		

	#     for y in attendance:
	#         y.attendence_done = False
	#         y.tree_link = False
		
	#     for x in attendance:
	#         empl_id =self.env['hr.employee'].search([('emp_code','=',x.machine_id)])
	#         x.employee_id = empl_id.id
	#         x.department =empl_id.department_id.id

	#         if x.check_in!=False:

	#             check_in=self.convert24(x.check_in)
	#             x.attendance_date = str(x.date)+" "+ str(check_in)
	#             x.time = str(check_in)
				
	#         if x.check_out!=False:

	#             check_out=self.convert24(x.check_out)
	#             x.attendance_date = str(x.date)+" "+ str(check_out)
	#             x.time = str(check_out)
				
	#         if type(x.attendance_date)!= bool:
	#             ecube_raw_date =  dt.datetime.strptime(x.attendance_date, '%Y-%m-%d %H:%M:%S ')
	#             raw_date = ecube_raw_date - relativedelta(hours=5)
	#             x.ecube_date =raw_date 
	#         else:
	#             pass


	@api.multi
	def set_data(self):
		pass

		# attendance = self.env['ecube.raw.attendance'].search([('date','>','2020-11-01')])
		# for x in attendance:
		# 	x.tree_link = False
		# 	x.attendence_done = False

		# actual = self.env['actual.attendence'].search([('date','>','2020-11-05')])
		# for act in actual:
		# 	parent_id = act.id
		# 	self.env.cr.execute("DELETE FROM actual_attendence WHERE id = %s", (parent_id,))
			
		
		# for x in attendance:
		# 	if x.name:
		# 		parent_id = x.id
		# 		print x.id
		# 		self.env.cr.execute("DELETE FROM ecube_raw_attendance WHERE id = %s", (parent_id,))
		# 		# if x.attendance_date:

				# 	x.attendance_date = str(x.date)+" "+ str(x.time)
						
				# 	if type(x.attendance_date)!= bool:
				# 		print x
				# 		print x.attendance_date
				# 		# raw_date = ecube_raw_date - relativedelta(hours=5)
				# 		x.ecube_date =x.attendance_date
				# 		ecube_raw_date =  dt.datetime.strptime(x.ecube_date, '%Y-%m-%d %H:%M:%S ')
				# 		print "check 1"
				# 		x.ecube_date = ecube_raw_date - relativedelta(hours=5)
				# 		print "check 2"
			# empl_id =self.env['hr.employee'].search([('emp_code','=',x.machine_id)])
			# x.employee_id = empl_id.id
			# x.department =empl_id.department_id.id

			# if x.check_in!=False:

			#     check_in=self.convert24(x.check_in)
			#     x.attendance_date = str(x.date)+" "+ str(check_in)
			#     x.time = str(check_in)
				
			# if x.check_out!=False:

			#     check_out=self.convert24(x.check_out)
			#     x.attendance_date = str(x.date)+" "+ str(check_out)
			#     x.time = str(check_out)
				
			# if type(x.attendance_date)!= bool:
			# else:
			#     pass


	def convert24(self,str1): 
	  
		# Checking if last two elements of time 
		# is AM and first two elements are 12 
		if str1[-2:] == "AM" and str1[:2] == "12": 
			
			return "00" + str1[2:-2] 
		# remove the AM     
		elif str1[-2:] == "AM": 
			 
			return str1[:-2] 
		# Checking if last two elements of time 
		# is PM and first two elements are 12    
		elif str1[-2:] == "PM" and str1[:2] == "12": 
			print "str1[:-2]" 
			print str1[:-2] 
			return str1[:-2] 
			
		else: 
			print "str(int(str1[:2]) + 12) + str1[2:9]" 
			print str(int(str1[:2]) + 12) + str1[2:9] 
			# add 12 to hours and remove PM 
			return str(int(str1[:2]) + 12) + str1[2:9] 