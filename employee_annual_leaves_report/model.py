#-*- coding:utf-8 -*-
from openerp import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime
import re

class EmployeeWiseAttendence(models.AbstractModel):
	_name = 'report.employee_annual_leaves_report.absent_report'

	@api.model
	def render_html(self,docids, data=None):
		report_obj = self.env['report']
		record_wizard = self.env['employee.annual.leaves.report'].browse(self.env.context.get('active_ids'))
		company = self.env['res.company'].search([])

		typed = record_wizard.typed
		department = record_wizard.department

		if typed == 'specific':
			employees = []
			for x in record_wizard.employee:
				employees.append(x)

		else:
			employees = self.env['hr.employee'].search([])


		if typed == 'department':
			annual_leaves = self.env['annual.leaves'].search([('employee_id.department_id','=',department.id)])
			emp_liste = []
			for al in annual_leaves:
				for emp in al.tree_link_id:
					if emp.leaves_type.name == 'Annual Leaves':
						emp_liste.append({
							'department':department.name,
							'emp_code':emp.annual_leaves_tree.employee_id.emp_code,
							'emp_name':emp.annual_leaves_tree.employee_id.name,
							'job':emp.annual_leaves_tree.employee_id.job_id.name,
							'basic':emp.annual_leaves_tree.employee_id.contract_id.wage,
							'total_leaves':emp.total_leaves,
							'rem_leaves':emp.leave_remaining,
							'avail_leaves':emp.leave_availed,
							})
		if typed == 'specific':
			# print "Spec"
			emp_liste = []
			for e in employees:
				annual_leaves = self.env['annual.leaves'].search([('employee_id','=',e.id)])
				for al in annual_leaves:
					for emp in al.tree_link_id:
						if emp.leaves_type.name == 'Annual Leaves':
							emp_liste.append({
								'department':e.department_id.name,
								'emp_code':e.emp_code,
								'emp_name':e.name,
								'job':e.job_id.name,
								'basic':e.contract_id.wage,
								'total_leaves':emp.total_leaves,
								'rem_leaves':emp.leave_remaining,
								'avail_leaves':emp.leave_availed,
								})

		

		docargs = {
			'doc_ids': docids,
			'doc_model': 'hr.employee',
			'data': data,
			'company': company,
			'department':department,
			'main_data':emp_liste,
			}

		return self.env['report'].render('employee_annual_leaves_report.absent_report', docargs)