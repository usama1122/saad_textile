# -*- coding: utf-8 -*-
{
    'name': "Employee Annual Leaves Report",

    'summary': "Employee Annual Leaves Report",

    'description': "Employee Annual Leaves Report",

    'author': "Siddiq Chauhdry",
    # 'website': "http://www.bcube.pk",

    # any module necessary for this one to work correctly
    'depends': ['base','web','report','hr'],
    # always loaded
    'data': [
        'template.xml',
        'views/module_report.xml',
    ],
    'css': ['static/src/css/report.css'],
}
