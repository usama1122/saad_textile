# #-*- coding:utf-8 -*-
from odoo import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime
from openerp.exceptions import Warning, ValidationError, UserError

class EmployeeAnnualLeavesReport(models.TransientModel):
	_name = "employee.annual.leaves.report"

	employee = fields.Many2many('hr.employee',string="Employee")
	department = fields.Many2one('hr.department',string="Department")

	typed = fields.Selection([
		('department','Department Wise'),
		('specific','Specific'),
		],string='Employee(s)',)

	@api.multi
	def print_report(self):
		data = {}
		data['form'] = self.read(['employee','department','form','to','typed'])[0]
		return self.env['report'].get_action(self, 'employee_annual_leaves_report.absent_report', data=data)