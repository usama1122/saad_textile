# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from datetime import date, datetime, timedelta
from openerp.exceptions import Warning, ValidationError, UserError
from dateutil.relativedelta import relativedelta
from odoo import exceptions, _


class HrOvertime(models.Model):
	_name = 'hr.overtime'
	_inherit = ['mail.thread']
	_rec_name = 'rec_name'
	_order = "id desc"

	date = fields.Date(string="Date From", required= True)
	date_to = fields.Date(string="Date To", required= True)
	department = fields.Many2one('hr.department',string="Department")
	total_overtime_hours = fields.Float(string="Total Overtime Hours")
	total_overtime_amount = fields.Float(string="Total Overtime Amount")
	total_working_days = fields.Integer(string="Total Working Days",default=26)
	percentage = fields.Integer(string="Percentage",track_visibility='onchange')
	tree_link = fields.One2many('hr.overtime.tree','tree_linked')
	rec_name = fields.Char(String="Rec name")
	manual = fields.Boolean()

	stages = fields.Selection([('draft','Draft'),('approved','Approved')],string='Stages',default='draft',track_visibility='onchange')

	
	@api.multi
	def generate_employees(self):
		self.tree_link.unlink()
		if self.department:
			employee = self.env['hr.employee'].search([('active','=',True),('department_id','=',self.department.id)])
			print employee
			for x in employee:
				overtime_approve = self.env['overtime.approve.tree'].search([('approve','=',True),('tree_link.stages','=','validated'),('tree_link.date','>=',self.date),('tree_link.date','<=',self.date_to),('employee_id','=',x.id)])
				actual_overtime_hours = 0
				for o in overtime_approve:
					actual_overtime_hours = actual_overtime_hours + o.actual_overtime_hours
				adv_tree = self.env['hr.overtime.tree'].create({
					'employee': x.id,
					# 'card_no': x.card_no.id,
					'actual_overtime_hours': actual_overtime_hours,
					# 'overtime': x.overtime,
					'tree_linked': self.id,
					})

			for j in self.tree_link:
				j.onchange_card()
				j.manual_hours()

	@api.multi
	def update_values(self):
		if self.tree_link:
			for x in self.tree_link:
				employee = self.env['hr.employee'].search([('card_no.id','=',x.card_no.id)])
				x.employee = employee.id
				contract = self.env['hr.contract'].search([('right_contract','=',True),('employee_id.card_no.id','=',x.card_no.id)])
				if contract:
					if contract.hours:
						hours = contract.hours
					if not contract.hours:
						hours = 8
				else:
					hours = 8
				if x.employee.overtime:
					x.overtime_rate = (employee.salary/26)/hours * 2

				x.overtime_amount = x.overtime_rate * (x.approved_overtime_hours + x.remain_hours)



	@api.onchange('percentage')
	def get_percent(self):
		if self.tree_link:
			for x in self.tree_link:
				x.get_approvedhours()
				x.manual_hours()
				x.onchange_actual()
				

	@api.onchange('date','department')
	def onchange_depart(self):
		self.rec_name = str(self.department.name) + ',' + str(self.date)

	@api.onchange('tree_link')
	def onchange_tree(self):
		actual_overtime = 0

		total_overtime = 0

		for x in self.tree_link:
			actual_overtime = actual_overtime + x.approved_overtime_hours
			total_overtime = total_overtime + x.overtime_amount

		self.total_overtime_hours = actual_overtime
		self.total_overtime_amount = total_overtime
						
	@api.multi
	def in_waiting(self):
		self.stages = "draft"
						
	@api.multi
	def in_validate(self):
		counter = 0
		for x in self.tree_link:
			if x.approved != True:
				counter = counter + 1

		print counter

		if counter == 0:
			self.stages = "approved"
		else:
			raise ValidationError('Please Approve all the Overtimes')

class HrOvertimeTreeView(models.Model):
	_name = 'hr.overtime.tree'


	employee = fields.Many2one('hr.employee',string="Employee", required= True)
	planed_overtime_hours = fields.Float(string="Planed Overtime Hours") 
	working_days = fields.Float(string="Working Days") 
	actual_overtime_hours = fields.Float(string="Actual Overtime Hours")
	approved_overtime_hours = fields.Float(string="Approved Hours")
	remain_hours = fields.Float(string="Extra Hours")
	overtime_rate = fields.Float(string="Overtime Rate")
	overtime_amount = fields.Float(string="Overtime Amount")
	extra_amount = fields.Float(string="Extra Amount")
	approved_amount = fields.Float(string="Approved Amount")
	remarks = fields.Char(string="Remarks")
	approved = fields.Boolean(string="Approved" , default = True)
	overtime = fields.Boolean(string="Overtime" ,readonly=True)
	total_laeves = fields.Float(string="Total Leaves")
	total_absent = fields.Float(string="Total Absent")
    # emp_code = fields.Char(string="Employee Code")

	tree_linked = fields.Many2one('hr.overtime')

	@api.onchange('card_no')
	def onchange_card(self):
		pass
		# if self.card_no:
		# 	employee = self.env['hr.employee'].search([('card_no.id','=',self.card_no.id)])
		# 	self.employee = employee.id
		# 	contract = self.env['hr.contract'].search([('right_contract','=',True),('employee_id.card_no.id','=',self.card_no.id)])
		# 	if contract:
		# 		if contract.hours:
		# 			hours = contract.hours
		# 		if not contract.hours:
		# 			hours = 8
		# 	else:
		# 		hours = 8
		# 	if self.employee.overtime:
		# 		self.overtime_rate = (employee.salary/26)/hours * 2
		# 	leave = self.env['hr.holidays'].search([('employee_id.card_no','=',self.card_no.id),('holiday_status_id.name','=','Leave')])
		# 	absent = self.env['hr.holidays'].search([('employee_id.card_no','=',self.card_no.id),('holiday_status_id.name','=','Absent')])
		# 	self.total_laeves = leave.number_of_days_temp
		# 	self.total_absent = absent.number_of_days_temp
		# 	self.working_days = self.tree_linked.total_working_days - (self.total_absent)


	@api.onchange('actual_overtime_hours')
	def get_approvedhours(self):
		if self.actual_overtime_hours:
			# if self.employee.overtime:
			self.approved_overtime_hours = self.actual_overtime_hours - (self.actual_overtime_hours * self.tree_linked.percentage / 100 )
			# self.approved_overtime_hours = self.actual_overtime_hours * (100-self.tree_linked.percentage)/100
	@api.onchange('approved_overtime_hours')
	def manual_hours(self):
		if self.approved_overtime_hours > 40:
			self.remain_hours =  self.approved_overtime_hours - 40
			self.approved_overtime_hours = 40
		else:
			self.remain_hours = 0


	@api.onchange('total_absent','total_laeves')
	def manual_leaves(self):
		if self.tree_linked.manual:
			self.working_days = self.tree_linked.total_working_days - (self.total_absent)




	@api.onchange('approved_overtime_hours','overtime_rate','actual_overtime_hours','remain_hours')
	def onchange_actual(self):
		self.overtime_amount = self.overtime_rate * (self.approved_overtime_hours + self.remain_hours)
		self.approved_amount = self.overtime_rate * (self.approved_overtime_hours )
		self.extra_amount = self.overtime_rate * (self.remain_hours)


