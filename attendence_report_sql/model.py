#-*- coding:utf-8 -*-
from openerp import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime
import re
import calendar
class EmployeeWiseAttendence(models.AbstractModel):
    _name = 'report.attendence_report_sql.attendence_report'

    @api.model
    def render_html(self,docids, data=None):
        report_obj = self.env['report']
        record_wizard = self.env['actual.attend.wizard'].browse(self.env.context.get('active_ids'))
        company = self.env['res.company'].search([])
        employees_own = self.env['hr.employee'].sudo().search([('user_id','=',self._uid)])

        form = record_wizard.form
        to = record_wizard.to
        typed = record_wizard.typed
        department = record_wizard.department
        shift_id = record_wizard.shift_id

        d1 = datetime.datetime.strptime(record_wizard.form, "%Y-%m-%d")
        d2 = datetime.datetime.strptime(record_wizard.to, "%Y-%m-%d")

        if record_wizard.form:
            year  = int(record_wizard.form[:4])
            month = int(record_wizard.form[5:7])
            day = int(record_wizard.form[8:10])
            month_days =  calendar.monthrange(year,month)[1]
        print "month days"
        print month_days
        print month_days
        print month_days
        print month_days
        delta = d2 - d1
        dates = []
        for i in range(delta.days + 1):
            dates.append((d1 + timedelta(days=i)).strftime('%Y-%m-%d'))

        if typed == 'specific':
            employees = []
            for x in record_wizard.employee:
                employees.append(x)

        if typed == 'department':
            employees = []
            rec = self.env['hr.employee'].search([('department_id.id','=',department.id)])
            # rec = rec.sorted(key=lambda r: r.entity)
            for x in rec:
                employees.append(x)

        # if typed == 'entity' and work_entity == True:
        #     employees = []
        #     actual_attendence = self.env['actual.attendence'].search([('date','>=',form),('date','<=',to),('machine_entity','=',entity.id)])
        #     for x in actual_attendence:
        #         if x.employee_id not in employees:
        #             employees.append(x.employee_id)

        # if typed == 'entity' and work_entity == False:
        #     employees = self.env['hr.employee'].search([('entity.id','=',entity.id)])


        if typed == 'all':
            employees = self.env['hr.employee'].search([])

        size = len(dates)


        def get_toal_days(date_from,date_to,employee):

            employee = self.env['hr.employee'].search([('id','=',self.id)])
            if employee.branch == 1:

                employee_present = self.env['actual.attendence'].search_count([('todaystatus','=','present'),('employee_id','=',employee.id),('date','>=',date_from),('date','<=',date_to)])

                employee_present_holiday = self.env['actual.attendence'].search_count([('todaystatus','in',['holiday','gazetted_holiday']),('checkin','>',0),('employee_id','=',employee.id),('date','>=',date_from),('date','<=',date_to)])

                total_present = (employee_present + employee_present_holiday)
                if employee.daily_wager:
                    return (total_present)

                if total_present:

                    if total_present >= 6 and total_present <= 11:
                        total_present = total_present + 1

                    elif total_present >= 12 and total_present <= 17:
                        total_present = total_present + 2

                    elif total_present >= 18 and total_present <= 23:
                        total_present = total_present + 3

                    elif total_present >= 24 and total_present <= 30:
                        total_present = total_present + 4

                    elif total_present == 31:
                        total_present = total_present + 5

                return (total_present)
            if employee.branch == 2:
                total_present = 0
                employee_present = self.env['actual.attendence'].search_count([('todaystatus','in',['present','annual_leaves','sick_leaves','casual_leaves']),('employee_id','=',employee.id),('date','>=',date_from),('date','<=',date_to)])

                employee_present_holiday = self.env['actual.attendence'].search_count([('todaystatus','in',['holiday','gazetted_holiday']),('employee_id','=',employee.id),('date','>=',employee.joining_date),('date','<=',date_to)])

                total_present = (employee_present + employee_present_holiday)
                return (total_present)



        def getattend(date,employee,attr):
            if attr == 'entry':

                value1 = "checkin"
                dated = date
                employees = employee.id
                table_name = "actual_attendence"
                # if work_entity == True:
                #     self.env.cr.execute("SELECT checkin FROM actual_attendence WHERE employee_id = "+str(employees)+" AND date = '"+dated+"'" +" AND machine_entity = '"+str(entity.id)+"'")
                # else:
                self.env.cr.execute("SELECT checkin FROM actual_attendence WHERE employee_id = "+str(employees)+" AND date = '"+dated+"'")
                result = self._cr.fetchone()
                checkin = 0
                if re.findall(r"[-+]?\d*\.\d+|\d+", str(result)):
                    n = result[0]
                    checkin = n

                if checkin != 0:
                    return checkin
                    # return '{0:02.0f}:{1:02.0f}'.format(*divmod(checkin * 60, 60))
                else: 
                    return "-"
            
            if attr == 'exit':

                value2 = "checkout"
                dated = date
                employees = employee.id
                table_name = "actual_attendence"
                # if work_entity == True:
                #     self.env.cr.execute("SELECT ("+value2+") FROM "+table_name+" WHERE employee_id = "+str(employees)+" AND date = '"+dated+"'" +" AND machine_entity = '"+str(entity.id)+"'")
                # else:
                self.env.cr.execute("SELECT ("+value2+") FROM "+table_name+" WHERE employee_id = "+str(employees)+" AND date = '"+dated+"'")
                result = self._cr.fetchone()
                checkout = 0
                if re.findall(r"[-+]?\d*\.\d+|\d+", str(result)):
                    n = result[0]
                    
                    checkout = n
                if checkout != 0:
                    return checkout
                    # return '{0:02.0f}:{1:02.0f}'.format(*divmod(checkout * 60, 60))
                else: 
                    return "-"
            
            if attr == 'total':

                value3 = "total_time"
                dated = date
                employees = employee.id
                table_name = "actual_attendence"
                # if work_entity == True:
                #     self.env.cr.execute("SELECT ("+value3+") FROM "+table_name+" WHERE employee_id = "+str(employees)+" AND date = '"+dated+"'" +" AND machine_entity = '"+str(entity.id)+"'")
                # else:
                self.env.cr.execute("SELECT ("+value3+") FROM "+table_name+" WHERE employee_id = "+str(employees)+" AND date = '"+dated+"'")
                result = self._cr.fetchone()
                totaled = 0
                if re.findall(r"[-+]?\d*\.\d+|\d+", str(result)):
                    n = result[0]
                    totaled = n

                if totaled != 0:
                    return totaled
                    # return '{0:02.0f}:{1:02.0f}'.format(*divmod(abs(totaled) * 60, 60))
                else: 
                    return "-"
            
            if attr == 'overtime':

                value3 = "overtime_hour"
                status = "present"
                form_date = form
                to_date = to
                employees = employee.id
                table_name = "actual_attendence"
                total_over_time_hour = 0
                total_over_time = 0
                totaled = 0
                # if work_entity == True:
                #     attend_rec_data = self.env['actual.attendence'].search([('employee_id','=',employees),('date','>=',form),('date','<=',to),('total_time','!=',False),('todaystatus','=','present'),('machine_entity','=',entity.id)])
                # else:    
                # attend_rec_data = self.env['actual.attendence'].search([('employee_id','=',employees),('date','>=',form),('date','<=',to),('total_time','!=',False),('todaystatus','=','present')])
                # for x in attend_rec_data:
                #     if x.overtime_hour:
                #         over_split_hour = x.overtime_hour.split(":")
                #         over_mint =  float(over_split_hour[1])
                #         over_time = (over_mint)/60
                #         total_over_time = int(over_split_hour[0]) + float(over_time)
                #         total_over_time = '%.2f' % total_over_time
                #         total_over_time_hour = total_over_time_hour + float(total_over_time)
                totaled = total_over_time_hour
                return float(totaled)
            
            if attr == 'under':
                value3 = "under_hour"
                dated = date
                employees = employee.id
                table_name = "actual_attendence"
                totaled = 0
                total_under_time = 0
                total_under_time_hour = 0
                totaled = 0
                # if work_entity == True:
                #     attend_rec_data = self.env['actual.attendence'].search([('employee_id','=',employees),('date','>=',form),('date','<=',to),('total_time','!=',False),('todaystatus','=','present'),('machine_entity','=',entity.id)])
                # else:
                # attend_rec_data = self.env['actual.attendence'].search([('employee_id','=',employees),('date','>=',form),('date','<=',to),('total_time','!=',False),('todaystatus','=','present')])
                # for x in attend_rec_data:
                #     if x.under_hour:
                #         under_split_hour = x.under_hour.split(":")
                #         under_mint =  float(under_split_hour[1])
                #         under_mint = (under_mint)/60
                #         total_under_time = int(under_split_hour[0]) + float(under_mint)
                #         total_under_time = '%.2f' % total_under_time
                #         total_under_time_hour = total_under_time_hour + float(total_under_time)
                totaled = total_under_time_hour
                return float(totaled)

            if attr == 'real':

                value3 = "total_time"
                dated = date
                employees = employee.id
                table_name = "actual_attendence"
                totaled = 0
                total_work_time = 0
                total_work_time_hour = 0
                # if work_entity == True:
                #     attend_rec_data = self.env['actual.attendence'].search([('employee_id','=',employees),('date','>=',form),('date','<=',to),('total_time','!=',False),('todaystatus','=','present'),('machine_entity','=',entity.id)])
                # else:
                attend_rec_data = self.env['actual.attendence'].search([('employee_id','=',employees),('date','>=',form),('date','<=',to),('total_time','!=',False),('todaystatus','=','present')])

                for x in attend_rec_data:
                    if x.total_time:
                        # work_split_hour = x.total_time.split(":")
                        # work_mint =  float(work_split_hour[1])
                        # work_mint = (work_mint)/60
                        # total_work_time = int(work_split_hour[0]) + float(work_mint)
                        # total_work_time = '%.2f' % total_work_time
                        total_work_time_hour = total_work_time_hour + float(x.total_time)
                totaled = total_work_time_hour
                return float(totaled)

            if attr == 'leave':
                if shift_id:
                    actual = self.env['actual.attendence'].search([('employee_id','=',employee.id),('date','=',date),('shift','=',shift_id.name)])
                else:
                    actual = self.env['actual.attendence'].search([('employee_id','=',employee.id),('date','=',date)])
                
                if actual.todaystatus == 'holiday':
                    return 'OFF'
                
                if actual.todaystatus == 'absent':
                    return 'A'
                
                if actual.todaystatus == 'half_leave':
                    return 'HL'
                
                if actual.todaystatus == 'leave':
                    return 'L'
                
                if actual.todaystatus == 'sick_leaves':
                    return 'SL'
                
                if actual.todaystatus == 'annual_leaves':
                    return 'AL'
                
                if actual.todaystatus == 'gazetted_holiday':
                    return 'GH'
                
                if actual.todaystatus == 'work_from_home':
                    return 'WFH'
                
                if actual.todaystatus == 'special_leave':
                    return 'SPL'
                
                if actual.todaystatus == 'casual_leaves':
                    return 'CL'
                
                if actual.todaystatus == 'marriage_leaves':
                    return 'ML'
                
                if actual.todaystatus == 'short_leave':
                    return 'SHL'

            if attr == 'per_hour':
                per_day_salary = 0
                value3 = "per_day"
                dated = date
                month_salary = employee.xx_salary
                per_day_salary = month_salary / month_days
                print "11111"
                print employee.schedule.name
                if employee.schedule.shift_work_hour:
                    per_day_salary = per_day_salary / employee.schedule.shift_work_hour
                    return float(per_day_salary)
                else:
                    return float(0)


            if attr == 'per_day':
                per_day_salary = 0
                value3 = "per_day"
                dated = date
                if employee.branch == 1:
                    if employee.daily_wager:
                        return (employee.xx_salary)
                    month_salary = employee.xx_salary
                    per_day_salary = month_salary / month_days
                    
                    return float(per_day_salary)
                if employee.branch == 2:
                    month_salary = employee.xx_salary
                    per_day_salary = month_salary / 26
                    
                    return float(per_day_salary)


            if attr == 'diff':
                per_houre = 0
                if employee.schedule:
                    per_day_salary = 0
                    value3 = "diff"
                    dated = date
                    month_salary = employee.xx_salary
                    per_day_salary = month_salary / 30
                    per_houre = per_day_salary / 9
                    # per_houre = per_day_salary / employee.schedule.working_time_hour
                
                # return per_day_salary
                return float(per_houre)
        employees = sorted(employees, key=lambda x: x['name'])
        login_user = self.env['res.users'].search([('id','=',self._uid)])
        docargs = {
            'doc_ids': docids,
            'doc_model': 'hr.employee',
            'data': data,
            'form': form,
            'to': to,
            'employees': employees,
            'dates': dates,
            'getattend': getattend,
            'get_toal_days': get_toal_days,
            'login_user': login_user,
            'company': company,
            # 'entity': entity,
            # 'work_entity': work_entity,
            'size': size
            }

        return report_obj.render('attendence_report_sql.attendence_report', docargs)