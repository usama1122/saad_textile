# #-*- coding:utf-8 -*-
from odoo import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime
from openerp.exceptions import Warning, ValidationError, UserError

class TheBasicAttendenceReport(models.TransientModel):
	_name = "actual.attend.wizard"

	employee = fields.Many2many('hr.employee',string="Employee")
	department = fields.Many2one('hr.department',string="Department")
	shift_id = fields.Many2one('shifts.attendence',string="Shift")
	form = fields.Date(string="From")
	to = fields.Date(string="To")
	typed = fields.Selection([
		('all','ALL'),
		('department','Department Wise'),
		('specific','Specific'),
		],string='Employee(s)',)



	@api.multi
	def print_ledger(self):
		data = {}
		data['form'] = self.read(['employee','department','form','to','typed'])[0]
		return self.env['report'].get_action(self, 'attendence_report_sql.attendence_report', data=data)