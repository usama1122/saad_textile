# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from openerp.exceptions import Warning, ValidationError, UserError
from datetime import datetime,date
from dateutil.relativedelta import relativedelta


class HrContractExt(models.Model):
	_inherit = 'hr.contract'

	overtime = fields.Float(string="Over Time", track_visibility='onchange')
	fuel_allowence = fields.Float(string="Employee/Team of the month", track_visibility='onchange')
	casses_bonus = fields.Float(string="Cases Bonus", track_visibility='onchange')
	aqa_allowence = fields.Float(string="AQA Allowance", track_visibility='onchange')
	leave_incentive = fields.Float(string="Leave Incentive", track_visibility='onchange')
	hafiz_allowance = fields.Float(string="Hafiz Allowance", track_visibility='onchange')
	night_allowance = fields.Float(string="Night Allowance", track_visibility='onchange')
	arrears_amount = fields.Float(string="Arrears Amount", track_visibility='onchange')
	arreras_reason = fields.Char(string="Reason", track_visibility='onchange')

	unpaid_leaves = fields.Float(string="Unpaid Leave", track_visibility='onchange')
	late_arrival = fields.Float(string="Late Arrival", track_visibility='onchange')
	prod_rejection = fields.Float(string="Prod-Rejection", track_visibility='onchange')
	cases_ded = fields.Float(string="Cases Ded", track_visibility='onchange')
	food = fields.Float(string="FOOD", track_visibility='onchange')
	other_deductions = fields.Float(string="Other Deductions", track_visibility='onchange')
	other_reason = fields.Char(string="Reason", track_visibility='onchange')
	arrears_tree = fields.One2many('arrears.history', 'tree_link', track_visibility='onchange')
	other_history_tree = fields.One2many('others.history', 'tree_link', track_visibility='onchange')
	tax_year = fields.Many2one('tax.year')
	taxable_salary = fields.Float(string="Annual Taxable Salary",compute="get_tavable_salary")
	previous_gross_salary = fields.Float(string="Paid Gross Salary" ,compute="get_tavable_salary")
	remaining_no_of_month = fields.Float(string="No of month" ,compute="get_tavable_salary")
	previous_tax_amount = fields.Float(string="Previous Tax Amount" ,compute="get_pre_tax")
	# Button functions
	emp_code = fields.Char(string="Employee ID" ,readonly=True)
	branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')
	gratuity = fields.Float(string="Gratuity Opening", track_visibility='onchange')
	# job_id = fields.Char("hr.job", related="employee_id.job_id",readonly=True)
	# department_id = fields.Char("hr.department", related="employee_id.department_id",readonly=True)

	

	@api.onchange('employee_id')
	def get_branch_id(self):
		if self.employee_id:
			self.emp_code = self.employee_id.emp_code
			if not self.branch:
				self.branch = self.employee_id.branch.id

	@api.multi
	def write(self, vals):
		before=self.write_date
		super(HrContractExt, self).write(vals)
		after = self.write_date
		if before != after:
			self.get_branch_id()

		return True


	@api.model
	def create(self, vals):
		new_record = super(HrContractExt, self).create(vals)
		new_record.get_branch_id()
		return new_record

	def get_employee_gratuity(self):
		print "Hii.. I'm Gratuity Fetching Function"
		print "Hii.. I'm Gratuity Fetching Function"
		contracts = self.env['hr.contract'].search([])
		for x in contracts:
			if x.employee_id:
				print "---------"
				print x.employee_id.name
				print "-----CURRENT----"
				print x.gratuity
				print "-----UPDATED----"
				x.gratuity = x.employee_id.gratuity
				print x.gratuity
				print "*********"


	@api.one
	def get_tavable_salary(self):
		if self.tax_year:
			paid_salaries = self.env['hr.payslip.line'].search([('salary_rule_id.name','=','Gross'),('slip_id.employee_id','=',self.employee_id.id),('slip_id.date_from','>=',self.tax_year.start_date),('slip_id.date_from','<=',self.tax_year.end_date),('slip_id.state','=','done')])
			paid_gross_amount = 0
			total_of_fix_allow_and_basic = 0
			total_of_fix_allow_and_basic = 0
			remaining_month_allowances = 0
			if paid_salaries:
				for amount in paid_salaries:
					paid_gross_amount = paid_gross_amount + amount.amount
			print "Gross salary of previous months of tax year in payslips"
			print paid_gross_amount
			self.previous_gross_salary = paid_gross_amount
			remaining_month = 12 - len(paid_salaries)
			print "remaining month"
			print remaining_month
			self.remaining_no_of_month = remaining_month
			total_of_fix_allow_and_basic = self.wage + self.hafiz_allowance + self.night_allowance + self.fuel_allowence
			
			remaining_month_allowances = self.overtime + self.leave_incentive + self.casses_bonus + self.arrears_amount + self.aqa_allowence
			
			total_salay_amount = ((remaining_month * total_of_fix_allow_and_basic)) + paid_gross_amount  + (2 * self.wage) + remaining_month_allowances
			
			self.taxable_salary = total_salay_amount

	@api.one
	def get_pre_tax(self):
		if self.tax_year:
			fasical_date = date.today()
			date_end =fasical_date
			july_date= fasical_date.replace(day=1,month=7)
			if date_end.month < 7:

				july_date= july_date-relativedelta(years=1)
				date_e=(datetime.strptime(str(july_date),'%Y-%m-%d')+relativedelta(months=11)).strftime('%Y-%m-%d')
				month = str(date_e[5:-3])
				date_end=datetime.strptime(str(date_e), '%Y-%m-%d').replace(day=30,month=int(month))
			else:
				date_e=(datetime.strptime(str(july_date),'%Y-%m-%d')+relativedelta(months=11)).strftime('%Y-%m-%d')
				print date_e
				print date_e
				month = str(date_e[5:-3])
				print month
				print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa"
				print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa"
				print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa"
				date_end=datetime.strptime(str(date_e), '%Y-%m-%d').replace(day=30,month=int(month))
				print date_end
				print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa"
				print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa"
				print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaa"

			paid_tax = self.env['hr.payslip.line'].search([('salary_rule_id.name','=','Tax Deduction'),('slip_id.employee_id','=',self.employee_id.id),('slip_id.date_from','>=',july_date.strftime('%Y-%m-%d')),('slip_id.date_from','<=',date_end),('slip_id.state','=','done')])
			paid_tax_amount = 0
			print "1111111111111"
			print paid_tax
			print paid_tax
			if paid_tax:
				for amount in paid_tax:
					print amount.amount
					paid_tax_amount = paid_tax_amount + amount.amount
			self.previous_tax_amount = paid_tax_amount


	@api.multi
	def update_arrears (self):
		if self.arrears_amount:
			history_rec = self.env['arrears.history'].create({
				'date':datetime.today(),
				'arrears_amount':self.arrears_amount,
				'reason':self.arreras_reason,
				'tree_link':self.id,
				})


	@api.multi
	def update_deduction (self):
		if self.other_deductions:
			history_rec = self.env['others.history'].create({
				'date':datetime.today(),
				'others_deduction_amount':self.other_deductions,
				'reason':self.other_reason,
				'tree_link':self.id,
				})


class ArrearsHistory(models.Model):
	_name = 'arrears.history'
	_rec_name = 'date'

	date = fields.Date(string="Date")
	arrears_amount = fields.Float(string="Arrears Amount")
	reason = fields.Char(string="Reason")
	tree_link = fields.Many2one('hr.contract')

class OtherHistoryTree(models.Model):
	_name = 'others.history'
	_rec_name = 'date'

	date = fields.Date(string="Date")
	others_deduction_amount = fields.Float(string="Others Deduction Amount")
	reason = fields.Char(string="Reason")
	tree_link = fields.Many2one('hr.contract')

class TaxYear(models.Model):
	_name = 'tax.year'
	_rec_name = 'rec_name'

	start_date = fields.Date(string="Start Date")
	rec_name = fields.Char(string="Name" ,compute="_setRecordName",store=True)
	end_date = fields.Date(string="End Date")

	@api.one
	@api.depends('start_date', 'end_date')
	def _setRecordName(self):
		if self.start_date and self.end_date:
			startyear = datetime.strptime(self.start_date, '%Y-%m-%d').strftime('%Y')
			endyear = datetime.strptime(self.end_date, '%Y-%m-%d').strftime('%Y')
			self.rec_name = startyear +"-"+ endyear

