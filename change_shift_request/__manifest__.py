# -*- coding: utf-8 -*-
{
    'name': "Change Shift Request ",

    'summary': """
        Change Shift  Request """,

    'description': """
        You can make Shift Change Request of employees.


Main Features
-------------
* Manually Change Shift to the system.
* Save  Changes to Shift to the sysytem.
* Can show  Changes to Shift of employee if present.
    """,

    'author': "Hamza Azeem Qureshi ,Enterprise Cube (pvt) ltd",
    'website': "https://ecube.pk",
    'sequence': '10',
    'category': 'shift_change',
    'version': '0.1',
 

    # any module necessary for this one to work correctly
    'depends': ['base','hr','hr_employees'],

    # always loaded
    'data': [
        'views/views.xml',
    ],

    # only loaded in demonstration mode
    'demo': [

    ],

    'auto_install': False,
    'installable': True,
    # 'images': ['static/description/storage.JPG'],
}
