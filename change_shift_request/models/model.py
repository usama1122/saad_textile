from odoo import models, fields,service ,api, _
from odoo.exceptions import Warning, ValidationError, UserError
from datetime import timedelta,datetime,date


class ChangeShiftRequest(models.Model):


	_name = 'shift.change'
	_rec_name = 'employee_id'
	_inherit = ['mail.thread']


	emp_code = fields.Char(string='Employee Id',track_visibility='onchange',required=True,)
	employee_id = fields.Many2one('hr.employee',string="Employee Name")
	manager_id = fields.Many2one('hr.employee',string="Manager ",related="employee_id.parent_id",)
	current_shift_id= fields.Many2one('shifts.attendence',string="Previous Shift",readonly=True, )
	from_date= fields.Date(string='From Date')
	to_date= fields.Date(string='To Date')
	reason= fields.Char(string='Reason')
	requested_shift_id= fields.Many2one('shifts.attendence',string="Requested Shift",track_visibility='onchange')
	branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')
	state = fields.Selection([
		('draft', 'Draft'),
		('manager_approve',' Approved By Manager'),
		('validate', 'Validate'),
		],default='draft' ,track_visibility='onchange')

	
	@api.onchange('employee_id')
	def get_branch_id(self):
		if self.employee_id:
			self.emp_code = self.employee_id.emp_code
			if not self.branch:
				self.branch = self.employee_id.branch.id

	@api.multi
	def write(self, vals):
		before=self.write_date
		super(ChangeShiftRequest, self).write(vals)
		after = self.write_date
		if before != after:
			self.get_branch_id()

		return True


	@api.model
	def create(self, vals):
		new_record = super(ChangeShiftRequest, self).create(vals)
		new_record.get_branch_id()
		return new_record

	@api.multi
	def manager_approve(self):
		self.state = "manager_approve"


	@api.multi
	def is_validate(self): 
		self.state = "validate"
		actual_attendnace = self.env['actual.attendence'].search([('date','>=',self.from_date),('date','<=',self.to_date),('employee_id','=',self.employee_id.id)])
		print "check 1"
		print actual_attendnace
		if actual_attendnace:
			for actual in actual_attendnace:
				print "set shift button start"
				print actual
				actual.set_shift()
				print "---------------------"
	
	@api.multi
	def is_draft(self):
		self.state = "draft"
		actual_attendnace = self.env['actual.attendence'].search([('date','>=',self.from_date),('date','<=',self.to_date),('employee_id','=',self.employee_id.id)])
		if actual_attendnace:
			for actual in actual_attendnace:
				actual.shift = self.current_shift_id.name
				actual.shift_id = self.current_shift_id.id
				actual.set_shift()

	# @api.multi
	# def write(self, vals):
		
	# 	rec = super(ChangeShiftRequest, self).write(vals)
	# 	self._check_name()

		
	# 	return rec


	# @api.model
	# def create(self, vals):
	# 	new_record = super(ChangeShiftRequest, self).create(vals)
	# 	new_record._check_name()
	# 	return new_record


# if ( not (w_order.date_planned_finished <= start_time or w_order.date_planned_start >= virtual_end)):

	# @api.constrains('from_date','to_date')
	# def _check_name(self):
	# 	record=self.env['shift.change'].search([('employee_id','=',self.employee_id.id),('id','!=',self.id)])
	# 	print record
	# 	for record_employee in record:	
	# 		print "LooppLoopp"
	# 		print record_employee

			# if ( not (record_employee.to_date <= self.from_date or record_employee.from_date <= self.to_date )):
			# if ( not (record_employee.from_date <= self.from_date or record_employee.to_date <= self.to_date )):
				
			
			# 	pass
			# else:
				# print "kkkkkkkkkkkkkk"
				# raise ValidationError("Date overlaps for already existing Employee.")
				# print "SSSSSSSSSSSSSSSSSSSS"
		# print "SSSSSSSSSSSSSSSSSSSS"
		# return True

	@api.constrains('from_date', 'to_date', 'employee_id')
	def _check_dates(self):
	
		for check in self:
			# Starting date must be prior to the ending date
			from_date = check.from_date
			to_date = check.to_date
			if to_date < from_date:
				raise ValidationError('The ending date must not be prior to the starting date.')


			domain = [
				('id', '!=', check.id),
				('employee_id', '=', check.employee_id.id),
				'|', '|',
				'&', ('from_date', '<=', check.from_date), ('to_date', '>=', check.from_date),
				'&', ('from_date', '<=', check.to_date), ('to_date', '>=', check.to_date),
				'&', ('from_date', '<=', check.from_date), ('to_date', '>=', check.to_date),
			]

			if self.search_count(domain) > 0:
				raise ValidationError("Date overlaps for already existing Employee.")

	# _constraints = [
	# 	(_check_name, 'The Project should be linked to a Sale Order to select an Sale Order Items.',''),
	# ]


	@api.onchange('employee_id')
	def _onchange_employee(self):
		if self.employee_id:
			self.emp_code =self.employee_id.emp_code
			self.current_shift_id =self.employee_id.schedule.id
		else:
			self.emp_code =None
			self.current_shift_id = None


	@api.onchange('emp_code')
	def _onchange_emp_code(self):
		if self.emp_code:
			employee_id = self.env['hr.employee'].search([('emp_code','=',self.emp_code)],limit=1)
			if employee_id:
				self.employee_id =employee_id.id
				self.current_shift_id =employee_id.schedule.id
	
				self.claim_type=False
		
		else:
			self.employee_id =None
			self.current_shift_id = None


