# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from openerp.exceptions import ValidationError	
class struct_user_extend(models.Model):
	_inherit  = 'res.users'
	branch = fields.Many2one ('branch',string="Branch")

class branchAAA(models.Model):
	_name = 'branch'

	address = fields.Char(string="Address")
	name = fields.Char(string="Name")
	phone = fields.Char(string="Phone")
	mobile = fields.Char(string="Mobile")
	state = fields.Selection([
		('draft', 'Draft'),
		('validate', 'Validate'),
		], default = "draft")

	@api.multi
	def unlink(self):
		for x in self:
			if x.state == 'validate':
				raise ValidationError('Cannot delete record in this stage.')
		res = super(branchAAA, self).unlink()

		return res

	@api.multi
	def draft_pressed(self):
		self.state = "draft"

	@api.multi
	def validate_pressed(self):
		self.state = "validate"