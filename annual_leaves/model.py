# -*- coding: utf-8 -*- 
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.exceptions import ValidationError
from openerp.exceptions import Warning, ValidationError, UserError
import datetime as dt
from datetime import date, datetime, timedelta
import time
import calendar
# from dateutil.relativedelta import relativedelta
from dateutil.relativedelta import relativedelta

class AnnualLeavesForm(models.Model):
	_name = 'annual.leaves'
	_inherit = ['mail.thread','ir.needaction_mixin']
	_rec_name = 'employee_id'

	employee_id = fields.Many2one('hr.employee',string="Employee",copy=False,track_visibility='onchange')
	card_no = fields.Many2one('emp.card.num',string="Card No" ,copy=False,track_visibility='onchange')
	year = fields.Many2one('new.year',string="Year" ,required=True)
	total_leaves = fields.Float(string="Total Leaves")
	leave_remaining = fields.Float(string="Remaining Leaves")
	leave_availed = fields.Float(string="Availed Leaves")
	tree_link_id = fields.One2many('annual.leaves.tree','annual_leaves_tree',track_visibility='onchange')
	tree_link_holidays = fields.One2many('hr.holidays','availed_leaves_tree')
	stages = fields.Selection([
		('draft',' Un Approve'),
		('validated','Approve'),
		('done','Done'),
		],default="draft",copy=False,track_visibility='onchange')

	emp_code = fields.Char(string="Employee ID")
	active = fields.Boolean(string="Active" , default=True)

	@api.multi
	def in_draft(self):
		self.stages = "draft"
						
	@api.multi
	def in_validate(self):
		self.stages = "validated"


	@api.onchange('emp_code','employee_id')
	def get_employee_card_no(self):
		if self.employee_id:
			self.emp_code = self.employee_id.emp_code
		else:
			if self.emp_code:
				emp = self.env['hr.employee'].search([('emp_code','=',self.emp_code)])
				if emp:
					self.employee_id = emp.id


	@api.onchange('tree_link_id')
	def get_total_leaves(self):
		if self.tree_link_id:
			total_leaves = 0
			for x in self.tree_link_id:
				total_leaves = total_leaves + x.total_leaves
			self.total_leaves = total_leaves

	@api.onchange('tree_link_holidays')
	def get_leave_availed(self):
		leave_availed = 0.0
		if self.tree_link_holidays:
			for x in self.tree_link_holidays:
				leave_availed = leave_availed + x.number_of_days_temp_ecube

		opening = 0.0
		for tree in self.tree_link_id:
			opening = opening + tree.leave_availed_opening
		

		self.leave_availed = float(leave_availed) + float(opening)

	@api.onchange('total_leaves','leave_availed')
	def get_leave_remaining(self):
		self.leave_remaining = self.total_leaves - self.leave_availed

	@api.multi
	def set_data(self):
		all_leaves = self.env['annual.leaves'].search([('stages','!=','done')])
		for annual in all_leaves:

			for tree in annual.tree_link_id:
				if tree.leave_type_char == "Annul leave":
					tree.leaves_type =  6
				if tree.leave_type_char == "casual leave":
					tree.leaves_type =  7
				
				tree.get_leave_remaining()

			emp = self.env['hr.employee'].search([('emp_code','=',str(annual.emp_code))])
			annual.employee_id = emp.id
			
			annual.get_total_leaves()
			annual.get_leave_availed()
			annual.get_leave_remaining()
			
			annual.stages = "validated"

	@api.multi 
	def unlink(self): 
		for x in self: 
			if x.stages != "draft": 
				raise ValidationError('Cannot Delete Record') 
		return super(AnnualLeavesForm,self).unlink()

	@api.multi 
	def update_leave(self):
		if self.tree_link_id:
			for x in self.tree_link_id:
				leave = 0 
				for y in self.tree_link_holidays:
					if x.leaves_type == y.holiday_status_id:
						leave = leave + y.number_of_days_temp_ecube
				x.leave_availed = leave
				x.leave_remaining = x.total_leaves - (x.leave_availed + x.leave_availed_opening)

class AnnualLeavesTree(models.Model):
	_name = 'annual.leaves.tree'

	leaves_type = fields.Many2one('hr.holidays.status',copy=False,string="Leaves Type",required=True)
	total_leaves = fields.Float(string="Total Leaves")
	leave_availed = fields.Float(string="Leave Availed")
	leave_availed_opening = fields.Float(string="Leave Availed Opening")
	leave_remaining = fields.Float(string="Leave Remaining")
	annual_leaves_tree = fields.Many2one('annual.leaves')
	leave_type_char = fields.Char('Leave Type')


	@api.onchange('total_leaves')
	def get_leave_remaining (self):
		self.leave_remaining = self.total_leaves - (self.leave_availed + self.leave_availed_opening)

class AvailedLeavesTree(models.Model):
	_inherit = 'hr.holidays'

	active =fields.Boolean('Active',default = True)
	day_type = fields.Selection([
		('fulday','Full'),
		('half','Half'),
		],default="fulday",string="Day Type")
	including_holidays = fields.Boolean('Including Holidays')
	readonly_days_type = fields.Boolean('Readonly Days Type')
	actual_attendence_status = fields.Boolean('Actual Attendence Status')
	date_from = fields.Datetime('Date From', default=datetime.today())
	date_to = fields.Datetime('Date To', default=datetime.today())
	date_from_ecube = fields.Date('Date From',default=datetime.today())
	date_to_ecube = fields.Date('Date To')
	paid_leave = fields.Float('Paid Leave')
	without_paid_leave = fields.Float('Without Paid Leave')
	actual_attendence_id = fields.Integer('Actual Attendence ID')
	number_of_days_temp_ecube = fields.Float(string="Days" ,compute="get_number_of_days_temp_ecube")
	leave_remaining = fields.Float(string="Remaining Leaves",compute="get_number_of_days_temp_ecube")
	availed_leaves_tree = fields.Many2one('annual.leaves')
	employee_manager = fields.Many2one('hr.employee' , string="Manager" , related="employee_id.parent_id")
	emp_code = fields.Char(string="Employee ID")
	# emp_code = fields.Char(string="Employee ID" , related="employee_id.emp_code")
	is_short_leave =  fields.Boolean('Is a Short Leave')
	# _sql_constraints = [    
	#     ('date_check2', "CHECK(1=1)", "yooooooooooo"),
	# ]


	def check_short_leaves_in_month(self):

		if self.holiday_status_id.name == "Short leave":
			self.is_short_leave = True
			self.date_to_ecube = self.date_from_ecube
			last_day = datetime.strptime(self.date_from_ecube, '%Y-%m-%d') + relativedelta(day=1, months=+1, days=-1)
			first_day = datetime.strptime(self.date_from_ecube, '%Y-%m-%d') + relativedelta(day=1)
			print "/////////////////"
			# ('state', 'not in', ['cancel', 'refuse']),
			short_leaves = self.env['hr.holidays'].search_count([('date_from_ecube', '>=', first_day),('date_to_ecube', '<=', last_day),('employee_id','=',self.employee_id.id),('holiday_status_id','=',self.holiday_status_id.id)])
			if short_leaves > 2:
				raise ValidationError('You Have Already Applied for 2 Short Leaves This Month')


	@api.onchange('holiday_status_id')
	def on_changing_leave_type(self):
		if self.holiday_status_id.name == "Short leave":
			self.is_short_leave = True
			self.date_to_ecube = self.date_from_ecube
		if self.holiday_status_id.name != "Short leave":
			self.is_short_leave = False



	@api.onchange('paid_leave','without_paid_leave')
	def onchangeleaves(self):
		if self.number_of_days_temp_ecube:
			if self.paid_leave > self.number_of_days_temp_ecube:
				self.paid_leave = 0
				return {'value':{},'warning':{'title':
						'warning','message':"Paid leaves must be less then Leave Days"}}
			
			if self.without_paid_leave > self.number_of_days_temp_ecube:
				self.without_paid_leave = 0
				return {'value':{},'warning':{'title':
						'warning','message':"UnPaid leaves must be less then Leave Days"}}


			if self.paid_leave:
				self.without_paid_leave = self.number_of_days_temp_ecube - self.paid_leave
			
			if self.without_paid_leave:
				self.paid_leave = self.number_of_days_temp_ecube - self.without_paid_leave

	def auto_update_paid_leaves(self):
		if self.paid_leave == 0 and self.without_paid_leave == 0:
			self.paid_leave = self.number_of_days_temp_ecube


	@api.model
	def create(self, vals):
	  # self.constraint_remove()
		new_record = super(AvailedLeavesTree, self).create(vals)
		new_record.overlaps()
		new_record.check_short_leaves_in_month()
		new_record.set_date_duration()
		
		new_record.update_datefrom_dateto()
		new_record.auto_update_paid_leaves()
		new_record.onchange_number_of_days_temp_ecube()
		# new_record.update_datefrom_dateto()
		# new_record.short_leave_limit()

		return new_record


	 #  def check_short_leaves_in_month(self):
		# first_date = datetime.strptime((str(self.date_from_ecube.split('-')[0]) + '-' + str(01) + '-' + str(01)), '%Y-%m-%d')

		# end_date_of_month =  datetime.strptime((str(self.date_from_ecube.split('-')[0]) + '-' + str(12) + '-' + str(31)), '%Y-%m-%d')

		# print "/////////////////"
		# print "/////////////////"
		# print first_date
		# print "/////////////////"
		# print end_date_of_month
		# print "/////////////////"
		# print "/////////////////"

		 #  leave = self.env['hr.holidays'].search([('date_to_ecube', '>=', self.date_from_ecube),('date_from_ecube', '<=', self.date_to_ecube),('employee_id','=',self.employee_id.id),('id', '!=', self.id),('state', 'not in', ['cancel', 'refuse'])])
			# if leave:
			# 	raise ValidationError('You can not have 2 leaves that overlaps on same day!')


	# # @api.multi
	# @api.onchange('number_of_days_temp')
	# def constraint_remove(self):
	#     if self.date_from_ecube == self.date_to_ecube:
	#         self.number_of_days_temp = 1
	#         # print("XXXXXXXXXXXXXXXXXXXXXXXX")
	#         # print self.date_to_ecube
	#         # print("XXXXXXXXXXXXXXXXXXXXXXXX")
	#         timenow = str(self.date_to_ecube) +' 01:02:03'
	#         new_date = dt.datetime.strptime(timenow, '%Y-%m-%d %H:%M:%S')
	#         self.date_to = new_date
	#         # print("XXXXXXXXXXXXXXXXXXXXXXXX")
	#         # print self.date_to
	#         # print("XXXXXXXXXXXXXXXXXXXXXXXX")
	#         # self.number_of_days_temp = 1

	#         if self.date_to_ecube:
	#             timenow = str(self.date_to_ecube) +' 01:02:03'
	#             new_date = dt.datetime.strptime(timenow, '%Y-%m-%d %H:%M:%S')
	#             self.date_to = new_date
	#             # self.number_of_days_temp = 1
	#             # self.date_to += dt.timedelta(hours=2, minutes=59)

	#     # print("XXXXXXXXXXXXXXXXXXXXXXXX")
	#     # print("XXXXXXXXXXXXXXXXXXXXXXXX")

	@api.multi
	def write(self, vals):


		before = self.write_date
		result = super(AvailedLeavesTree, self).write(vals)
		after = self.write_date
		
		if before != after:
			# self.constraint_remove()

			self.check_short_leaves_in_month()
			self.set_date_duration()
			self.overlaps()
			self.update_datefrom_dateto()
			self.auto_update_paid_leaves()
			self.onchange_number_of_days_temp_ecube()
			# self.short_leave_limit()

		return result

	@api.constrains('date_from', 'date_to')
	def _check_date(self):
		for holiday in self:
			domain = [
				('date_from', '<=', holiday.date_to),
				('date_to', '>=', holiday.date_from),
				('employee_id', '=', holiday.employee_id.id),
				('id', '!=', holiday.id),
				('type', '=', holiday.type),
				('state', 'not in', ['cancel', 'refuse']),
			]
			nholidays = self.search_count(domain)
			if nholidays:
				raise ValidationError('You can not have 2 leaves that overlaps on same day!')

	
	@api.onchange('date_from_ecube','date_to_ecube')
	def update_datefrom_dateto(self):
		if self.holiday_status_id.name == "Short leave" and self.is_short_leave == True:
			self.date_to_ecube = self.date_from_ecube
		
		if self.date_from_ecube and self.date_to_ecube:
			self.date_from = self.date_from_ecube
			self.date_to = self.date_to_ecube

	@api.onchange('card_no')
	def get_employee_card_no_detialed(self):
		emp = self.env['hr.employee'].search([('card_no','=',self.card_no.id)])
		if emp.card_no:
			self.employee_id = emp.id
			self.department_id = emp.department_id.id

	@api.onchange('employee_id')
	def get_employee_detialed(self):
		if self.employee_id:
			self.card_no = self.employee_id.card_no.id
			self.department_id = self.employee_id.department_id.id

	def set_date_duration(self):
		if self.date_from_ecube and self.date_to_ecube:
			if self.date_from_ecube > self.date_to_ecube:
				raise ValidationError('Please set the Date Duration')

	@api.one
	@api.depends('employee_id','holiday_status_id','date_from_ecube','date_to_ecube','including_holidays','day_type')
	

	def get_number_of_days_temp_ecube(self):

		if self.holiday_status_id:
			if self.holiday_status_id.name in ['Sick Leaves','Annual Leaves','Casual Leaves']:
				self.readonly_days_type = False
			else:
				self.day_type = 'fulday'
				self.readonly_days_type = True
		days = 0
		if self.date_from_ecube and self.date_to_ecube:
			# days = int(self.date_to_ecube[-2:]) - int(self.date_from_ecube[-2:])

			get_days = datetime.strptime(self.date_to_ecube,'%Y-%m-%d') - datetime.strptime(self.date_from_ecube,'%Y-%m-%d')
			days = get_days.days
				# days = day + 1 

		hold = 0
		if self.including_holidays == True:
			holidays = self.env['holidays.tree'].search([('date','>=',self.date_from_ecube),('date','<=',self.date_to_ecube)])
			for x in holidays:
				hold = hold + 1
		if self.day_type == 'half':
			self.number_of_days_temp_ecube = 0.5
		else:
			if self.date_from_ecube and self.date_to_ecube:
				if self.date_from_ecube == self.date_to_ecube:
					self.number_of_days_temp_ecube = 1
				else:
					self.number_of_days_temp_ecube = days + 1
					
					# if self.date_from_ecube > self.date_to_ecube:
					# else:
					#     self.number_of_days_temp_ecube = days

		if self.including_holidays == True:
			self.number_of_days_temp_ecube = hold + days

		annual_lev = self.env['annual.leaves.tree'].search([('annual_leaves_tree.employee_id','=',self.employee_id.id),('leaves_type','=',self.holiday_status_id.id),('annual_leaves_tree.stages','=','validated')])
		if annual_lev:
			if self.date_from_ecube:
				if str(annual_lev.annual_leaves_tree.year.name) == str(self.date_from_ecube[:4]):
					self.leave_remaining = annual_lev.leave_remaining

	@api.onchange('number_of_days_temp_ecube')
	def onchange_number_of_days_temp_ecube(self):
		if self.day_type == 'half':
			self.date_to_ecube = self.date_from_ecube

	def action_approve(self):
		new_record = super(AvailedLeavesTree, self).action_approve()

		if self.employee_id.can_approve_leave == False:
			user = self.env['res.users'].search([('id','=',self._uid)])
			if user.id == self.employee_id.user_id.id:
				raise ValidationError('Access Denied')

		annual = self.env['annual.leaves'].search([('employee_id','=',self.employee_id.id)])
		for x in annual:
			if str(x.year.name) == str(self.date_from_ecube[:4]):
				self.availed_leaves_tree = x.id
				x.update_leave()
				x.get_leave_availed()
		if self.number_of_days_temp_ecube > self.leave_remaining:
			if self.holiday_status_id.name in ['Short leave','Marriage Leaves','Special leave','Work From Home']:
				pass
			else:
				raise ValidationError('Company Can not allow Leaves')
		self.attendence_status_update()

		return new_record

	def attendence_status_update_future_attendnace(self):
		
		if self.day_type == 'fulday':
			actual = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','>=',self.date_from_ecube),('date','<=',self.date_to_ecube)])

			actual_count = self.env['actual.attendence'].search_count([('employee_id','=',self.employee_id.id),('date','>=',self.date_from_ecube),('date','<=',self.date_to_ecube)])
			
			attendence_status = 0 
			if actual_count == self.number_of_days_temp_ecube:
				attendence_status = 1

			count_paid = 1
			for x in actual:
				if x.todaystatus == 'absent':
					x.todaystatus = 'leave'
					if self.holiday_status_id.name == 'Sick Leaves':
						x.todaystatus = 'sick_leaves'
					
					if self.holiday_status_id.name == 'Annual Leaves':
						x.todaystatus = 'annual_leaves'
					
					if self.holiday_status_id.name == 'Special leave':
						x.todaystatus = 'special_leave'
					
					if self.holiday_status_id.name == 'Casual Leaves':
						x.todaystatus = 'casual_leaves'
					
					if self.holiday_status_id.name == 'Marriage Leaves':
						x.todaystatus = 'marriage_leaves'
					
					if self.holiday_status_id.name == 'Work From Home':
						x.todaystatus = 'work_from_home'
					
					if self.holiday_status_id.name == 'Short leave':
						# x.todaystatus = 'short_leave'
						x.short_leave = 'short_leave'
					
					x.leave_id_bool = True
					x.leave_id = self.id
					if attendence_status == 1:
						self.actual_attendence_status = True
					count_paid = count_paid + 1


		if self.day_type == 'half':
			actual_attendence = self.env['actual.attendence'].search([('date','=',self.date_from_ecube),('employee_id','=',self.employee_id.id)])
			if actual_attendence:
				actual_attendence.todaystatus = 'half_leave'

				if self.holiday_status_id.name == 'Sick Leaves':
					actual_attendence.todaystatus = 'sick_leaves'
				
				if self.holiday_status_id.name == 'Annual Leaves':
					actual_attendence.todaystatus = 'annual_leaves'
				
				if self.holiday_status_id.name == 'Casual Leaves':
					actual_attendence.todaystatus = 'casual_leaves'



				actual_attendence.leave_id_bool = True
				actual_attendence.leave_id = self.id
				self.actual_attendence_status = True
				self.actual_attendence_id = actual_attendence.id


	def attendence_status_update(self):
		self.actual_attendence_full_day_leave_approve()
		self.half_leave_update()

	def actual_attendence_full_day_leave_approve(self):
		if self.day_type == 'fulday':
			actual = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','>=',self.date_from_ecube),('date','<=',self.date_to_ecube)])
			print (actual)

			actual_count = self.env['actual.attendence'].search_count([('employee_id','=',self.employee_id.id),('date','>=',self.date_from_ecube),('date','<=',self.date_to_ecube)])
			
			attendence_status = 0 
			if actual_count == self.number_of_days_temp_ecube:
				attendence_status = 1

			count_paid = 1
			for x in actual:
				if x.todaystatus == 'absent':
					x.todaystatus = 'leave'
					if self.holiday_status_id.name == 'Sick Leaves':
						x.todaystatus = 'sick_leaves'
					
					if self.holiday_status_id.name == 'Annual Leaves':
						x.todaystatus = 'annual_leaves'
					
					if self.holiday_status_id.name == 'Special leave':
						x.todaystatus = 'special_leave'
					
					if self.holiday_status_id.name == 'Casual Leaves':
						x.todaystatus = 'casual_leaves'
					
					if self.holiday_status_id.name == 'Marriage Leaves':
						x.todaystatus = 'marriage_leaves'
					
					if self.holiday_status_id.name == 'Work From Home':
						x.todaystatus = 'work_from_home'
					
					if self.holiday_status_id.name == 'Short leave':
						x.todaystatus = 'short_leave'
					
					x.leave_id_bool = True
					x.leave_id = self.id
					if attendence_status == 1:
						self.actual_attendence_status = True
					count_paid = count_paid + 1
				else:
					if self.holiday_status_id.name == 'Short leave':
						x.todaystatus = 'short_leave'
					else:
						raise ValidationError('This Employee is present in the mentioned date')


	def actual_attendence_full_day_leave_approve_revers(self):
		if self.day_type == 'fulday':
			actual = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','>=',self.date_from_ecube),('date','<=',self.date_to_ecube)])

			count_paid = 1
			for x in actual:
				# if count_paid <= self.paid_leave:
				if x.todaystatus in ['leave','sick_leaves','annual_leaves','special_leave','casual_leaves','marriage_leaves','short_leave','work_from_home']:
					x.todaystatus = 'absent'
					x.leave_id_bool = False
					x.leave_id = 0
					x.set_shift()
					self.actual_attendence_status = False
					count_paid = count_paid + 1


	def half_leave_update(self):
		if self.day_type == 'half':
			actual_attendence = self.env['actual.attendence'].search([('date','=',self.date_from_ecube),('employee_id','=',self.employee_id.id)])
			if actual_attendence:
				actual_attendence.todaystatus = 'half_leave'

				if self.holiday_status_id.name == 'Sick Leaves':
					actual_attendence.todaystatus = 'sick_leaves'
				
				if self.holiday_status_id.name == 'Annual Leaves':
					actual_attendence.todaystatus = 'annual_leaves'
				
				if self.holiday_status_id.name == 'Casual Leaves':
					actual_attendence.todaystatus = 'casual_leaves'



				actual_attendence.leave_id_bool = True
				actual_attendence.leave_id = self.id
				self.actual_attendence_status = True
				self.actual_attendence_id = actual_attendence.id


	def half_leave_update_revers(self):
		if self.day_type == 'half':
			actual_attendence = self.env['actual.attendence'].search([('date','=',self.date_from_ecube),('employee_id','=',self.employee_id.id)])
			if actual_attendence:
				actual_attendence.todaystatus = 'absent'
				actual_attendence.leave_id_bool = False
				actual_attendence.leave_id = 0
				actual_attendence.set_shift()
				self.actual_attendence_status = False
				self.actual_attendence_id = actual_attendence.id


	def overlaps(self):
		leave = self.env['hr.holidays'].search([('date_to_ecube', '>=', self.date_from_ecube),('date_from_ecube', '<=', self.date_to_ecube),('employee_id','=',self.employee_id.id),('id', '!=', self.id),('state', 'not in', ['cancel', 'refuse'])])
		if leave:
			raise ValidationError('You can not have 2 leaves that overlaps on same day!')

	def action_refuse(self):
		new_action_refuse = super(AvailedLeavesTree, self).action_refuse()
		for record in self:

			annual = self.env['annual.leaves'].search([('employee_id','=',record.employee_id.id)])
			print annual
			for x in annual:

				if str(x.year.name) == str(record.date_from_ecube[:4]):
					record.availed_leaves_tree = False
					x.update_leave()
					x.get_leave_availed()
					
			record.actual_attendence_full_day_leave_approve_revers()
			record.half_leave_update_revers()
			# self.action_confirm()

		return new_action_refuse

class NewYearForm(models.Model):
	_name = 'new.year'

	name = fields.Char('Name')

