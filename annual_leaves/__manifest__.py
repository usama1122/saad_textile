{ 
    'name': 'Annual Leaves',

    'summary': 'Annual Leaves', 

    'description': 'Annual Leaves', 

    'author': 'Rana Rizwan',

    'website': "http://www.bcube.com",

    'depends': ['base','web_readonly_bypass','hr','hr_holidays'], 

    'application': True, 

    'data': [
    	'views/view.xml',
    	'menu.xml',
    ], 
}
