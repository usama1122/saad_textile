# -*- coding: utf-8 -*- 
from odoo import models, fields, api
from datetime import datetime, timedelta , date
import time
import calendar
from dateutil.relativedelta import relativedelta
import re

class DpsHrDashboard(models.Model): 
	_name = 'dps.hr.dashboard'
	_rec_name = 'name'

	

	name = fields.Char(string="Name")
	
	# emp_present = fields.Integer(string="Present",compute='calc_present_emp_saad1')
	emp_present = fields.Integer(string="Present",compute=lambda self: self.calc_present_emp_saad1("present"))
	emp_present_2 = fields.Integer(string="Present",compute=lambda self: self.calc_present_emp_saad2("present"))
	emp_absent = fields.Integer(string="Absent",compute=lambda self: self.calc_present_emp_saad1("absent"))
	emp_absent_2 = fields.Integer(string="Absent",compute=lambda self: self.calc_present_emp_saad2("absent"))
	emp_on_leave = fields.Integer(string="Absent")
	emp_on_casual_leave = fields.Integer(string="Absent")
	emp_on_short_leave = fields.Integer(string="Absent")
	emp_on_sick_leave = fields.Integer(string="Absent")
	emp_on_annual_leave = fields.Integer(string="Absent")
	emp_on_special_leave = fields.Integer(string="Absent")
	emp_on_marriage_leaves = fields.Integer(string="Absent")
	emp_on_work_from_home = fields.Integer(string="Absent")
	emp_on_half_leave = fields.Integer(string="Absent")
	shift_change_requests = fields.Integer(string="Shift Change Request")
	requests_of_casual_leave = fields.Integer(string="Absent")
	requests_of_short_leave = fields.Integer(string="Absent")
	requests_of_sick_leave = fields.Integer(string="Absent")
	requests_of_annual_leave = fields.Integer(string="Absent")
	requests_of_special_leave = fields.Integer(string="Absent")
	requests_of_marriage_leaves = fields.Integer(string="Absent")
	requests_of_work_from_home = fields.Integer(string="Absent")
	
	typee = fields.Selection([
		('admin', 'Admin'),
		# ('finance', 'Finance'),
		# ('ceo', 'CEO'),
		# ('basic', 'Basic'),
		('manager_saad1', 'Manager Saad 1'),
		('manager_saad2', 'Manager Saad 2'),
		],string="View For",default='basic')
	user_id = fields.Many2one('res.users', string="User")
	# expenses = fields.Integer(compute='calc_expenses')


# Function For Getting Present Employees

	@api.one
	def calc_present_emp_saad1(self,attr):
		# if self.typee == 'admin':
		""" Only for saad 1 Entity """
		# status = " "
		status = attr
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'

		print "----------------"
		print self.emp_present
		print dated
		print "----------------"

		if attr:

			actual = self.env['actual.attendence'].search_count([('date','=',dated),('todaystatus','=',status),('branch','=',1)])

			if attr == 'present':
				self.emp_present = actual

			if attr == 'absent':
				self.emp_absent = actual

			# if attr == 'casual_leaves':
			#   self.emp_on_casual_leave = actual

			# if attr == 'short_leave':
			#   self.emp_on_short_leave = actual
			
			# if attr == 'sick_leaves':
			#   self.emp_on_sick_leave = actual

			# if attr == 'annual_leaves':
			#   self.emp_on_annual_leave = actual

			# if attr == 'leave':
			#   self.emp_on_leave = actual

			# if attr == 'special_leave':
			#   self.emp_on_special_leave = actual

			# if attr == 'marriage_leaves':
			#   self.emp_on_marriage_leaves = actual

			# if attr == 'work_from_home':
			#   self.emp_on_work_from_home = actual

			# if attr == 'half_leave':
			#   self.emp_on_half_leave = actual
		else:
			self.emp_present = 0
			self.emp_absent = 0
			self.emp_on_casual_leave = 0
			self.emp_on_short_leave = 0
			self.emp_on_half_leave = 0
			self.emp_on_sick_leave = 0
			self.emp_on_annual_leave = 0
			self.emp_on_leave = 0
			self.emp_on_special_leave = 0
			self.emp_on_marriage_leaves = 0
			self.emp_on_work_from_home = 0

	@api.one
	def calc_present_emp_saad2(self,attr):
		# if self.typee == 'admin' or self.typee == 'manager':
		# status = " "
		status = attr
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'

		print "----------------"
		print self.emp_present
		print dated
		print "----------------"

		if attr:

			actual = self.env['actual.attendence'].search_count([('date','=',dated),('todaystatus','=',status),('branch','=',2)])

			if attr == 'present':
				self.emp_present_2 = actual

			if attr == 'absent':
				self.emp_absent_2 = actual


		else:
			self.emp_present = 0
			self.emp_absent = 0
			self.emp_on_casual_leave = 0
			self.emp_on_short_leave = 0
			self.emp_on_half_leave = 0
			self.emp_on_sick_leave = 0
			self.emp_on_annual_leave = 0
			self.emp_on_leave = 0
			self.emp_on_special_leave = 0
			self.emp_on_marriage_leaves = 0
			self.emp_on_work_from_home = 0

		#################################################
		###############MANAGER WISE DATA#################
		#################################################

		# if self.typee == 'manager':
		#   # status = " "
		#   status = attr
		#   dated = datetime.today().strftime('%Y-%m-%d')
		#   # dated = '2021-09-01'

		#   print "----------------"
		#   print self.emp_present
		#   print dated
		#   print "----------------"

		#   if attr:

		#       actual = self.env['actual.attendence'].search_count([('date','=',dated),('todaystatus','=',status),('employee_id.parent_id.user_id','=',self.user_id.id)])

		#       if attr == 'present':
		#           self.emp_present = actual

		#       if attr == 'absent':
		#           self.emp_absent = actual

		#       if attr == 'casual_leaves':
		#           self.emp_on_casual_leave = actual

		#       if attr == 'short_leave':
		#           self.emp_on_short_leave = actual
				
		#       if attr == 'sick_leaves':
		#           self.emp_on_sick_leave = actual

		#       if attr == 'annual_leaves':
		#           self.emp_on_annual_leave = actual

		#       if attr == 'leave':
		#           self.emp_on_leave = actual

		#       if attr == 'special_leave':
		#           self.emp_on_special_leave = actual

		#       if attr == 'marriage_leaves':
		#           self.emp_on_marriage_leaves = actual

		#       if attr == 'work_from_home':
		#           self.emp_on_work_from_home = actual

		#       if attr == 'half_leave':
		#           self.emp_on_half_leave = actual
		#   else:
		#       self.emp_present = 0
		#       self.emp_absent = 0
		#       self.emp_on_casual_leave = 0
		#       self.emp_on_short_leave = 0
		#       self.emp_on_half_leave = 0
		#       self.emp_on_sick_leave = 0
		#       self.emp_on_annual_leave = 0
		#       self.emp_on_leave = 0
		#       self.emp_on_special_leave = 0
		#       self.emp_on_marriage_leaves = 0
		#       self.emp_on_work_from_home = 0

		# if self.typee == 'finance':
		#   po = self.env['purchase.order'].search_count([('state','=','finance_approved')])
		#   self.purchase_order = po
		#   print "----------------- Finance-------------------"
		#   print po
		# if self.typee == 'ceo':
		#   po = self.env['purchase.order'].search_count([('state','=','final_approved')])
		#   self.purchase_order = po
		#   print "----------------- CEO-------------------"
		#   print po
		# if self.typee == 'basic':
		#   po = self.env['purchase.order'].search_count([('state','=','draft')])
		#   self.purchase_order = po
		#   print "----------------- Basic-------------------"
		#   print po

# Function For Getting Present Employees Ends Here


# Function For Getting Change Shifts Requests


	@api.one
	def calc_shift_change_request(self):
		if self.typee == 'admin':

			requests = self.env['shift.change'].search_count([('state','=','draft')])

			self.shift_change_requests = requests

		if self.typee == 'manager':

			requests = self.env['shift.change'].search_count([('state','=','draft'),('employee_id.parent_id.user_id','=',self.user_id.id)])

			self.shift_change_requests = requests

# Function For Getting Change Shifts Requests Ends Here


# Function For Getting Annual Leave Requests


	@api.one
	def calc_leave_requests(self,attr):
		if self.typee == 'admin':
			if attr:

				requests = self.env['hr.holidays'].search_count([('state','=','confirm'),('holiday_status_id.name','=',attr)])

				if attr == 'Sick Leaves':
					self.requests_of_sick_leave = requests

				if attr == 'Annual Leaves':
					self.requests_of_annual_leave = requests

				if attr == 'Short leave':
					self.requests_of_short_leave = requests

				if attr == 'Work From Home':
					self.requests_of_work_from_home = requests

				if attr == 'Special leave':
					self.requests_of_special_leave = requests

				if attr == 'Marriage Leaves':
					self.requests_of_marriage_leaves = requests

				if attr == 'Casual Leaves':
					self.requests_of_casual_leave = requests
			else:
				self.requests_of_sick_leave = 0
				self.requests_of_annual_leave = 0
				self.requests_of_short_leave = 0
				self.requests_of_work_from_home = 0
				self.requests_of_special_leave = 0
				self.requests_of_marriage_leaves = 0
				self.requests_of_casual_leave = 0


		if self.typee == 'manager':
			if attr:

				requests = self.env['hr.holidays'].search_count([('state','=','confirm'),('holiday_status_id.name','=',attr),('employee_manager.user_id','=',self.user_id.id)])

				if attr == 'Sick Leaves':
					self.requests_of_sick_leave = requests

				if attr == 'Annual Leaves':
					self.requests_of_annual_leave = requests

				if attr == 'Short leave':
					self.requests_of_short_leave = requests

				if attr == 'Work From Home':
					self.requests_of_work_from_home = requests

				if attr == 'Special leave':
					self.requests_of_special_leave = requests

				if attr == 'Marriage Leaves':
					self.requests_of_marriage_leaves = requests

				if attr == 'Casual Leaves':
					self.requests_of_casual_leave = requests
			else:
				self.requests_of_sick_leave = 0
				self.requests_of_annual_leave = 0
				self.requests_of_short_leave = 0
				self.requests_of_work_from_home = 0
				self.requests_of_special_leave = 0
				self.requests_of_marriage_leaves = 0
				self.requests_of_casual_leave = 0

# Function For Getting Annual Leave Requests Ends Here





# Button for Opening Present Employees


	@api.multi
	def button_emp_present(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		# if self.typee == 'admin':
		return {
			'type': 'ir.actions.act_window',
			'name': 'Present Employee',
			'res_model': 'actual.attendence',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'domain': [('todaystatus', '=','present'),('date','=',dated)]
		}
		# if self.typee == 'manager':
		#   return {
		#       'type': 'ir.actions.act_window',
		#       'name': 'Present Employee',
		#       'res_model': 'actual.attendence',
		#       'view_type': 'form',
		#       'view_mode': 'tree,form',
		#       'domain': [('todaystatus', '=','present'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
		#   }


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_absent(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		# if self.typee == 'admin':
		return {
			'type': 'ir.actions.act_window',
			'name': 'Absent Employee',
			'res_model': 'actual.attendence',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'domain': [('todaystatus', '=','absent'),('date','=',dated)]
		}
		# if self.typee == 'manager':
		#   return {
		#       'type': 'ir.actions.act_window',
		#       'name': 'Absent Employee',
		#       'res_model': 'actual.attendence',
		#       'view_type': 'form',
		#       'view_mode': 'tree,form',
		#       'domain': [('todaystatus', '=','absent'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
		#   }


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		# if self.typee == 'admin':
		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee on Leave',
			'res_model': 'actual.attendence',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'domain': [('todaystatus', '=','leave'),('date','=',dated)]
		}
		# if self.typee == 'manager':
		#   return {
		#       'type': 'ir.actions.act_window',
		#       'name': 'Employee on Leave',
		#       'res_model': 'actual.attendence',
		#       'view_type': 'form',
		#       'view_mode': 'tree,form',
		#       'domain': [('todaystatus', '=','leave'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
		#   }


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_casual_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		# if self.typee == 'admin':
		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee on Casual Leave',
			'res_model': 'actual.attendence',
			'view_type': 'form',
			'view_mode': 'tree,form',
			'domain': [('todaystatus', '=','casual_leaves'),('date','=',dated)]
		}
		# if self.typee == 'manager':
		#   return {
		#       'type': 'ir.actions.act_window',
		#       'name': 'Employee on Casual Leave',
		#       'res_model': 'actual.attendence',
		#       'view_type': 'form',
		#       'view_mode': 'tree,form',
		#       'domain': [('todaystatus', '=','casual_leaves'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
		#   }


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_short_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Short Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','short_leave'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Short Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','short_leave'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_sick_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Sick Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','sick_leaves'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Sick Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','sick_leaves'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_annual_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Annual Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','annual_leaves'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Annual Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','annual_leaves'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_special_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Special leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','special_leave'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Special Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','special_leave'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_marriage_leaves(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Marriage Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','marriage_leaves'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Marriage Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','marriage_leaves'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_work_from_home(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Work from Home Employee',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','work_from_home'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Work from Home Employee',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','work_from_home'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..


# Button for Opening Present Employees


	@api.multi
	def button_emp_on_half_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Half Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','half_leave'),('date','=',dated)]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Employee on Half Leave',
				'res_model': 'actual.attendence',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('todaystatus', '=','half_leave'),('date','=',dated),('employee_id.parent_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..

# ======================================================
# ======================================================
# ======================================================


# Button for Opening Present Employees


	@api.multi
	def button_shift_change_requests(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Shift Change Request',
				'res_model': 'shift.change',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('state','=','draft')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Shift Change Request',
				'res_model': 'shift.change',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('state','=','draft'),('manager_id.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_sick_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Sick Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Sick Leaves'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Sick Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Sick Leaves'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_annual_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Annual Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Annual Leaves'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Annual Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Annual Leaves'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_short_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Short Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Short leave'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Short Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Short leave'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_work_from_home(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Work From Home Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Work From Home'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Work From Home Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Work From Home'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_special_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Special Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Special leave'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Special Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Special leave'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_marriage_leaves(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Marriage Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Marriage Leave'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Marriage Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Marriage Leave'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..




# Button for Opening Present Employees


	@api.multi
	def button_requests_of_casual_leave(self):
		dated = datetime.today().strftime('%Y-%m-%d')
		# dated = '2021-09-01'
		if self.typee == 'admin':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Casual Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Casual Leaves'),('state','=','confirm')]
			}
		if self.typee == 'manager':
			return {
				'type': 'ir.actions.act_window',
				'name': 'Casual Leaves Request',
				'res_model': 'hr.holidays',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'domain': [('holiday_status_id.name', '=','Casual Leaves'),('state','=','confirm'),('employee_manager.user_id','=',self.user_id.id)]
			}


# Button for Opening Present Employees Ends Here..

# =================================================================
# =================================================================
# =================================================================


	@api.multi
	def button_daily_attendance(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Daily Attendance Report',
			'res_model': 'dailyreport.attend',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
			'context': {'default_readonly_by_pass':True},
		}

	@api.multi
	def button_employee_wise_attendance(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Attendence Report',
			'res_model': 'actual.attend.wizard',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}


	@api.multi
	def button_employee_tardiness(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee Tardiness Report',
			'res_model': 'employee.tardiness.report',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}


	@api.multi
	def button_employee_absent_report(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee Absent Report',
			'res_model': 'employee.absent.report',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}


	@api.multi
	def button_employee_late_Arrival_report(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Late Arrival Report',
			'res_model': 'earlylate.attend',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}



	@api.multi
	def button_employee_annual_leaves_report(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee Annual Leaves Report',
			'res_model': 'employee.annual.leaves.report',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}



	@api.multi
	def button_employee_leave_incentive_report(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee Leaves Incentive Report',
			'res_model': 'employee.leave.incentive.report',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}



	@api.multi
	def button_employee_gratuity_report(self):

		return {
			'type': 'ir.actions.act_window',
			'name': 'Employee Gratuity Report',
			'res_model': 'employee.gratuity.report',
			'view_type': 'form',
			'view_mode': 'form',
			'target':'new',
			'domain': [],
		}
