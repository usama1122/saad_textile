{
	'name': 'HR Dashboard', 
	'description': 'HR Dashboard', 
	'author': 'Siddiq Chauhdry', 
	'depends': ['account'], 
	'application': True,
	'data': [
      	"views/assets.xml",
		'views/template.xml'
	],
}