#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###################################################
from openerp import models, fields, api
from datetime import date
from datetime import date, timedelta
import datetime

class SampleDevelopmentReport(models.AbstractModel):
	_name = 'report.attendance_late_early.customer_report'

	@api.model
	def render_html(self,docids, data=None):
		report_obj = self.env['report']
		report = report_obj._get_report_from_name('attendance_late_early.customer_report')
		active_wizard = self.env['earlylate.attend'].search([])
		emp_list = []
		for x in active_wizard:
			emp_list.append(x.id)
		emp_list = emp_list
		emp_list_max = max(emp_list) 

		record_wizard = self.env['earlylate.attend'].search([('id','=',emp_list_max)])

		record_wizard_del = self.env['earlylate.attend'].search([('id','!=',emp_list_max)])
		record_wizard_del.unlink()


		form = record_wizard.form
		to = record_wizard.to
		typed = record_wizard.typed
		employee = record_wizard.employee
		department = record_wizard.department
		time = record_wizard.time


		d1 = datetime.datetime.strptime(record_wizard.form, "%Y-%m-%d")
		d2 = datetime.datetime.strptime(record_wizard.to, "%Y-%m-%d")

		delta = d2 - d1
		dates = []
		for i in range(delta.days + 1):
			dates.append((d1 + timedelta(days=i)).strftime('%Y-%m-%d'))


		def get_total_late_time(actual_attendance):
			latetime = 0
			if actual_attendance:
				# print actual_attendance
				shift = ""
				check_in = 0.0
				check_out = 0.0
				check_in = actual_attendance.checkin
				check_out = actual_attendance.checkout
				
				""" shift seprate by - e:g 09:00 - 18:00 """
				shift = actual_attendance.shift
				shift_div = shift.split('-')
				
				""" Remove spaces in string """
				shift1 =shift_div[0]
				shift2 =  shift_div[1].lstrip()
				
				""" convet : to float . """
				line_list = shift1.split(':')
				shift_float = '.'.join(line_list)
				
				""" convet : to float . """
				line_list1 = shift2.split(':')
				shift_float1 = '.'.join(line_list1)

				if not check_in == 0:

					if check_in and shift_float:
						latetime = float(check_in) - float(shift_float)

			return latetime


		def get_total_early_time(actual_attendance):
			earlytime = 0
			if actual_attendance:
				# print actual_attendance
				shift = ""
				check_in = 0.0
				check_out = 0.0
				check_in = actual_attendance.checkin
				check_out = actual_attendance.checkout
				
				""" shift seprate by - e:g 09:00 - 18:00 """
				shift = actual_attendance.shift
				shift_div = shift.split('-')
				
				""" Remove spaces in string """
				shift1 =shift_div[0]
				shift2 =  shift_div[1].lstrip()
				
				""" convet : to float . """
				line_list = shift1.split(':')
				shift_float = '.'.join(line_list)
				
				""" convet : to float . """
				line_list1 = shift2.split(':')
				shift_float1 = '.'.join(line_list1)

				if not check_out == 0:

					if check_out and shift_float1:
						earlytime = float(shift_float1) - float(check_out)

			print earlytime
			return earlytime


		if typed == 'all' and time == "lin":
			records = []
			def get_record(attr):
				del records [:]
				rec = self.env['actual.attendence'].search([('intime','=','lin'),('date','=',attr)])
				for x in rec:
					records.append(x)



		if typed == 'all' and time == "eout":
			records = []
			def get_record(attr):
				del records [:]
				rec = self.env['actual.attendence'].search([('outtime','=','eout'),('date','=',attr)])
				for x in rec:
					records.append(x)



		if typed == 'all' and time == "both":
			records = []
			def get_record(attr):
				del records [:]
				rec = self.env['actual.attendence'].search([('date','=',attr)])
				for x in rec:
					if x.intime == 'lin' or x.outtime == 'eout':
						records.append(x)



		if typed == 'department' and time == "lin":
			records = []
			def get_record(attr):
				del records [:]
				for y in department:
					rec = self.env['actual.attendence'].search([('intime','=','lin'),('date','=',attr),('employee_id.department_id.id','=',y.id)])
					for x in rec:
						records.append(x)




		if typed == 'department' and time == "eout":
			records = []
			def get_record(attr):
				del records [:]
				for y in department:
					rec = self.env['actual.attendence'].search([('outtime','=','eout'),('date','=',attr),('employee_id.department_id.id','=',y.id)])
					for x in rec:
						records.append(x)



		if typed == 'department' and time == "both":
			records = []
			def get_record(attr):
				del records [:]
				for y in department:
					rec = self.env['actual.attendence'].search([('date','=',attr),('employee_id.department_id.id','=',y.id)])
					for x in rec:
						if x.intime == 'lin' or x.outtime == 'eout':
							records.append(x)



		if typed == 'specific' and time == "lin":
			records = []
			def get_record(attr):
				del records [:]
				employees = []
				for x in record_wizard.employee:
					employees.append(x)
				for x in employees:
					rec = self.env['actual.attendence'].search([('intime','=','lin'),('date','=',attr),('employee_id.id','=',x.id)])
					for z in rec:
						records.append(z)



		if typed == 'specific' and time == "eout":
			records = []
			def get_record(attr):
				del records [:]
				employees = []
				for x in record_wizard.employee:
					employees.append(x)
				for x in employees:
					rec = self.env['actual.attendence'].search([('outtime','=','eout'),('date','=',attr),('employee_id.id','=',x.id)])
					for z in rec:
						records.append(z)



		if typed == 'specific' and time == "both":
			records = []
			def get_record(attr):
				del records [:]
				employees = []
				for x in record_wizard.employee:
					employees.append(x)
				for x in employees:
					rec = self.env['actual.attendence'].search([('date','=',attr),('employee_id.id','=',x.id)])
					for z in rec:
						if z.intime == 'lin' or z.outtime == 'eout':
							records.append(z)



		def get_name():
			name = " "
			if time == "lin":
				name = "Late Arrival"
			if time == "eout":
				name = "Early Departure"
			if time == "both":
				name = "Late Arrival - Early Departure"

			return name


		def get_check(idz,attr):
			
			attendence = self.env['actual.attendence'].search([('id','=',idz)])
			if attr == 'entry':
				if attendence.checkin:
					return '{0:02.0f}:{1:02.0f}'.format(*divmod(attendence.checkin * 60, 60))
				else: 
					return "-"
			
			if attr == 'exit':
				if attendence.checkout:
					return '{0:02.0f}:{1:02.0f}'.format(*divmod(attendence.checkout * 60, 60))
				else: 
					return "-"



		

		docargs = {
		
			'doc_ids': docids,
			'doc_model': 'actual.attendence',
			'form': form,
			'to': to,
			'dates': dates,
			'department': department,
			'employee': employee,
			'records': records,
			'get_record': get_record,
			'get_name': get_name,
			'get_check': get_check,
			'get_total_late_time': get_total_late_time,
			'get_total_early_time': get_total_early_time,
			
	
			}

		return report_obj.render('attendance_late_early.customer_report', docargs)