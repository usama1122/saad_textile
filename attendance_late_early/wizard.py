#-*- coding:utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api
from datetime import date


class AttendLateEarly(models.Model):
	_name = "earlylate.attend"

	employee = fields.Many2many('hr.employee',string="Employee")
	department = fields.Many2many('hr.department',string="Department")
	form = fields.Date(string="From",default=date.today())
	to = fields.Date(string="To" ,default=date.today())
	typed = fields.Selection([
		('all','All'),
		('department','Department Wise'),
		('specific','Specific'),
		],string='Employee(s)',default='all')
	
	time = fields.Selection([
		('lin','Late Arrival'),
		('eout','Early Departure'),
		('both','Both'),
		],string='Time' , default='lin' , readonly=True)
	







	
