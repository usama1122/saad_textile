# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import Warning
from openerp.exceptions import ValidationError
from openerp.exceptions import Warning, ValidationError, UserError
import datetime
from datetime import date, datetime, timedelta
import time
import calendar
from datetime import time
from dateutil.relativedelta import relativedelta
import datetime as dt
import pytz
import pandas as pd

class ActualAttendence(models.Model):
    _name = 'actual.attendence'
    _rec_name = 'employee_rec_name'
    
    employee_id = fields.Many2one('hr.employee', string="Employee", required="True")
    department = fields.Many2one('hr.department', string="Department")
    shift_link = fields.Many2one('shifts.attendence', string="shift link")
    leave_id = fields.Integer(string="Leave ID")
    leave_id_bool = fields.Boolean(string="Leave ID Bool")
    card_no = fields.Char(string='Card No')
    employee_rec_name = fields.Char()
    shift_work_hour = fields.Float(String="Shift Hour")
    
    checkin = fields.Float(string="Check In")
    checkout = fields.Float(string="Check Out")
    total_time = fields.Float(string="Total Time")
    total_late_time = fields.Float(string="Total Late Time")
    star_hours = fields.Float(string="Star Overtime")
    end_hours = fields.Float(string="End Overtime")
    start_shift = fields.Float(string="Start Shift Time")
    end_shift = fields.Float(string="End Shift Time")
    standard_working_hour = fields.Float(string="Standard Working Hours")
    checkin_dt = fields.Datetime(string="Checkin DT")
    checkout_dt = fields.Datetime(string="Checkout DT")
    start_date = fields.Datetime(string="Shift Start Date")
    end_date = fields.Datetime(string="Shift End Date")
    effective_start_date = fields.Datetime(string="Effective Start Date")
    effective_end_date = fields.Datetime(string="Effective End Date")
    end_date_overtime = fields.Datetime(string="End Date Overtime")
    total_hours = fields.Float(string="Total Overtime")
    overtime = fields.Boolean(string="Overtime")
    shift = fields.Char(string="Effective Shift for the day")
    check_out_lavi_time = fields.Char(String="Check Out Lavi Time")    
    intime = fields.Selection([
        ('lin', 'Late In'),
        # ('ein', 'Early In'),
        ('-', ' - '),
        ],default='-', string="In Time")
    
    outtime = fields.Selection([
        ('lout', 'Late Out'),
        ('eout', 'Early Out'),
        ('-', ' - '),
        ],default='-', string="Out Time")
    
    todaystatus = fields.Selection([
        ('present', 'Present'),
        ('absent', 'Absent'),
        ('half_leave', 'Half Leave'),
        ('leave', 'Leave'),
        ('holiday', 'Holiday'),
        ('sick_leaves', 'Sick Leaves'),
        ('annual_leaves', 'Annual Leaves'),
        ('special_leave', 'Special leave'),
        ('casual_leaves', 'Casual Leaves'),
        ('marriage_leaves', 'Marriage Leaves'),
        ('short_leave', 'Short leave'),
        ('work_from_home', 'Work From Home'),
        ('gazetted_holiday', 'Gazetted Holiday'),
        ], string="Status Today")
    
    defaultstatus = fields.Selection([
        ('holiday', 'Holiday'),
        ('gazetted_holiday', 'Gazetted Holiday'),
        ], string="Default Status")

    non_validate = fields.Boolean(string="Non Validated")
    date = fields.Date(string="Date", required="True")
    raw_tree = fields.One2many('ecube.raw.attendance', 'tree_link')
    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

    


    def update_attendance_leaves_status(self):
        pending_leaves =  self.env['hr.holidays'].search([('state','=','validate'),('actual_attendence_status','=',False),('employee_id','=',self.employee_id.id)])
        if pending_leaves:
            for leaves in pending_leaves:
                leaves.attendence_status_update_future_attendnace()




        
        

    @api.multi
    def get_all(self):
        self.env['actual.attendence.wizard'].attend_get()
        # self.env['actual.attendence.wizard'].LinkingRawAttendence()
        # raw = self.env['ecube.raw.attendance'].search([])
        # count = 0
        # for x in raw:
        #     count = count + 1
        #     print x.id
        #     if x.attendance_date:
        #         DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        #         ecube_raw_date = datetime.strptime(str(x.attendance_date), DATETIME_FORMAT)
        #         raw_date = ecube_raw_date - relativedelta(hours=5)
        #         x.ecube_date = raw_date
            
    
    @api.multi
    def all_pre_actual_attendance_update(self):
        actual_attend = self.env['actual.attendence'].search([])
        print len(actual_attend)
        print "xxxxxxxxxxxxx"
        for attend in actual_attend:
            print attend.id
            print attend.date
            attend._get_rec_name()

    def is_shift_change(self):
        check_is_shift_change = self.env['shift.change'].search([('employee_id','=',self.employee_id.id),('from_date','<=',self.date),('to_date','>=',self.date),('state','=','validate')])
        if check_is_shift_change:
            self.shift = check_is_shift_change.requested_shift_id.name
            self.shift_link = check_is_shift_change.requested_shift_id.id

    
    @api.multi
    def set_shift(self):
        self.is_shift_change()
        self.branch = self.employee_id.branch.id
        if not self.shift_link:
            self.shift_link = self.employee_id.schedule.id
        self.shift_work_hour = self.employee_id.schedule.shift_work_hour
        if not self.shift:
            self.shift = self.employee_id.schedule.name
        
        self.effective_start_date = False
        self.effective_end_date = False
        
        if self.shift:
            shift_start_date = str(self.date)+' '+self.shift[:5]
            shift_end_date = str(self.date)+' '+self.shift[-5:]
            date_time_start = dt.datetime.strptime(shift_start_date, '%Y-%m-%d %H:%M')
            date_time_end = dt.datetime.strptime(shift_end_date, '%Y-%m-%d %H:%M')
            date = dt.datetime.strptime(str(self.date), '%Y-%m-%d')
            self.start_date = date_time_start - relativedelta(hours=5)
            self.end_date = date_time_end - relativedelta(hours=5)
            self.end_date_overtime = date_time_end - relativedelta(hours=5)
            if self.shift_link.overlap_shift == False:
                self.effective_start_date = date - relativedelta(hours=5)
                self.effective_end_date = date - relativedelta(hours=5,minutes=1) + relativedelta(days=1)
            
            """ Means we increase 8 house check out time (5 hour deafult server time and 3 hour manu add mentioned below.) """
            # self.effective_end_date = date_time_end - relativedelta(hours=2)
            start_time_var =  date_time_start.time()
            end_time_var =  date_time_end.time()
            
            start_time_float = start_time_var.hour+start_time_var.minute/60.0
            end_time_float = end_time_var.hour+end_time_var.minute/60.0
            self.start_shift = start_time_float
            self.end_shift = end_time_float
            if self.shift_link.overlap_shift == True:
                self.effective_start_date = date_time_start - relativedelta(hours=6.5)
                self.end_date = (date_time_end - relativedelta(hours=5)) + relativedelta(days=1)
                self.end_date_overtime = (date_time_end - relativedelta(hours=5)) + relativedelta(days=1)
                """ Means we increase 8 house check out time (5 hour deafult server time and 3 hour manu add mentioned below.) """
                self.effective_end_date = (date_time_end - relativedelta(hours=2)) + relativedelta(days=1)
                start_shift = 24 - self.start_shift


        self.set_overtime()
        self._get_rec_name()
        self._get_depart()

    def set_overtime(self):
        if self.shift:

            attendance_date = []
            date_time_list = []
            self.checkin = 0
            self.checkout = 0
            self.total_time = 0
            
            if self.raw_tree:
                for x in self.raw_tree:
                    if not x.ecube_date: 
                        if x.attendance_date:
                            ecube_raw_date = datetime.strptime(str(x.attendance_date), DATETIME_FORMAT)
                            raw_date = ecube_raw_date - relativedelta(hours=5)
                            x.ecube_date = raw_date
                    if x.ecube_date:
                        if x.ecube_date[:16] not in date_time_list:
                            date_time_list.append(x.ecube_date[:16])
                            attendance_date.append(x)

                attendance_date = sorted(attendance_date, key=lambda x: x.ecube_date)
                if attendance_date:
                    checkin_dt = attendance_date[0]
                    self.checkin_dt = checkin_dt.ecube_date
                    if self.checkin_dt:
                        self.todaystatus = "present"
                    date_time_start = dt.datetime.strptime(self.checkin_dt, '%Y-%m-%d %H:%M:%S')
                    date_time_start_var = date_time_start + relativedelta(hours=5)
                    start_time_var =  date_time_start_var.time()
                    start_time_float = start_time_var.hour+start_time_var.minute/60.0
                    self.checkin = start_time_float
                    

                    s_date_1 = dt.datetime.strptime(self.start_date, '%Y-%m-%d %H:%M:%S')
                    start_date = (s_date_1  + relativedelta(hours=5)) + relativedelta(minutes=15)
                    start_date_actual = (s_date_1  + relativedelta(hours=5))
                    
                    s_date = (s_date_1  - relativedelta(hours=5)) - relativedelta(minutes=15)
                    s_date_late_in = (s_date_1  + relativedelta(hours=5)) - relativedelta(minutes=15)
                   

                    checkin_dt_1 = dt.datetime.strptime(self.checkin_dt, '%Y-%m-%d %H:%M:%S')
                    checkin_dt = checkin_dt_1  +  relativedelta(hours=5)
                    


                    self.total_late_time = 0
                    if checkin_dt > start_date:
                        self.intime = 'lin'
                        difference = relativedelta(start_date_actual, checkin_dt)
                        hours = difference.hours
                        minutes = difference.minutes
                        total_minutes = ((hours * 60) + minutes)
                        total_hours =  float(total_minutes) / 60.00
                        self.total_late_time =  abs(total_hours)
                    else:
                        self.intime = '-'

                    
                    self.checkout = 0.0
                    self.total_hours = 0.0
                    self.total_time = 0.0
                    self.outtime = '-'
                    if len(attendance_date) > 1:
                        checkout_dt =  attendance_date[-1]
                        self.checkout_dt =  checkout_dt.ecube_date
                        date_time_end = dt.datetime.strptime(self.checkout_dt, '%Y-%m-%d %H:%M:%S') 
                        date_time_end_var = date_time_end + relativedelta(hours=5)
                        end_time_var =  date_time_end_var.time()
                        end_time_float = end_time_var.hour+end_time_var.minute/60.0
                        self.checkout =  end_time_float
                        e_date_1 = dt.datetime.strptime(self.end_date_overtime, '%Y-%m-%d %H:%M:%S')
                        e_date = (e_date_1  - relativedelta(hours=5)) - relativedelta(minutes=15)
                        checkout_dt_1 = dt.datetime.strptime(self.checkout_dt, '%Y-%m-%d %H:%M:%S')
                        checkout_dt = checkout_dt_1  - relativedelta(hours=5)
                        
                        if e_date > checkout_dt:
                            self.outtime = 'eout'
                        else:
                            self.outtime = '-'

                        if self.checkin_dt and self.checkout_dt:
                            time1 = datetime.strptime(str(self.checkin_dt), '%Y-%m-%d %H:%M:%S')
                            time2 = datetime.strptime(str(self.checkout_dt), '%Y-%m-%d %H:%M:%S')
                            if self.checkin > self.checkout:
                                difference = relativedelta(time2, time1)
                                hours = difference.hours
                                minutes = difference.minutes
                                total_minutes = ((hours * 60) + minutes)
                                total_hours =  float(total_minutes) / 60.00
                                self.total_time =  total_hours
                            if self.checkin < self.checkout:
                                difference = relativedelta(time1, time2)
                                hours = difference.hours
                                minutes = difference.minutes
                                total_minutes = ((hours * 60) + minutes)
                                total_hours =  float(total_minutes) / 60.00
                                self.total_time =  abs(total_hours)
                                self.total_time = self.checkout - self.checkin

            else:
                self.todaystatus = "absent"

            if self.todaystatus == "absent":

                self.update_attendance_leaves_status()
            leave = self.env['hr.holidays'].search([('employee_id','=',self.employee_id.id),('date_from_ecube','<=',self.date),('date_to_ecube','>=',self.date),('state','=','validate')])
            if leave:


                    self.todaystatus = 'leave'

                    if leave.holiday_status_id.name == 'Sick Leaves':
                        self.todaystatus = 'sick_leaves'
                    
                    if leave.holiday_status_id.name == 'Annual Leaves':
                        self.todaystatus = 'annual_leaves'
                    
                    if leave.holiday_status_id.name == 'Special leave':
                        self.todaystatus = 'special_leave'
                    
                    if leave.holiday_status_id.name == 'Casual Leaves':
                        self.todaystatus = 'casual_leaves'
                    
                    if leave.holiday_status_id.name == 'Marriage Leaves':
                        self.todaystatus = 'marriage_leaves'
                    
                    if leave.holiday_status_id.name == 'Work From Home':
                        self.todaystatus = 'work_from_home'
                    
                    if leave.holiday_status_id.name == 'Short leave':
                        self.todaystatus = 'short_leave'


            holiday = self.env['holidays.tree'].search([('date','=',self.date)])
            for h in holiday:
                if h.tree_link:
                    total_hours = self.total_time
                    if total_hours > 0:
                        self.total_hours = total_hours
                    if total_hours < 0:
                        self.total_hours = 0.0
                    if holiday.remarks == 'Sunday Holiday':
                        self.todaystatus = "holiday"
                        self.defaultstatus = 'holiday'
                    elif holiday.remarks == 'Saturday Holiday':
                        self.todaystatus = "holiday"
                        self.defaultstatus = 'holiday'
                    else:
                        self.todaystatus = "gazetted_holiday"
                        self.defaultstatus = 'gazetted_holiday'
                else:
                    if self.total_time:
                        total_hours = self.checkout - self.end_shift
                        if total_hours > 0:
                            self.total_hours = total_hours
                        if total_hours < 0:
                            self.total_hours = 0.0
    
    @api.onchange('employee_id','date')
    def _get_rec_name(self):
        try:
            if self.employee_id and self.date:
                self.employee_rec_name = str(self.employee_id.name) + " " + str(self.date)

        except Exception, e:
            print "Process terminate not connnect ......... : {}".format(e)

    
    @api.onchange('employee_id')
    def _get_depart(self):
        self.department = self.employee_id.department_id.id

    def create_leaves_from_actual_attendance_status_absent(self):
        today_date = datetime.today().strftime('%Y-%m-%d')
        today = datetime.strptime(today_date, "%Y-%m-%d")
        actual_attendance = self.env['actual.attendence'].search([('leave_id_bool','=',False),('date','<',today),('todaystatus','=','absent')])

        if actual_attendance:
            for attend in actual_attendance:
                leave = self.env['hr.holidays'].search([('employee_id','=',attend.employee_id.id),('date_from_ecube','<=',attend.date),('date_to_ecube','<=',attend.date),('state','=','validate')])
                if leave:
                    attend.todaystatus = 'leave'
                    attend.leave_id = leave.id
                    attend.leave_id_bool = True
                    leave.actual_attendence_id = attend.id
    
    
    def employee_terminate(self):
        date = datetime.now()
        last_day = date + relativedelta(day=1, months=+1, days=-1)
        first_day = date + relativedelta(day=1)

        datefrom = str(first_day)
        dateto = str(last_day)
        self.env.cr.execute("SELECT id , employee_id , date FROM actual_attendence WHERE date BETWEEN '"+datefrom+"'   AND '"+dateto+"'AND todaystatus = 'absent'" )

        actual_attendance = self.env.cr.dictfetchall()
        actual_attendance = pd.DataFrame(actual_attendance)
        actual_attendance['count'] = 1
        actual_attendance = actual_attendance.groupby(['employee_id'],as_index = False).sum()

        for index, row in actual_attendance.iterrows():
            if row['count'] > 9:
                """ if maximum 10 absent found in tha month """
                actual_attendnace_emp = self.env['actual.attendence'].search([('todaystatus','=','absent'),('date','>=',first_day),('date','<=',last_day),('employee_id','=',row['employee_id'])])


                count = 0
                """ check continuously 10 absent found are not """
                for attendance in actual_attendnace_emp:
                    if count == 0:
                        start_date = attendance.date
                    if start_date:
                        if count != 0:
                            start_date = datetime.strptime(start_date, "%Y-%m-%d")
                            start_date  = start_date + relativedelta(days=1)
                            start_date = str(start_date).split(" ")
                            start_date = start_date[0]
                    
                    if start_date == attendance.date:
                        count = count + 1
                    else:
                        count = 0

                    if count > 8:
                        """ count start from 0 Count > 8 means if employee consective 10 absent then employess go to the terminate. """
                        print "employee terminate"
                        self.env['employee.termination.letter'].create({
                            'date': date,
                            'employee_id': attendance.employee_id.id,
                            'employee_card': 1,
                            'dor': date,
                            'employee_name': attendance.employee_id.name,
                            })
                        break






    def set_pre_data(self):
        if self.overtime == False:
            self.end_date_overtime = self.end_date
        else:
            pass
        attendance_date = []
        if self.raw_tree:
            for x in self.raw_tree:
                
                DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
                if x.attendance_date:
                    ecube_raw_date = datetime.strptime(str(x.attendance_date), DATETIME_FORMAT)
                    raw_date = ecube_raw_date - relativedelta(hours=5)
                    x.ecube_date = raw_date
                attendance_date.append(x)

            attendance_date = sorted(attendance_date, key=lambda x: x.ecube_date)

            checkin_dt = attendance_date[0]
            if checkin_dt.ecube_date:
                self.checkin_dt = checkin_dt.ecube_date
            checkout_dt =  attendance_date[-1]
            if checkout_dt.ecube_date:
                self.checkout_dt =  checkout_dt.ecube_date

            if self.checkout_dt and self.end_date_overtime:
                if self.checkout_dt > self.end_date_overtime:
                    checkout_dt = fields.Datetime.from_string(self.checkout_dt)
                    end_date_overtime = fields.Datetime.from_string(self.end_date_overtime)
                    difference = relativedelta(checkout_dt, end_date_overtime)
                    days = difference.days
                    hours = difference.hours
                    minutes = difference.minutes
                    total_hour = float(minutes) / 60
                    self.total_hours = float(total_hour) + hours
                else:
                    self.total_hours = 0


    @api.multi
    def re_validate(self):

        employees = self.env['hr.employee'].search([])

        for employee in employees:

            attendence = self.env['ecube.raw.attendance'].search([('employee_id.id','=',employee.id),('date','=',self.date)])

            if attendence:

                schedule = employee.schedule
                lavi_time = float(1)

                now = datetime.strptime(self.date, "%Y-%m-%d")
                day = calendar.day_name[now.weekday()]
                in_time = 0
                out_time = 0

                if schedule:
                    in_time = float(schedule.intime)
                    out_time = float(schedule.outtime)

                    sced_in = in_time
                    sced_out = out_time

                    print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
                    if in_time < 1:
                        in_time = (12 + in_time) - lavi_time
                    else:
                        print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
                        in_time = in_time - lavi_time

                    out_time = out_time + lavi_time

                    total_required = out_time - in_time

                    print "AAAAAAAAAAAAAAAAAAAAAAAAAA"
                    if in_time != 0 or out_time != 0:

                        result = '{0:02.0f}:{1:02.0f}'.format(*divmod(in_time * 60, 60))
                        intime = datetime.strptime(result, "%H:%M")
                        intime_hrs = self.getHours(intime)

                        result = '{0:02.0f}:{1:02.0f}'.format(*divmod(out_time * 60, 60))
                        print result
                        print result
                        print "result"
                        print result
                        print result
                        outtime = datetime.strptime(result,  "%H:%M")
                        outtime_hrs = self.getHours(outtime)

                        times = []
                        for x in attendence:
                            times.append(x.attendance_date)

                        final_checkin = 0
                        final_checkout = 0
                        final_total = 0
                        send_vals_in = 0
                        send_vals_out = 0

                        booleans = False
                        newin =  0
                        for x in times:
                            if self.getHours(x) > intime_hrs and self.getHours(x) < outtime_hrs:
                                newin = self.timeToFloat(x)
                                final_checkin = newin
                                if (final_checkin + 0.25) < sced_in:
                                    send_vals_in = 1
                                elif final_checkin > (sced_in + 0.25):
                                    send_vals_in = 2
                                break

                        newout =  0
                        for x in reversed(times):
                            if self.getHours(x) > intime_hrs and self.getHours(x) < outtime_hrs:
                                newout = self.timeToFloat(x)
                                break

                        if newout != newin:
                            final_checkout = newout
                            if final_checkout != 0:
                                final_total = final_checkout - final_checkin
                            else:
                                booleans = True
                            if (final_checkout + 0.25) < sced_out:
                                send_vals_out = 1
                            elif final_checkout > (sced_out + 0.25):
                                send_vals_out = 2
                                
                        else:
                            booleans = True
                            final_checkout = 0
                            final_total = 0

                        self.checkin = final_checkin
                        self.checkout = final_checkout
                        self.total_time = final_total

    def getHours(self, date):
        if date:
            raw_date = str(date).split(" ")
            required_date = raw_date[1].split(":")
            new_date = required_date[0]+":"+required_date[1]
            return new_date

    def timeToFloat(self, date):
        if date:
            raw_date = datetime.strptime(date, "%Y-%m-%d %H:%M:%S ")
            hours_added_date = raw_date
            diff_in_time = hours_added_date - datetime.strptime(str(hours_added_date.date())+" "+"00:00:00 ",  "%Y-%m-%d %H:%M:%S ") 
            final_hrs = diff_in_time.seconds/3600.0
            return final_hrs

class ActualAttendenceTree(models.Model):
    _inherit = 'hr.employee'

    schedule = fields.Many2one('shifts.attendence',string="Shift",track_visibility='onchange')
class ActualAttendenceTree(models.Model):
    _inherit = 'ecube.raw.attendance'


    ecube_date = fields.Datetime(string="DateTime")
    tree_link = fields.Many2one('actual.attendence',string="Tree Link")

    @api.model
    def create(self, vals):
        new_record = super(ActualAttendenceTree, self).create(vals)
      
        # DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        # ecube_raw_date = datetime.strptime(str(new_record.attendance_date), DATETIME_FORMAT)
        # raw_date = ecube_raw_date - relativedelta(hours=5)
        # new_record.ecube_date = raw_date

        return new_record



class ActualAttendenceWizard(models.Model):
    _name = 'actual.attendence.wizard'

    date = fields.Date(string="Date")

    @api.multi
    def attend_get(self):
        dbName = self._cr.dbname
        if dbName == "saad_hr_db":
            print "startttttttt"

            # unmark = self.env['ecube.raw.attendance'].search([('attendence_done','=',True)])
            # for x in unmark:
            #   x.attendence_done = False

            # attend = self.env['actual.attendence'].search([])
            # attend.unlink()

            attend_dates = self.env['dates.attendence'].search([])

            lastdate = attend_dates[-1]
            lastdate = datetime.strptime(lastdate.date, "%Y-%m-%d")
            self.CreateDateWiseRecords(lastdate)
        # self.LinkingRawAttendence()
    


    def CreateDateWiseRecords(self,lastdate):

        today_date = datetime.today().strftime('%Y-%m-%d')
        today = datetime.strptime(today_date, "%Y-%m-%d")

        if lastdate < today:
            while (lastdate < today):
                lastdate = lastdate + timedelta(days=1)
                print lastdate
                print "lastdate lastdate lastdate lastdate "
                create_date = self.env['dates.attendence'].create({
                    'date': lastdate,
                })

                employees = self.env['hr.employee'].search([('active','=',True)])
                date = lastdate

                for employee in employees:
                    actual = self.env['actual.attendence'].search([('date','=',date),('employee_id','=',employee.id)])
                    if not actual:
                        create_date = actual.create({
                            'employee_id': employee.id,
                            'card_no': employee.emp_code,
                            'date': date,
                            'shift': employee.schedule.name,
                            'shift_link': employee.schedule.id,
                            'shift_work_hour': employee.schedule.shift_work_hour,
                        })

                        create_date.set_shift()
                        # create_date.update_attendance_leaves_status()

                    # schedule = self.GetSchedule(employee,date,'schedule')

                #     string = date.strftime('%Y-%m-%d')
                #     year = float(self.getyear(date))
                #     rec = self.env['holidays.attendence'].search([('year','=',year)])
                #     leave = self.env['hr.holidays'].search([('employee_id','=',employee.id),('date_from','<=',string),('date_to','>=',string)])
                    
                #     counter_strike = 0
                #     if rec:
                #         for x in rec.tree:
                #             if x.date == date:
                #                 status = 'holiday'
                #                 counter_strike = counter_strike + 1
                #                 self.MakeRecords(employee,date,schedule,status,final_checkout=False,attendence=False,final_checkin=False,final_total=False,booleans=False,send_vals_in=False,send_vals_out=False)
                #     if counter_strike == 0:
                #         if leave:
                #             status = 'leave'
                #             self.MakeRecords(employee,date,schedule,status,final_checkout=False,attendence=False,final_checkin=False,final_total=False,booleans=False,send_vals_in=False,send_vals_out=False)
                    
                #         else:
                #             status = 'absent'
                #             self.MakeRecords(employee,date,schedule,status,final_checkout=False,attendence=False,final_checkin=False,final_total=False,booleans=False,send_vals_in=False,send_vals_out=False)                   
    
    @api.model
    def LinkingRawAttendence(self):
        dbName = self._cr.dbname
        print "LinkingRawAttendence"
        print dbName
        if str(dbName) == "saad_hr_db":
            print "----------------"
            current_date = fields.date.today()
            date = (current_date - relativedelta(days=3))
            # all_unmark = self.env['ecube.raw.attendance'].search([('attendence_done','=',False),('employee_id','!=',False),('date','>',date)])
            all_unmark = self.env['ecube.raw.attendance'].search([('attendence_done','=',False),('employee_id','!=',False),('date','>','2021-03-31')])
            count = 0
            print len(all_unmark)
            count = 0
            for x in all_unmark:
                if x.ecube_date:
                    count = count + 1
                    print count
                    # DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
                    # ecube_raw_date = datetime.strptime(str(x.attendance_date), DATETIME_FORMAT)
                    # raw_date = ecube_raw_date - relativedelta(hours=5)
                    # x.ecube_date = raw_date
                    print x
                    recorded = self.env['actual.attendence'].search([('employee_id.id','=',x.employee_id.id),('effective_start_date','<=',x.ecube_date),('effective_end_date','>=',x.ecube_date)])
                    print recorded
                    if recorded:
                        x.attendence_done = True
                        rec = recorded[-1]
                        x.tree_link = rec.id
                        rec.set_shift()
                        print "attendance link successfully"
                    # recorded.set_shift()
                # count = count + 1
                # print count
                # x.tree_link = False

                # if x.attendence_done == False:

                #   date = x.date
                #   employee = x.employee_id

                #   if employee:

                #       attendence = self.env['ecube.raw.attendance'].search([('employee_id','=',employee.id),('date','=',date),('attendence_done','=',False)])

                #       for x in attendence:



                #       for attend in attendence:
                #           attend.attendence_done = True
                #           # print attend.attendence_done
                #           attend.tree_link = recorded.id

                        # if recorded:
                        #   self.AdjustCalculateTimings(recorded)

    def GetSchedule(self,employee,date,attr):

        changed = 0
        changed_shift = self.env['change.shift'].search([('form','<=',date),('to','>=',date)])
        
        if changed_shift:
            for x in changed_shift:
                for y in x.tree:
                    if y.employee_id.id == employee.id:
                        schedule = y.shift
                        changed = changed + 1
                        lavi_time = float(x.lavi)
        
        if changed == 0:
            schedule = employee.schedule
            lavi_time = schedule.lavi_time
            # lavi_time = float(5)

        if attr == 'schedule':
            return schedule

        if attr == 'lavi':
            return lavi_time

    def CalculateTimes(self,schedule,lavi,attr):

        in_time = 0
        out_time = 0

        in_time = float(schedule.intime)
        out_time = float(schedule.outtime)
        """ yasir : lavi time 5 mean han ka default time 18 han to ya usy increase kr ka 23 kr da ga  """
        lavi_time = lavi

        if in_time < 1:
            in_time = (12 + in_time) - lavi_time
        else:
            in_time = in_time - lavi_time

        out_time = out_time + lavi

        total_required = out_time - in_time

        if in_time != 0 or out_time != 0:

            result = '{0:02.0f}:{1:02.0f}'.format(*divmod(in_time * 60, 60))
            intime = datetime.strptime(result,"%H:%M")
            intime_hrs = self.getHours(intime)

            result = '{0:02.0f}:{1:02.0f}'.format(*divmod(out_time * 60, 60))
            outtime = datetime.strptime(result,"%H:%M")
            outtime_hrs = self.getHours(outtime)

        if attr == 'intime':
            return intime_hrs

        if attr == 'outtime':
            return outtime_hrs

    def CreateFinalValues(self,attendence,schedule,lavi_time,attr):

        unadjust = []
        if schedule:

            sced_in = schedule.intime 
            sced_out = schedule.outtime

            intime_hrs = self.CalculateTimes(schedule,lavi_time,'intime')
            outtime_hrs = self.CalculateTimes(schedule,lavi_time,'outtime')

            final_checkin = 0
            final_checkout = 0
            final_total = 0

            booleans = False
            newin =  0
            send_vals_in = 0
            send_vals_out = 0

            for x in attendence:
                
                # The time is in 24 hrs version. So this condition can only be true if intime and out time are in two different days.

                if intime_hrs > outtime_hrs:
                    outtimes = self.AddTimes(intime_hrs,outtime_hrs)
                else:
                    outtimes = outtime_hrs
                if self.getHours(x.attendance_date) > intime_hrs and self.getHours(x.attendance_date) < outtimes:
                    newin = self.timeToFloat(x.attendance_date)
                    # Convert the time in floting digits 
                    final_checkin = newin
                    # Adding 25 mins gap To calculate Early In and Early Out 
                    if (final_checkin + 0.25) < sced_in:
                        send_vals_in = 1
                    elif final_checkin > (sced_in + 0.25):
                        send_vals_in = 2
                    break

            newout =  0
            for x in reversed(attendence):
                if intime_hrs > outtime_hrs:
                    intime_hrs = "00:00"

                    if self.getHours(x.attendance_date) > intime_hrs and self.getHours(x.attendance_date) < outtime_hrs:
                        unadjust.append(x)

                else:
                    data  = self.getHours(x.attendance_date)

                    if self.getHours(x.attendance_date) > intime_hrs and self.getHours(x.attendance_date) < outtime_hrs:
                        newout = self.timeToFloat(x.attendance_date)
                        break

            if newout != newin:
                final_checkout = newout
                if final_checkout != 0:
                    final_total = final_checkout - final_checkin
                else:
                    # If the employee didn't checks out.
                    booleans = True

                if (final_checkout + 0.25) < sced_out:
                    send_vals_out = 1

                    # booleans = True
                elif final_checkout > (sced_out + 0.25):
                    abc = sced_out + 0.25

                    send_vals_out = 2
                    
            else:
                booleans = True
                final_checkout = 0
                final_total = 0

            if attr == 'checkin':
                return final_checkin

            if attr == 'checkout':
                return final_checkout

            if attr == 'total':
                return final_total

            if attr == 'boolean':
                return booleans

            if attr == 'vals_in':
                return send_vals_in

            if attr == 'vals_out':
                return send_vals_out

            if attr == 'unadjust':
                return unadjust

    def AdjustOlds(self,lists,date,record):
        date = date
        employee = record.employee_id
        schedule = self.GetSchedule(employee,date,'schedule')
        lavi_time = self.GetSchedule(employee,date,'lavi')
        attendence = lists
        outtime_hrs = self.CalculateTimes(schedule,lavi_time,'outtime')

        recorded = self.env['actual.attendence'].search([('employee_id.id','=',employee.id),('date','=',date)])

        newout =  0
        for x in attendence:
            if  self.getHours(x.attendance_date) < outtime_hrs:
                    newout = self.timeToFloat(x.attendance_date)

                    if recorded:
                        recorded.checkout = newout
                        x.tree_link = recorded.id
                    else:
                        status = 'present'
                        self.MakeRecords(employee,date,schedule,status,newout,attendence,final_checkin=False,final_total=False,booleans=False,send_vals_in=False,send_vals_out=False)
                    break

    def AdjustCalculateTimings(self,record):
        attendence = record.raw_tree
        date = record.date
        employee = record.employee_id

        print record
        print "............................"
        schedule = self.GetSchedule(employee,date,'schedule')
        lavi_time = self.GetSchedule(employee,date,'lavi')

        final_checkin = self.CreateFinalValues(attendence,schedule,lavi_time,"checkin")
        final_checkout = self.CreateFinalValues(attendence,schedule,lavi_time,"checkout")
        final_total = self.CreateFinalValues(attendence,schedule,lavi_time,"total")

        booleans = self.CreateFinalValues(attendence,schedule,lavi_time,"boolean")
        send_vals_in = self.CreateFinalValues(attendence,schedule,lavi_time,"vals_in")
        send_vals_out = self.CreateFinalValues(attendence,schedule,lavi_time,"vals_out")
        unadjust = self.CreateFinalValues(attendence,schedule,lavi_time,"unadjust")

        status = 'present'
        self.MakeRecords(employee,date,schedule,status,final_checkout,attendence,final_checkin,final_total,booleans,send_vals_in,send_vals_out)

        if unadjust:
            dated = datetime.strptime(date, "%Y-%m-%d")
            olddate = dated - timedelta(days=1)
            self.AdjustOlds(unadjust,olddate,record)

    def MakeRecords(self,employee,date_arg,schedule,status,final_checkout,attendence,final_checkin,final_total,booleans,send_vals_in,send_vals_out):
        
        recorded = self.env['actual.attendence'].search([('employee_id.id','=',employee.id),('date','=',date_arg)])
        if recorded:
            
            recorded.employee_id = employee.id
            recorded.department = employee.department_id.id
            recorded.checkin = final_checkin
            recorded.checkout = final_checkout
            recorded.total_time = final_total
            recorded.date = date_arg
            recorded.non_validate = booleans

            if schedule:
                recorded.shift = schedule.name
            recorded.todaystatus = status

            if send_vals_in != 0:
                if send_vals_in == 1:
                    recorded.intime = 'ein'
                if send_vals_in == 2:
                    recorded.intime = 'lin'

            if send_vals_out != 0:
                if send_vals_out == 1:
                    recorded.outtime = 'eout'
                if send_vals_out == 2:
                    recorded.outtime = 'lout'

            if attendence:
                for attend in attendence:
                    attend.tree_link = recorded.id
                    # attend.attendence_done = booleans

        else:
            create_order = self.env['actual.attendence'].create({
                'employee_id': employee.id,
                'card_no': employee.card_no.name,
                'department': employee.department_id.id,
                'checkin': final_checkin,
                'checkout': final_checkout,
                'total_time': final_total,
                'date': date_arg,
                'non_validate': booleans,
                'todaystatus': status,
            })
            if schedule:
                create_order.shift = schedule.name

            if send_vals_in != 0:
                if send_vals_in == 1:
                    create_order.intime = 'ein'
                if send_vals_in == 2:
                    create_order.intime = 'lin'

            if send_vals_out != 0:
                if send_vals_out == 1:
                    create_order.outtime = 'eout'
                if send_vals_out == 2:
                    create_order.outtime = 'lout'

            if attendence:
                for attend in attendence:
                    attend.tree_link = create_order.id
                    attend.attendence_done = booleans

    def getHours(self,date):
        if date:
            raw_date = str(date).split(" ")
            required_date = raw_date[1].split(":")
            new_date = required_date[0]+":"+required_date[1]
            return new_date

    def AddTimes(self,time1,time2):
        time_01 = time1.split(":")
        time_01_hrs = int(time_01[0])
        time_01_mins = int(time_01[1])

        time_02 = time2.split(":")
        time_02_hrs = int(time_02[0])
        time_02_mins = int(time_02[1])

        final_hrs = time_01_hrs + time_02_hrs
        final_mins = time_01_mins + time_02_mins

        new_time = str(final_hrs)+":"+str(final_mins)
        return new_time

    def timeToFloat(self,date):
        if date:
            raw_date = datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
            hours_added_date = raw_date
            diff_in_time = hours_added_date - datetime.strptime(str(hours_added_date.date())+" "+"00:00:00",  "%Y-%m-%d %H:%M:%S") 
            final_hrs = diff_in_time.seconds/3600.0
            return final_hrs

    def getyear(self,year):
        if year:
            raw_date = str(year).split(" ")
            required_date = raw_date[0].split("-")
            return required_date[0]

class AttendenceDates(models.Model):
    _name = 'dates.attendence'
    _rec_name = 'date'

    date = fields.Date(string="Attendence Date")

class AttendenceShifts(models.Model):
    _name = 'shifts.attendence'
    _rec_name = 'rec_name'

    intime = fields.Float(string="In Time")
    outtime = fields.Float(String="Out Time")
    monthly_working_hour = fields.Float(String="Monthly Working Hours")
    shift_work_hour = fields.Float(String="Shift Hour")
    lavi_time = fields.Float(String="Lavi Time" , default=1)
    check_out_lavi_time = fields.Char(String="Check Out Lavi Time")
    name = fields.Char(string="Shift")
    shift_name = fields.Char(string="Shift Name")
    rec_name = fields.Char(string="Rec Name")
    main = fields.Boolean(string="Main")
    overlap_shift = fields.Boolean(string="Overlap Shift")
    is_management = fields.Boolean(string="Is Management ?")
    is_night_shift = fields.Boolean(string="Is Night shift ?")
    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

    @api.onchange('intime','outtime','shift_name')
    def _onchange_times(self):
        intime = self._FloattoTime(self.intime)
        outtime = self._FloattoTime(self.outtime)

        self.name = '%s - %s' %(intime,outtime)
        self.rec_name = '%s - %s' %(self.name,self.shift_name)


    def _FloattoTime(self, floatTime):
        intime = '{0:02.0f}:{1:02.0f}'.format(*divmod(floatTime*60,60))
        intime_01 = datetime.strptime(intime, "%H:%M")
        intime_02 = str(intime_01).replace(':', ' ', 2)
        intime_03 = intime_02.split(" ")
        intime_04 = '%s:%s' %(intime_03[1],intime_03[2])
        return intime_04


class AttendenceHolidays(models.Model):
    _name = 'holidays.attendence'
    _rec_name = 'year'

    year = fields.Integer(string="Year")
    tree = fields.One2many("holidays.tree","tree_link")

    @api.multi
    def get_sundays(self):
        year = self.year
        items = []
        for x in self.tree:
            items.append(x.date)
        for d in self.allsundays(year):
            if d not in items:
                create_holiday = self.env['holidays.tree'].create({
                    'date' : d,
                    'day' : 'Sunday',
                    'remarks' : 'Sunday Holiday',
                    'tree_link' : self.id
                })

    def allsundays(self,attr):
        year = attr
        d = date(year, 1, 1)
        d += timedelta(days = 6 - d.weekday())

        while d.year == year:
            yield d
            d += timedelta(days = 7)

    def allfridays(self,attr):
        year = attr
        d = date(year, 1, 2)
        d += timedelta(days = 4 - d.weekday())

        while d.year == year:
            yield d
            d += timedelta(days = 7)

class AttendenceHolidaysTree(models.Model):
    _name = 'holidays.tree'
    _rec_name = 'day'

    date = fields.Date(string="Date")
    day = fields.Char(string="Day")
    remarks = fields.Char(string="Remarks")
    tree_link = fields.Char("holidays.attendence")

    @api.onchange('date')
    def _onchange_times(self):
        if self.date:
            now = datetime.strptime(self.date, "%Y-%m-%d")
            self.day = calendar.day_name[now.weekday()]

class ChangeShift(models.Model):
    _name = 'change.shift'
    _rec_name = 'name'

    to = fields.Date(string="To")
    form = fields.Date(string="From")
    name = fields.Char(string="New Shift")
    remarks = fields.Char(string="Remarks")
    lavi = fields.Float(string="Lavy")
    shift = fields.Many2one('shifts.attendence',string="New Shift")

    tree = fields.One2many("change.shift.tree","tree_link")
    stages = fields.Selection([
        ('draft',' Un Approve'),
        ('validated','Approve'),
        ],default="draft")

    @api.multi
    def in_draft(self):
        self.stages = "draft"
                        
    @api.multi
    def in_validate(self):
        self.stages = "validated"

    @api.onchange('to','form')
    def _onchange_times(self):
        if self.to or self.form:
            self.name = "%s - %s" %(self.form,self.to)

    @api.multi
    def records(self):
        if self.shift:
            value = 0
            self.tree.unlink()
            rec = self.env['hr.employee'].search([])
            if rec:
                for x in rec:
                    create_record = self.env['change.shift.tree'].create({
                            'employee_id':x.id,
                            'shift':self.shift.id,
                            'remarks':self.remarks,
                            'tree_link':self.id,
                            })
        else:
            raise ValidationError('PLease Enter The New Shift First.')

    @api.multi 
    def unlink(self): 
        for x in self: 
            if x.stages == "validated": 
                raise ValidationError('Cannot Delete Record') 
        return super(ChangeShift,self).unlink()

class ChangeShiftTree(models.Model):
    _name = 'change.shift.tree'
    _rec_name = 'employee_id'

    employee_id = fields.Many2one("hr.employee",string="Employee")
    shift = fields.Many2one('shifts.attendence',string="New Shift")
    remarks = fields.Char(string="Remarks")
    tree_link = fields.Char("change.shift")



""" Attendnace In-Out multiple time and exact total time calculate """
                            # number = 2
                            # loop_range = len(attendance_date)/2
                            # total_hours = 0.0
                            # total_hours_time = 0.0
                            # for x in range(loop_range):
                            #     attendance_date[:number]
                            #     time1 = datetime.strptime(str(attendance_date[number-2:number][0].ecube_date), '%Y-%m-%d %H:%M:%S')
                            #     time2 = datetime.strptime(str(attendance_date[number-2:number][1].ecube_date), '%Y-%m-%d %H:%M:%S')
                            #     if self.checkin > self.checkout:
                            #         difference = relativedelta(time2, time1)
                            #         hours = difference.hours
                            #         minutes = difference.minutes
                            #         total_minutes = ((hours * 60) + minutes)
                            #         total_hours =  float(total_minutes) / 60.00
                            #         number = number + 2
                            #         total_hours_time = total_hours_time + abs(total_hours)
                            #     if self.checkin < self.checkout:
                            #         difference = relativedelta(time1, time2)
                            #         hours = difference.hours
                            #         minutes = difference.minutes
                            #         total_minutes = ((hours * 60) + minutes)
                            #         total_hours =  float(total_minutes) / 60.00
                            #         number = number + 2
                            #         total_hours_time = total_hours_time + abs(total_hours)
                            #     self.total_time =  abs(total_hours_time)

