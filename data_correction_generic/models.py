# -*- coding: utf-8 -*- 
from odoo import models, fields, api, _
from odoo.exceptions import Warning
from odoo.exceptions import Warning, ValidationError,UserError
import collections
from datetime import datetime, timedelta
import datetime
import getpass
import psycopg2 as pg


class data_correct(models.Model):
	_name = 'data.correct'
	python_code = fields.Text(string="Python Code")
	field_querry = fields.Text(string="SQL Querry")
	result = fields.Char(string="Result")
	purpose = fields.Char(string="purpose")

	def run_python_code(self):
		try:
			if "import" in self.python_code:
				raise ValidationError("Sorry! You Can't import anything here.")
			else:
				exec(self.python_code)
		except Exception as e:
			raise ValidationError('Error..!\n'+str(e))


	@api.multi
	def run_sql_querry(self):


		user_name = getpass.getuser()
		database_name = self._cr.dbname
		conn = pg.connect(database=database_name, user=user_name)
		cur = conn.cursor()
		cur.execute(self.field_querry)
		rows_deleted = cur.rowcount
		conn.commit()

# cur.execute("DELETE FROM stock_quant WHERE id = %s", (y.id,))
