# -*- coding: utf-8 -*-
import re
from openerp import models, fields, api

class Cateory_Extend_payslip_ext(models.Model):
    _inherit  = 'hr.payslip'

    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange',related="employee_id.branch")

    @api.multi
    def action_payslip_done(self):
        self.compute_sheet()
        return self.write({'state': 'done'})

    # @api.onchange('employee_id')
    # def get_branch_id(self):
    #     if self.employee_id:
    #         if not self.branch:
    #             self.branch = self.employee_id.branch.id

    # @api.multi
    # def write(self, vals):
    #     before=self.write_date
    #     super(Cateory_Extend_payslip_ext, self).write(vals)
    #     after = self.write_date
    #     if before != after:
    #         self.get_branch_id()

    #     return True

    # @api.model
    # def create(self, vals):
    #     new_record = super(Cateory_Extend_payslip_ext, self).create(vals)
    #     print "check 1"
    #     print new_record.branch
    #     new_record.get_branch_id()
    #     print "check 2"
    #     print new_record.branch

    #     return new_record

class Cateory_Extend(models.Model):
    _inherit  = 'hr.payslip.run'

    is_account_validated = fields.Boolean("Is Account Validated")
    active = fields.Boolean("Active" , default=True)
    branch = fields.Many2one('branch',string="Entity" ,track_visibility='onchange')

    tax_deposit_date = fields.Date("Date")
    tax_deposit_type = fields.Selection([
        ('SBP', 'SBP'),
        ('NBP', 'NBP'),
        ('Treasury', 'Treasury'),
        ],string = "Deposit Type")
    tax_deposit_branch_city = fields.Char("Baranch/City")
    tax_deposit_amount = fields.Char("Amount(Rs)")
    tax_deposit_challan = fields.Char("Challan")


    @api.onchange('name')
    def get_branch_id(self):
        users = self.env['res.users'].search([('id','=',self._uid)])
        print "namememememememe"
        print users
        if self.name:
            if not self.branch:
                self.branch = users.branch.id

    @api.multi
    def write(self, vals):
        before=self.write_date
        super(Cateory_Extend, self).write(vals)
        after = self.write_date
        if before != after:
            self.get_branch_id()

        return True

    @api.multi
    def recompute_all(self):
        for x in self.slip_ids:
            if x.state not in ['done','reject']:
                x.compute_sheet()


    
    @api.multi
    def close_payslip_run(self):
        new_record = super(Cateory_Extend, self).close_payslip_run()
        for x in self.slip_ids:
            x.state = 'done'
        return new_record