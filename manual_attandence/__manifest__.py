# -*- coding: utf-8 -*-
{
    'name': "Manual Attendence ",

    'summary': """
        Make Manual Attendence if miss""",

    'description': """
        You can make Manual Attendence if miss.


Main Features
-------------
* Manually makes attendence to the system.
* Save Attendence to the sysytem.
* Can show oilder attendence if present.
    """,

    'author': "Hamza Azeem Qureshi ,Enterprise Cube (pvt) ltd",
    'website': "https://ecube.pk",
    'sequence': '10',
    'category': 'Backup',
    'version': '0.1',
 

    # any module necessary for this one to work correctly
    'depends': ['base','create_biometric_users','shift_schedule'],

    # always loaded
    'data': [
        'views/views.xml',
    ],

    # only loaded in demonstration mode
    'demo': [

    ],

    'auto_install': False,
    'installable': True,
    # 'images': ['static/description/storage.JPG'],
}
