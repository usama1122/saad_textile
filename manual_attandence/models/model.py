
from odoo import models, fields,service ,api, _
from odoo.exceptions import Warning
import os.path
import logging
from datetime import timedelta,datetime,date
import xmlrpclib



class ManualAttendenceExt(models.Model):


	_name = 'manual.attendence.ext'
	_rec_name = 'employee_id'

	

	employee_id = fields.Many2one('hr.employee',string="Employee Name")
	emp_code = fields.Char(string='Employee ID')
	department = fields.Many2one('hr.department',string="Department")
	shift_id= fields.Many2one('shifts.attendence',string="Shift")
	change_shift= fields.Many2one('shifts.attendence',string="Update Shift")
	datetime_in = fields.Datetime(string="DateTime In")
	datetime_out = fields.Datetime(string="DateTime Out")
	date_in = fields.Date(string='Date In',store=True,)
	date_out = fields.Date(string='Date Out',store=True,)
	time_in = fields.Char(string='Attendance Time In',store=True,)
	time_out = fields.Char(string='Attendance Time Out',store=True,)
	attendance_date_in = fields.Char('Attendance Date In')
	attendance_date_out = fields.Char('Attendance Date Out')
	raw_attendance_in = fields.Many2one('ecube.raw.attendance',string="Attendance in link")
	raw_attendance_out = fields.Many2one('ecube.raw.attendance',string="Attendance out link")
	tree_link = fields.One2many('ecube.raw.attendance','return_link')
	tree_link_mm = fields.Many2many('actual.attendence',string="Tree Link M2M")

	state = fields.Selection([
		('draft', 'Draft'),
		('validate', 'Validated'),
		],string = "Stages", default = 'draft' ,track_visibility='onchange')



	@api.multi
	def draft(self):
		self.state = "draft"
		self.validate_to_draft()
		actual_attendnace = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','=',self.date_in)])
		if actual_attendnace:
			actual_attendnace.set_shift()



	def validate_to_draft(self):
		if self.raw_attendance_in:
			if self.raw_attendance_in.attendence_done == True:
				actual_attendance = self.raw_attendance_in.tree_link
				print "1111111111111111111111111"
				print actual_attendance
				actual_attendance = self.raw_attendance_in.tree_link
				self.raw_attendance_in.tree_link = None
				self.raw_attendance_in.attendence_done = False
				print actual_attendance
				print "........................."
				actual_attendance.set_shift()
				# actual_attendance.calculate_raw_attendance()
				self.raw_attendance_in.unlink()

			else:
				self.raw_attendance_in.unlink()

		if self.raw_attendance_out:

			if self.raw_attendance_out.attendence_done == True:
				actual_attendance = self.raw_attendance_out.tree_link
				self.raw_attendance_out.tree_link = None
				self.raw_attendance_out.attendence_done = False
				print actual_attendance
				print "........................."
				actual_attendance.set_shift()
				self.raw_attendance_out.unlink()
			else:
				self.raw_attendance_out.unlink()

		# actual_attendnace = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','=',self.date_in)])
		# if actual_attendnace:
		# 	actual_attendnace.shift = self.change_shift.name


	@api.onchange('employee_id')
	def _onchange_employee(self):
		if self.employee_id:

			self.emp_code =self.employee_id.emp_code
			self.department = self.employee_id.department_id
			self.shift_id = self.employee_id.schedule.id
			if self.date_in:
				self.raw_attendance_rec(self.employee_id,self.date_in)
			if self.date_out:
				self.raw_attendance_rec(self.employee_id,self.date_out)
		# self.tree_link =

	@api.onchange('emp_code')
	def _onchange_machine_id(self):
		if self.emp_code:
			employee_id = self.env['hr.employee'].search([('emp_code','=',self.emp_code)],limit=1)
			if employee_id:
				self.employee_id =employee_id.id
				self.department =employee_id.department_id
				self.shift_id =employee_id.schedule.id
		

	@api.onchange('datetime_in')
	def _onchange_datetime_in(self):
		if self.datetime_in:

			start = datetime.strptime(self.datetime_in, "%Y-%m-%d %H:%M:%S")+ timedelta(hours=5)
			self.time_in = start.strftime("%H:%M:%S")
			self.date_in = start.strftime("%Y-%m-%d")
			self.raw_attendance_rec(self.employee_id,self.date_in)
			self.attendance_date_in = datetime.strptime(self.datetime_in, "%Y-%m-%d %H:%M:%S")+ timedelta(hours=5)
		elif self.datetime_in==False:
			self.time_in = ""
			self.date_in=False
			self.attendance_date_in = False
		
		


	@api.onchange('datetime_out')
	def _onchange_ecube_date(self):
		if self.datetime_out:

			start = datetime.strptime(self.datetime_out, "%Y-%m-%d %H:%M:%S")+ timedelta(hours=5)
			self.time_out = start.strftime("%H:%M:%S")
			self.date_out = start.strftime("%Y-%m-%d")
			self.raw_attendance_rec(self.employee_id,self.date_out)
			self.attendance_date_out = datetime.strptime(self.datetime_out, "%Y-%m-%d %H:%M:%S")+ timedelta(hours=5)
		elif self.datetime_out==False:
			self.time_out = ""
			self.date_out=False
			self.attendance_date_out = False


	@api.multi
	def validate(self):
		self.state = "validate"
		self.create_raw_attendance_ecube()

	def create_raw_attendance_ecube(self):
		
		if self.change_shift:
			actual_attendnace = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','=',self.date_in)])
			if actual_attendnace:
				actual_attendnace.shift = self.change_shift.name
				actual_attendnace.set_shift()
		raw_attendance = self.env['ecube.raw.attendance']
		

		if self.datetime_in:
			create_raw_attendance = raw_attendance.create({
					'employee_id': self.employee_id.id,
					'branch': self.employee_id.branch.id,
					'machine_id': self.emp_code,
					'ecube_date':self.datetime_in,
					'date':self.date_in,
					'department': self.department.id,
					'shift_id': self.shift_id.id,
					'attendance_date': self.attendance_date_in,
					'time': self.time_in
					# 'attendance_date_time': self.datetimein,
					# 'attendance_date': self.attendance_date_in,
					})
			self.raw_attendance_in = create_raw_attendance.id
		if self.datetime_out:
			# if self.change_shift:
			# 	actual_attendnace = self.env['actual.attendence'].search([('employee_id','=',self.employee_id.id),('date','=',self.date_out)])
			# 	if actual_attendnace:
			# 		actual_attendnace.shift = self.change_shift.name
			# 		actual_attendnace.set_shift()
			create_raw_attendance_out = raw_attendance.create({
					'machine_id': self.emp_code,
					'ecube_date':self.datetime_out,
					'date':self.date_out,
					'employee_id': self.employee_id.id,
					'branch': self.employee_id.branch.id,
					'department': self.department.id,
					'shift_id': self.shift_id.id,
					'attendance_date': self.attendance_date_out,
					'time': self.time_out

					})
			self.raw_attendance_out = create_raw_attendance_out.id



	@api.multi
	def unlink(self):
		for x in self:
			if x.state == "validate":
				raise ValidationError('Cannot Delete Validated Entry')

		super(ManualAttendenceExt, self).unlink()

	def raw_attendance_rec(self,employee_id,date):
		if date and employee_id:
				raw_attendance_rec = self.env['ecube.raw.attendance'].search([('employee_id','=',employee_id.id),('date','=',date)])
				self.tree_link = raw_attendance_rec.ids
				# rec_list= []
				# for attendance in raw_attendance_rec:
				#   print "sssssssssssssssssssssss"
				#   print attendance
				#   rec_list.append(attendance)
				#   print rec_list
				# self.tree_link=rec_list

class EcubeRawAttendenceExt(models.Model):
	_inherit = 'ecube.raw.attendance'

	return_link=fields.Many2one('manual.attendence.ext')